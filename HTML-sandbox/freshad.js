(function( $ ){
	
	function Freshad(){
		this.id = null;
		this.view = null;
		this.tracker = null;
		this.duration = 0;
	}

	Freshad.prototype = {
		isLandscape: function(){
			if(window.orientation)
				return Math.abs(window.orientation) == 90;
			var p = $(window);
			return p.innerWidth() / p.innerHeight() > 1

		},
		isMobile: function(){
			return true
			return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
		},
		init: function(id,view){
			if(!id)
				throw new Error( "Id Required");
			if(!view)
				throw new Error( "View Required");

			var self = this;


			self.id = id;
			self.view = view;
			self.tracker = fa('create', $('.freshad').first().attr('id') );
			self.tracker.send( 'Impression' );

			var storage = self.tracker.storage();

			var today = ( new Date() ).setHours(0,0,0,0);

			if( !('uniqueDay' in storage) || storage.uniqueDay != today ){
				storage.uniqueDay = today;
				self.tracker.storage( storage );
				self.tracker.send( 'Unique Impression' );
			}


			$('.component',view).each( function(){
				var component = $( this ).attr('name');
				$( this ).on('touchstart mousedown',function(e){
					self.tracker.send( 'Touch', component );
		 		});		
				$( 'a[target="_blank"]', this).on('touchstart mousedown',function(e){
					
						self.tracker.send( 'Exit',component,$(this).attr('href') );
		 		});
		 		var 	_played=false;var _halftime = false;
		 		$('video',this ).on('timeupdate',function(e){ 
		 			var video = e.target;
		 			if(video.currentTime > 0){
		 				if(!_played)
		 					self.tracker.send( 'Video Play', component );
		 				_played = true;
		 			}
		 			if( video.currentTime >= video.duration*0.5 ){
		 				if(!_halftime)
							self.tracker.send( 'Video Halftime',component );
						_halftime = true;
		 			}
		 			if( video.currentTime == video.duration ){
		 				self.tracker.send( 'Video Complete',component );
		 				_played = false;
		 				_halftime = false;
		 			}
					
				});
			})

			var tick = window.setInterval(function(){
				self.duration++;
				if( self.duration == 5 || self.duration == 10 || self.duration == 30) //don't track larger than 5 minutes
					self.tracker.send( 'Duration', null, String( self.duration ), self.duration )
				if(self.duration > 30 )
					window.clearInterval( tick );
			},1000);

			window.addEventListener('orientationchange', function(){ self.onRotate();self.onResize() });
			window.addEventListener('resize', function(){ self.onRotate();self.onResize() });
			self.onRotate();
			self.onResize();
			return this;

		},
		onRotate: function(){
			if(this.isLandscape()){  
				$(".freshad").removeClass('portrait');
				$(".freshad").addClass('landscape');
			}
			else{
				$(".freshad").addClass('portrait');
				$(".freshad").removeClass('landscape')
			}
		},
		onResize: function(){
			return;
			var deviceWidth  = $(window).innerWidth(),
				deviceHeight = $(window).innerHeight();

			$( ".freshad .landscape-escape").css({
				width: deviceWidth,
				height: deviceHeight
			});

			var target =  $( ".freshad " + ( this.isLandscape() ? ".landscape-escape" : ".content" ) );
			tw = target.width(),
			th = target.height();

			$('.freshad').css({
				width:tw,
				height:th
			});
		
	
			if(this.isMobile()){

				var scale = deviceWidth / tw; 
				//var h = deviceHeight / scale;

				$('.freshad').css({
					'transform' 				: 'scale('+scale+')',
					'-ms-transform' 			: 'scale('+scale+')', 
					'-webkit-transform' 		: 'scale('+scale+')',
					'left'						: (0.5 * (deviceWidth - (tw) ) ) + 'px',
					'top'						: (0.5 * (deviceHeight - (th) ) ) + 'px'
				});

			}
			else{
				$('body').css( 'overflow', 'auto');
			}
			

		}
	}

	
	var view = $('.freshad').first();

	var ad = ( new Freshad() ).init( view.attr('id'), view );




})( $ );