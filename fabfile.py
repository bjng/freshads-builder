from fabric.api import *
from contextlib import contextmanager as _contextmanager

env.hosts = [ 'root@wsm-qi.com' ]
env.supervisor_name = 'freshadsbuilder'
env.activate = 'source venv/bin/activate'
env.project_root = '/home/app.fresh-ads.com'
env.git = 'git@git.wsm-qi.com:root/freshadsbuilder.git'


@_contextmanager
def virtualenv():
        with prefix(env.activate):
            yield



def bootstrap():
	local("rm -f -R venv")
	local("pyvenv venv")
	with virtualenv():		
		local("curl -O https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py")
		local( "python ez_setup.py")
		local("easy_install pip")
		local("python setup.py develop")
		local("deactivate")

def develop():
	local("venv/bin/pserve local.ini http_port=5009 --reload ");


def commit(message=None):
	with settings(warn_only=True):
		cmd = 'git commit -a';
		if message:
			cmd = cmd + " -m '" + message  + "'"
		local(cmd)

def push():
	local("git push")

def pull():
	local("git pull")

def pcp(message=None):
	pull() 
	commit( message )
	push()

def cpp(message=None):
	commit( message )
	pull()
	push()

def cp(message=None):
	commit( message )
	pull()

def deploy():
	with settings(warn_only=True):
		if run("test -d %s" % env.project_root).failed:
			run("git clone %s %s" % (env.git, env.project_root ) )

			with cd(env.project_root):
				run("rm -f -R venv && pyvenv venv")
				with virtualenv():
					run( "curl -O https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | python " )
					run("easy_install pip")
					run("python setup.py develop")
					run("deactivate")

	with cd(env.project_root):
		run("git pull")

def live():
	env.hosts = [ 'root@162.13.113.206' ]

def restart():
	with settings(warn_only=True):
		run( 'supervisorctl restart ' + env.supervisor_name + ':')

def rebuild():
	run( "rm -f -R " + env.project_root)
	deploy()





