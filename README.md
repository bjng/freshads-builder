#Freshads - Ad Builder


Project for [Daily Mail UK](http://www.dailymail.co.uk/)

Daily Mail publishes a mobile version of their daily newspaper.
They needed a toolset that could deliver quickly Ads with some standard components like Image Galleries, Videos, Forms and 3D Turntables, that would be embedded into the electronic issues. It should be so easy to use that even an intern could create an ad very fast and the platform should also tracks reliable views and Interactions.

The project consisted of following parts:

1. A feature rich,  [Ad Builder](https://bitbucket.org/bjng/freshads-builder) based on HTML5 Canvas with a Python backend
2. Analytics Tracker Backend [Node.js Source](https://bitbucket.org/bjng/freshads-analytics/src) in particular the [Analytics Storage](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/analytics.js?at=master&fileviewer=file-view-default)
3. Analytics clients script, [source](https://bitbucket.org/bjng/freshads-analytics/src/9fa52a6f8a9cd90825c32f25f5ee479fd8cf09db/client/src/fa.js?at=master&fileviewer=file-view-default)
4. A little in-house admin interface to view the Stats in Express.js [Analytics Admin](https://bitbucket.org/bjng/freshads-analytics-frontend)

---

This repository contains part 1.

#Technologies

Python, [Pyramid](https://trypyramid.com/), Mongo DB, HTML5 Canvas, [Knockout.js](http://knockoutjs.com/) 

#Code Examples

## Frontend 

###Drawing Canvas for Ads (Frontend)

The main feature of the Ad Builder is a resizable canvas, which lets you easily select areas via click'n'drag and assign predefined HTML5 components, like a Video Player, Form or 3D Turntable. The UI is very intuitive, with possibilities to create grids and guides ala Photoshop and enable snapping for area selection or component transformations.
The canvas part was completely bespoke, no external canvas library was used. The  canvas behaved sometimes quite tricky when it was resized, depending on the settings it became very slow or the grids and guides where rendered blurry. Some fiddeling with the clients `oBackingStorePixelRatio` settings solved all that, but who has heared about that before?

[Ad Canvas](https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/static/js/canvasControl.js?at=master&fileviewer=file-view-default)

###Media Bucket

Each Ad needed some media embedded, normal images, videos, fonts, or image sequences. All of The media files together where not allowed to exceed a particular size as well and in particular images sequences needed to be displayed in a compact way. So we created our own little Upload Bucket that fullfilled these criterias

* Show progress of uploads
* Show files sizes and warning if Ad Max Size is reached
* Visually group image sequences with sometimes up to 180 files to one item
* Constrict Filetypes that are allowed for uploading
* Display how often a media file has been used and reused in all components, to be able to identify quickly unused files

[Ad Bucket Manager](https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/static/js/bucketManager.js?at=master&fileviewer=file-view-default)

###Font Manager

Sometimes Ads needed to have custom fonts, which are possible to upload via the Bucket Manager. What turned out to be tricky though was to show the new fonts without reloading the current canvas session.

[Font Manager](https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/static/js/fontManager.js?at=master&fileviewer=file-view-default)

##Backend

###Pyramid and Security

After a little research I decided to use Pyramid for the backend. I've read online that it is faster and less opinionated than Django and I didn't trust the Node.js Evironement yet enough at that time. And I was very happy with my choice.
I liked in particular the security part of Pyramid and how you manage role based access. Besides the following Ad Bundler its a pretty standard Backend though, a good starting point are the routes

[Routes](https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/views.py?at=master&fileviewer=file-view-default)

###Ad Bundler

To finalize an Ad it had to be bundled to a flattened folder and the zipped up.
This would be used then by the editorial team within Adobes publishing suite.

[Ad Bundler](https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/bundle.py?at=master&fileviewer=file-view-default)
https://bitbucket.org/bjng/freshads-builder/src/e5cc0f3f955130c1ce732e3469a625dc19ea9a6d/freshads/bundle.py?at=master&fileviewer=file-view-default


---

Developer Notes ...

#Installation
git clone https://git.wsm-qi.com/root/freshadsbuilder.git
pip install fabric
fab bootstrap

#Run Application
fab develop

#CI Helpers
fab pull = git pull
fab commit = git commit
fab push = git push
fab pcp = pull commit push
fab deploy = runs git clone or pull on dev server
fab restarts = restarts application on dev server
fab rebuild = rebuilds the hole app on dev server ( know what you do )
fab live = changes the host from dev to live server

Chaining is possible, e.g. pushing new code to git and deplying to live would be:
fab commit push live deploy restart 


#Component Development

#HANDLENAME > all lowercase
#Examples: carousel, htmldrop


#create prototype in DB:

use freshads
db.components.prototype.save( { type: '[HANDLENAME]', verboseName: '[HUMAN_READABLE_NAME]', model: { [YOUR_DATA_MODEL] } } )

Create js controller in:

freshads/static/components/HANDLENAME/
	HANDLENAME-edit.js
	HANDLENAME-view.js

Create view templates in:

freshads/templates/components/HANDLENAME/
	HANDLENAME-edit.mako
	HANDLENAME-view.mako

Components JS controllers
-------------------------
#uses AMD pattern of require.js


# EDIT ( HANDLENAME-edit.js)
# all available libraries defined in static/js/init.js
# currently bootstrap,knockout,lodash,zepto
# Example AMD Controller with, jquery, knockout and lodash

define( [ 'jquery', 'knockout','lodash' ] function( $, ko, lodash ){
	return function( view, model ){
		
		var view = $( view );
		var data = model.data;

		//YOUR AWSOME CODE HERE FOR EDITING A COMPONENT

	}
});



# VIEW ( HANDLENAME-view.js )
# needs to be very clean as this will be bundled into the final ad!
# Zepto is in place for DOM Handling
# as jquery is installed at the same time in the composer you need to set $ for zepto youself, Example:

define( [ 'zepto' ], function( Zepto ){
	
	return function( view ){
		var $ = Zepto;
		var view = $( '#' + view );

		//YOUR AWSOME CODE HERE FOR VIEWING A COMPONENT

	}
}









