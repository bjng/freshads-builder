<%include file="freshads:templates/include/header.mako"/>
<%include file="freshads:templates/include/top-nav.mako"/> 


<div id="admin" class="app-container">
	<header class="site-header full-width">
		<div class="col-4-12 cell">&nbsp;</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<div class="logo">
        		<a href="/">
        			<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
        		</a>
      		</div>
		</div>
	</header>

	<div class="content full-width">

		<div class="tile-list col-10-12">
			% if user.get('group') == 'superuser':
			<a class="tile" href="/users">
				<div class="tile-description">
					<h1>Users</h1>
					<small>Management</small>
				</div>
			</a>
			% endif
			<a class="tile" href="/templates/">
				<div class="tile-description">
					<h1>Templates</h1>
					<small>Management</small>
				</div>
			</a>
			<a class="tile" href="/clients/">
				<div class="tile-description">
					<h1>Clients</h1>
					<small>Management</small>
				</div>
			</a>			
		</div>

	</div>
</div>


<%include file="freshads:templates/include/footer.mako"/>