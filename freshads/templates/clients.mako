<%include file="freshads:templates/include/header.mako"/>

<%
	# disallowCreate is also controls the duplicate button
	group             = user.get('group')
	disallowCreate    = group == 'accountmanager' or group == 'producer' 
	disallowSettings  = group == 'accountmanager' or group == 'producer' 
	disallowDelete    = group == 'producer' or group == 'accountmanager'
%>

<%include file="freshads:templates/include/top-nav.mako"/> 

<div id="clients" class="app-container">
	<header class="site-header full-width">
		<div class="col-4-12 cell">
			<h1>Clients</h1>
		</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<div class="logo">
        		<a href="/">
        			<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
        		</a>
      		</div>
		</div>
	</header>

	<div class="content full-width">
		<div class="create-filter">
			<div class="col-4-12">&nbsp;</div>
			<div class="col-4-12 center">

				% if not disallowCreate :
				<button type="button" class="btn btn-success" data-bind="click:createItem" data-toggle="modal" data-target="#editItem">
					Create New Client <i class="fa fa-plus"></i>
				</button>
				% else:
					&nbsp;
				% endif

			</div>
			<div class="col-4-12 right">	
				<label>Filter By</label>
				<div class="btn-group">
				    <button data-filter="modified" data-direction="-1" type="button" class="btn btn-default active" data-bind="click:setFilter">DATE <i></i></button>
					<button data-filter="name" data-direction="1" type="button" class="btn btn-default" data-bind="click:setFilter">A-Z <i></i></button>
				</div>
			</div>
		</div>

		<div class="tile-list col-10-12" data-bind="foreach:items">
			<a class="tile" data-bind="attr:{href:'/clients/'+id}">
				
				% if not disallowSettings :
				<button class="btn btn-settings" data-bind="click:$parent.editItem" data-toggle="modal" data-target="#editItem"><i class="fa fa-cog"></i></button>
				% endif

				% if not disallowDelete : 
				<button class="btn btn-delete" data-bind="click:$parent.removeItem" data-role="delete"><i class="fa fa-trash-o"></i></button>
				% endif

				<div class="tile-description">
					<h1 data-bind="text:name, attr:{title:name}">Item</h1>
					<small data-bind="text:id"></small>
				</div>
			</a>
		</div>

		<div id="editItem" class="modal" >
			<div class="modal-dialog" data-bind="if:selectedItem" >
				<div class="modal-content">
					<form role="form" novalidate data-bind="submit:saveItem">
						<div class="modal-header"> 
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" data-bind="text: selectedItem()._id ? 'Edit Client' : 'New Client'">Edit Client</h4>
					     </div>
						
						<div class="modal-body" data-bind="with:selectedItem">
							<div class="form-group">
								<label for="name">Client Name</label>
								<input type="text" class="form-control" id="name" placeholder="Enter Name" data-bind="value:name,valueUpdate:'keyup'" pattern="^\w+[\s\w]*" data-content="Please enter a client name" data-placement="top" required />
							</div>

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	require(['app','clients'],function(app,C){
		new C('#clients')
	})
</script>

<%include file="freshads:templates/include/footer.mako"/>