<%include file="freshads:templates/include/header.mako"/>
<%include file="freshads:templates/include/top-nav.mako"/> 

<link rel="stylesheet" href="${request.static_url('freshads:static/css/analytics.css')}" type="text/css" media="screen" charset="utf-8" />

<div  id="${data.get('id')}" class="app-container">
	<div class="back-to-area"><h2><a class="back-to" href="/campaigns/${parent.get('id')}"><span class="fa fa-reply"></span> ${parent.get('name')}</a></h2></div>
	
	<header class="site-header full-width">
		<div class="col-4-12 cell">
				<h1>${data.get('name')}</h1>
				<span>Ad Id: ${data.get('id')}</span>
		</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<a class="logo" href="/">
        		<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
			</a>
		</div>
	</header>
	<div class="content full-width">
		<div class="analytics-area col-10-12">
			<div id="analytics">
				<h1>Analytics</h1>
				<div class="date-selection">
					<div class="form-group">
						<label class="control-label">from</label>
						<div class="input-group">
							<input type="text" class="form-control" id="bookingId" placeholder="" data-bind="datepicker:startDate" readonly >
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="form-group"> 
						<label>to</label>
						<div class="input-group">
							
							<input type="text" class="form-control" id="bookingId" placeholder="" data-bind="datepicker:endDate" readonly >
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<span class="help-block">
						Please pick your date range to display details.<br/>
					</span>
					
				</div>
				<div id="timerangeStats" class="canvas">


					<div data-bind="visible:days">
						<ul id="topDwell" data-bind="foreach:days"><li data-bind="click:$parent.currentDate, style:{ width: $parent.getClickSize() }"></li></ul>
						<div class="indicating">
							<div class="mDwell"><span></span>Impressions</div>
							<div class="aDwell"><span></span>Unique Impressions</div>
						</div>
						<div class="canvas-lg">
							<canvas id="canvas1" width="960" height="300"></canvas>
						</div> 
						<div style="clear:both;"></div>
					</div>
				</div>
				<div id="dayStats" data-bind="with:currentDate">

					<h1 data-bind="text:dateString"></h1>
					<div class="canvas-sml">
						<div class="canvas-content">
							<h2>TOTAL</h2>
							<p>This shows the number of times the advert has been viewed and its average dwell time. Unique impressions are filtered by ID Tracking.  <br/> Please be aware you may experience more impression counts than events and/or clicks. </p>
						</div>
					</div>
					<div class="canvas-sml">

						<table class="table table-striped">
							<tbody
								<tr>
									<td>Impressions</td>
									<td>&nbsp;</td>
									<td data-bind="text: count"></td>
								</tr>
								<tr>
									<td>Unique Impressions</td>
									<td>&nbsp;</td>
									<td data-bind="text:uniqueCount"></td>
								</tr>
					
							   <!-- ko foreach: durations -->
       							<tr>
									<td>Duration</td>
									<td>&gt; <span data-bind="text:time"></span> sec</td>
									<td data-bind="text:count"> </td>
								</tr>
    							<!-- /ko -->
						
							</tbody>
						</table>
						<table class="table table-striped" >
							
						</table>

					</div>
					<div class="divider"></div>
					<div class="canvas-sml">
						<div class="canvas-content">
							<h2>COMPONENT EVENTS</h2>
							<p>Each component will have its own count for the amount of times it has been interacted with per day. Videos will have counts for plays and completions. <br/> Exit links will be counted when a user has clicked on a link; this is the same for internal advert links. </p>
						</div>
					</div>
					<!--div class="canvas-sml">
						<div class="canvas-sm">
							<canvas id="canvas4" width="450" height="250"></canvas>
						</div>
					</div-->
					<div style="clear:both;"></div>
					<div class="canvas">

						<table class="table table-striped">
						<thead>
							<tr>
								<th>Event</th>
								<th>Component</th>
								<th>Count</th>
								<th class="count"></th>
								<th>Info</th>
							</tr>
						</thead>
						<tbody data-bind="foreach:events">
							<tr>
								<td data-bind="text: $data.n">Event</td>
								<td data-bind="text: $data.g">Component</td>
								<td data-bind="text: $data.c"></td>
								<td><div class="bar" data-bind="style: { width: $root.getEventPercentage( $data ),backgroundColor: $root.getEventColor( $data ) }">&nbsp;</div>
								</td>
								<td data-bind="text: $data.l">Info</td>
							</tr>
						</tbody>
						</table>
					</div>
					<div class="backToTop"><div class="btn btn-default btn-xs backToTop-container"><i class="fa fa-angle-up"></i></div></div>
					<div style="clear:both;"></div>
					<!--div class="canvas">
						<div class="canvas-lg">
							<canvas id="canvas3" width="300" height="300"></canvas>
						</div>
						<div class="backToTop"><div class="btn btn-default btn-xs backToTop-container"><i class="fa fa-angle-up"></i></div></div>
						<div style="clear:both;"></div>
					</div-->
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</div>
<div class="btn-group support">
	<div class="btn-group">
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-question"></i></button>
		<div class="dropdown-menu stopPropagation">
		<!-- Help section -->
		<div class="help-expanded">
		    <div class="help-top">
		      <h2>Help</h2>
		      <p>Please contact <a href="mailto:support@fresh-ads.com">support@fresh-ads.com</a></p>
		    </div>
		    <!--
		    <div class="help-list">
		      <ul>
		        <li class="link1">link1</li>
		        <li class="link2">link2</li>
		        <li class="link3">link3</li>
		        <li class="link4">link4</li>
		      </ul>
		    </div>
		    <div id="link1" class="help-section">
		      <h3>Header</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
		      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
		    </div>
		    <div id="link2" class="help-section">
		      <h3>Header</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
		      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
		    </div>
		    <div id="link3" class="help-section">
		      <h3>Header</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
		      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>    </div>
		    <div id="link4" class="help-section">
		      <h3>Header</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
		      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
		    </div> -->
		</div>
	</div>
	</div>
	<button class="btn btn-default logout"><a href="/logout"><i class="fa fa-sign-out"></i></a></button>
	
</div>  
<script type="text/javascript">
	require(['app','analytics'],function(app,C){
		new C("#${ data.get('id')}")
	})
</script>