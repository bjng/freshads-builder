<%include file="freshads:templates/include/header.mako"/>
<%
	# disallowCreate is also controls the duplicate button
	group             = user.get('group')
	disallowCreate    = group == 'accountmanager' or group == 'producer'
	disallowSettings  = group == 'accountmanager' or group == 'producer'
	disallowDelete    = group == 'producer' or group == 'accountmanager'
	disallowAnalytics = group == 'producer'

	gotoProofs        = group == 'accountmanager'
	primary_link      = '/proof/' if gotoProofs else  '/ads/'
	primary_link_id = '$data.proof.$oid' if gotoProofs else '$data._id.$oid'
%>
<%include file="freshads:templates/include/top-nav.mako"/> 

<div id="${ data.get('id') }" class="app-container">
	% if user.get('group') == "superuser":
		<div class="back-to-area" style="position: relative;left: 40px; width:30%;"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	% elif user.get('group') == "producermanager":
		<div class="back-to-area" style="position: relative;left: 40px; width:30%;"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	% else:
		<div class="back-to-area"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	%endif
	<header class="site-header full-width">
		<div class="col-4-12 cell">
			<h1>${data.get('name')}</h1>
			<span>Brand ID: ${data.get('id') }</span>
		</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<a class="logo" href="/">
	   			<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
			</a>
		</div>
	</header>

	<div class="content full-width">
		<div class="create-filter">
			<div class="col-4-12">&nbsp;</div>
			<div class="col-4-12 center">
				% if not disallowCreate :
				<button type="button" class="btn btn-success" data-bind="click:createItem" data-toggle="modal" data-target="#editItem">
					Create New Ad <i class="fa fa-plus"></i>
				</button>
				% else:
					&nbsp;
				% endif
			</div>
			<div class="col-4-12 right">	
				<label>Filter By</label>
				<div class="btn-group">
				    <button data-filter="modified" data-direction="-1" type="button" class="btn btn-default active" data-bind="click:setFilter">DATE <i></i></button>
					<button data-filter="name" data-direction="1" type="button" class="btn btn-default" data-bind="click:setFilter">A-Z <i></i></button>
				</div>
			</div>
		</div>


		<div class="tile-list col-10-12" data-bind="foreach:items">
			<div class="tile temp">
				
				% if not disallowSettings :
				<button class="btn btn-settings" data-bind="click:$parent.editItem" data-toggle="modal" data-target="#editItem"><i class="fa fa-cog"></i></button>
				% endif

				% if not disallowDelete : 
				<button class="btn btn-delete" data-bind="click:$parent.removeItem" data-role="delete"><i class="fa fa-trash-o"></i></button>
				% endif

				% if not disallowAnalytics :
				<button class="btn btn-analytics" data-bind="click:$parent.gotoAnalytics"><i class="fa fa-signal"></i></button>				
				% endif

				% if not disallowCreate :
				<!--button class="btn btn-duplicate" data-bind="click:$parent.copyItem"><i class="fa fa-copy"></i></button-->
				% endif

				<div class="template-title">
					<h1 data-bind="text:name">Item</h1>
					<small data-bind="text:$parent.toTimeString($data.modified)"></small>
				</div>

				<a class="template-device-area" data-bind="attr:{href: '${primary_link}' + ${primary_link_id} }">
					<div class="device-border">
						<span class="" data-bind="style:{width:$parent.getAdWidth($data),height:$parent.getAdHeight($data)}"></span>
					</div>
				</a>

				<div class="template-info">
					<small data-bind="text:id"></small><br />
					<small data-bind="text:bookingId"></small>
				</div>

				<div data-bind="style:{width:$parent.getAdWidth($data)}"></div>
			</div>
		</div>

		<div id="editItem" class="modal">
			<div class="modal-dialog" data-bind="if:selectedItem">
				<div class="modal-content">
					<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" data-bind="text: selectedItem()._id ? 'Edit Ad' : 'New Ad'">Edit Ad</h4>
				    </div>
				    <form role="form" novalidate data-bind="submit:saveItem">
					<div class="modal-body">
						<div data-bind="if: !selectedItem().id">
							<div class="template-select">
								<ul class="template-tiles" data-bind="foreach:templates">
									<li class="tile" data-bind="click:$parent.applyTemplate">
										
										<div class="template-title" data-bind="text:name"></div>
										
										<div class="template-device-area">
											<div class="device-border">
												<span class="" data-bind="style:{width:$parent.getAdWidth($data),height:$parent.getAdHeight($data)}"></span>
											</div>
										</div>

										<div class="template-info">
											w<span data-bind="text:style.width"></span> x 
											h<span data-bind="text:style.height"></span>
										</div>	
										<!--<span data-bind="text:style:width"></span>-->
									</li>
								</ul>
							</div>
						</div>
						<div data-bind="with:selectedItem">
							<div class="form-group">
								<label for="name">Ad Name</label>
								<input type="text" class="form-control" id="name" placeholder="Enter Name" data-bind="value:name,valueUpdate:'keyup'" pattern="^\w+[\s\w]*" data-content="Please enter an Ad Name" data-placement="top" required>
							</div>
							<div class="form-group">
								<label for="name">Campaign</label>
								<input required="true" type="text" class="form-control" id="campaign" pattern="^\w+[\s\w]*" placeholder="Enter Campaign" data-placement="top" data-content="Please enter a Campaign Name" data-bind="value:campaign,valueUpdate:'keyup'">
							</div>
							<div class="form-group">
								<label for="name">Booking ID</label>
								<input type="text" class="form-control" id="bookingId" placeholder="Enter Booking ID" data-bind="value:bookingId,valueUpdate:'keyup'">
							</div>


							<div class="form-group">
								<label for="name">Insertion Date</label>

								<div class="input-group">
									<input type="text" class="form-control" id="Insertion Date" placeholder="" data-bind="datepicker:insertionDate" readonly >
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label for="width">Width</label>
								<input type="text" class="form-control" id="width" placeholder="1024" data-bind="value:style.width,valueUpdate:'keyup'">
							</div>
							<div class="form-group">
								<label for="height">Height</label>
								<input type="text" class="form-control" id="height" placeholder="768" data-bind="value:style.height,valueUpdate:'keyup'">
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
	require(['campaign'],function(C){
		new C("${ data.get('id') }")
	})
</script>

<%include file="freshads:templates/include/footer.mako"/>