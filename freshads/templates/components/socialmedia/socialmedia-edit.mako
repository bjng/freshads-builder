<div class="">
	

	<div class="inner-panel">
	    
		
		<div class="social-link-container">
				<!-- FACEBOOK -->
				<div class="form-group">
					<label>Facebook URL<i class="fa fa-facebook-square" style="margin-left: 10px;"></i></label>
					<div class="input-group">
						<input id="fb-url" type="text" class="form-control input-sm" placeholder="http://..." data-bind="value:fbSource">
					</div>
				</div>
				<!-- TWITTER -->
				<div class="form-group">				
					<label>Twitter URL<i class="fa fa-twitter-square" style="margin-left: 10px;"></i></label>
					<div class="input-group">	
						<input id="tw-url" type="text" class="form-control input-sm" placeholder="http://..." data-bind="value:twSource">
					</div>
				</div>

				<div class="form-group">
					<label for="font-size">Font Size</label>	
					<div class="input-group">
						<input id="font-size" type="number" class="form-control" data-bind="value:iconSize">
					</div>
				</div>

				<div class="form-group">
					<label>Button color</label>
					<div class="input-group">
						<input type="text" class="form-control colorpicker bscp" value="rgb(255, 255, 255)" data-bind="colorpicker:fgColor" data-color-format="rgba">
						<span class="input-group-addon color-indicator fgcolor-indicator"><i data-bind="style:{ backgroundColor: fgColor }" style="border: 1px solid #cccccc;"></i></span>
					</div>
				</div>
		</div>
		<div style="clear:both">&nbsp;</div>
	</div>
</div>