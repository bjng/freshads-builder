<% 
	c = env.get('component')
	assets_root = env.get( 'assets_root')
	data = c.get('data',{})
	fbSrc = data.get('fbSource')
	twSrc = data.get('twSource')
	fgColor = data.get('fgColor')
	fontSize = str( data.get('iconSize','30') ) + 'px'
	style = c.get( 'style' )



%>
<div style="width:100%;height:100%;">

	% if fbSrc:
	<a href="${fbSrc}" target="_blank" style="text-decoration:none;"><i style="color:${fgColor};font-size:${ fontSize };line-height:${ fontSize }" class="icon icon-facebook"></i></a>
	% endif
	% if twSrc:
	<a href="${twSrc}" target="_blank" style="text-decoration:none;"><i style="color:${fgColor};font-size:${ fontSize };line-height:${ fontSize }" class="icon icon-twitter-2"></i></a>
	% endif	

</div>