<% 
	c = env.get('component')
	assets_root = env.get( 'assets_root')
	components = c.get('components')
	style = c.get( 'style' )
%>
<div style="width:100%;height:100%;overflow:hidden">
	<ul style="list-style-type:none;margin:0px;padding:0px;height:100%;position:relative">
		% for story in components:

		% if story.get( 'image' ):
		<li style="float:left;position:relative;height:100%;width:100%;padding:0px;margin:0px;background: no-repeat center url( '${ assets_root + str( story.get( 'image',{} ).get( 'filename','' ) ) }')">
			% if story.get('url'):
				<a href="${ story.get( 'url' ) }" target="_blank" style="display:block;width:100%;height:100%">&nbsp;</a>
			% endif
		</li>
		% endif
		% endfor
	</ul>
</div>
