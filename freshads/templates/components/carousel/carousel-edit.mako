
<div style="min-width:400px;max-width:800px;">
	<form role="form">	
		<ul class="list-unstyled" data-bind="foreach:components">
			<li>
				<div class="panel" style="overflow:hidden;position:relative">
					<div>
						<div data-bind="bucket:image,contentType:'image'" class="fa-image"></div>
					</div>
					<div class="form-group">
						<label>Image URL</label>
						<div class="input-group">
						  <input type="text" class="form-control input-sm" placeholder="http:// ( optional )" data-bind="value:url,valueUpdate:'afterkeydown'">
						</div>
					</div>
					<div style="clear:both">&nbsp;</div>
				</div>
				
			</li>

		</ul>
		<button style="width:100%" class="btn btn-primary" data-bind="click:createStory">Add Slide</button>
	</form>
<div>
