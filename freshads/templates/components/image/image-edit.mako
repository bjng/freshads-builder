<div class="panel">
	<div>
		<div data-bind="bucket:image,contentType:'image'" class="fa-image">
	</div>

	<div class="form-group">
		<label>Image URL</label>
		<div class="input-group">
			<input type="text" class="form-control input-sm" placeholder="http://" data-bind="value:url,valueUpdate:'keyup'">
		</div>
	</div>

</div>