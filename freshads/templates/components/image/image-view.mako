<% 
	c = env.get('component')
	assets_root = env.get( 'assets_root')
	d = c.get('data')
%>

%if d.get('url'):
<a href="${ d.get( 'url' ) }" style="position:absolute;width:100%;height:100%;overflow:hidden" target="_blank">
%endif
%if d.get('image'):
<img src="${ assets_root + str( d.get( 'image',{} ).get( 'filename','' ) ) }" style="position:absolute;width:100%;height:auto;"/>
%endif
%if d.get('url'):
</a>
%endif
