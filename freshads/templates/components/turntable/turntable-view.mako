<% 
	c = env.get('component')
	assets_root = env.get( 'assets_root' )
	d = c.get('data')

	images = d.get('images');

%>

%if images:
<div style="width:100%;height:100%;overflow:hidden;position:relative">
	<div  class="list-group-turntable" style="width:100%" >
		%for img in images:
		<img src="${assets_root + img.get( 'filename' )}" style="display:none;position:absolute;left:0px;width:100%" />
		%endfor
	</div>
	
</div>
%endif

