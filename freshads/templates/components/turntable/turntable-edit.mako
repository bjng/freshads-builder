<div>

	<p class="help-block">Please click "Select Sequence From Assets". Then click on the name of any item marked with : <i class="fa fa-forward" style="font-size:inherit" ></i>. This will automatically assign all images from that sequence to the Turntable.</p>
	<div>
		<div data-bind="bucket:data.images,contentType:'image',if:preview">
			<img data-bind="attr:{src:preview}" style="width:300px"/>
		</div>
	</div>
	<button class="btn btn-primary" data-bind="bucket:data.images,contentType:'image'" role="button">Select Sequence from Assets <i class="fa fa-cloud-upload"></i></button>

</div>