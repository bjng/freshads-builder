<% 
	c = env.get('component')
	assets_root = env.get( 'assets_root')
	data = c.get('data',{})
	backdropUrl = ''
	import urllib
	backdrop = data.get('backdropImage')
	backdropUrl =  assets_root + backdrop.get( 'filename' ) if backdrop else ''
	backdropUrl = urllib.parse.quote( backdropUrl )
	bundledVideos = data.get('videoFiles',None)
	
	streams = []

	if data.get('videoSrcMp4'): 
		streams.append( {'url': data.get('videoSrcMp4'), 'contentType': 'video/mp4'})
	if data.get('videoSrcWebM'): 
		streams.append( {'url': data.get('videoSrcWebM'), 'contentType': 'video/webm'})

	streams = streams if len( streams ) else None
	
	backdropColor = data.get('backdropColor')
	bgColor = data.get('bgColor')
	fgColor = data.get('fgColor')
	style = c.get( 'style' )
	brightcoveId = data.get('brightcoveId')
	brightcoveToken = data.get('brightcoveToken')
%>

<div class="video-container" data-brightcoveid="${brightcoveId}" data-brightcovetoken="${brightcoveToken}" style="background: ${backdropColor}">
	
	
	<video class="video">
	% if bundledVideos:
		% for video in bundledVideos:
		<source src="${ assets_root + video.get('filename') }" type="${ video.get('contentType') }">
		% endfor
	% elif brightcoveToken and brightcoveId:
		
	% elif streams:
		% for stream in streams:
		<source src="${stream.get('url')}" type="${stream.get('contentType')}">
		%endfor
	% endif
		Your browser doesn't support HTML5 video.
	</video>
	<img class="poster" src="${ backdropUrl }"/>
	<div class="hud">
		<div class="superPlay"><span><i class="icon-play"></i></span></div>
		<div class="controls" style="background-color:${bgColor}">
			<button type="button" class="btn-play-pause" style="color:${fgColor}"><i class="icon-play"></i><i class="icon-pause"></i></button>
			<button type="button" class="btn-fullscreen" style="color:${fgColor}"><i class="icon-expand"></i></button>
			<div class="seek-bar-container">
				<div class="seek-bar"></div>
				<div class="seek-handle"></div>
			</div>
		</div>
	</div>
	<div class="error"><p>This video can't be played.</br> Please check your connection!</p></div>
</div>