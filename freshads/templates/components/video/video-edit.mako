<div>

		<div class="asset-input-container">
			<label>Backdrop Image</label>
			<div data-bind="bucket:data.backdropImage" class="fa-image"></div>
		</div>

		<div class="form-group">
			<label>From Bundle</label>
			<div>
				<div data-bind="visible:data.videoFiles().length">
					<ul class="list-group" data-bind="foreach:data.videoFiles">
						<li class="list-group-item sm"><span data-bind="text:filename"></span><button type="button" class="close" aria-hidden="true" style="line-height:18px;margin-right:5px;" data-bind="click:$root.removeVideoFile">&times;</button></li>
					</ul>
				</div>
				<button class="btn btn-primary" data-bind="bucket:data.videoFiles,contentType:'video'" role="button">Select from Assets <i class="fa fa-cloud-upload"></i></button>
			</div>
		</div>




		<div class="form-group">
			<label>Brightcove API Token</label>
			<input type="text" class="form-control input-sm" placeholder="Token with URL Access" data-bind="value:data.brightcoveToken">
		</div>

		<div class="form-group">
			<label>Brightcove API Id</label>
			<input type="text" class="form-control input-sm" placeholder="" data-bind="value:data.brightcoveId">
		</div>

		<div class="form-group">
			<label>Hosted MP4 URL</label>
			<input type="text" class="form-control input-sm" placeholder="http://..." data-bind="value:data.videoSrcMp4">

		</div>

		<div class="form-group">
			<label>Hosted WebM URL</label>
			<input type="text" class="form-control input-sm" placeholder="http://..." data-bind="value:data.videoSrcWebM">
		</div>

		<div class="form-group colorpicker">
			<label>Backdrop Colour</label>
			<div class="input-group">
				<input type="text" class="form-control" value="rgb(0, 0, 0, 1)" data-bind="colorpicker:data.backdropColor" data-color-format="rgba">
				<span class="input-group-addon color-indicator bgcolor-indicator"><i data-bind="style:{ backgroundColor: data.backdropColor }"></i></span>
			</div>
		</div>


		<div class="form-group colorpicker">
			<label>Controls Colour</label>
			<div class="input-group">
				<input type="text" class="form-control" value="rgb(0, 0, 0, 1)" data-bind="colorpicker:data.bgColor" data-color-format="rgba">
				<span class="input-group-addon"><i data-bind="style:{ backgroundColor: data.bgColor }"></i></span>
			</div>
		</div>

		<div class="form-group colorpicker">
			<label>Button Colour</label>
			<div class="input-group">
				<input type="text" class="form-control" value="rgb(255, 255, 255)" data-bind="colorpicker:data.fgColor" data-color-format="rgba">
				<span class="input-group-addon color-indicator"><i data-bind="style:{ backgroundColor: data.fgColor }"></i></span>
			</div>
		</div>
		
		<div style="clear:both;height:0;">&nbsp;</div>
</div>