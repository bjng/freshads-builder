<%
  component = env.get('component')
  data = component.get('data')
  ad = env.get('ad')
  labelStyle = 'color:' + data.get('labelColor')
  inputStyle = 'background-color:' + data.get('inputBackgroundColor') + ';color:' + data.get('inputColor')
%>
%if data.get('action'):
<form role="form"  method="POST" class="form-horizontal" action="${data.get('action')}" style="color:${data.get('labelColor','')}" novalidate>
  <input type="hidden" name="freshadId" value="${ad.get('id')}">
  <input type="hidden" name="bookingId" value="${ad.get('bookingId')}">
  %if data.get('title').get('active'):
  <div class="form-group">
    <label class="control-label" for="title">Title</label>
    <div>
      <select value="Mr" style="width:80px" name="title" class="form-control">
        <option>Mr</option>
        <option>Mrs</option>
      </select>
    </div>
  </div>
  %endif
  <div class="form-group">
    <label for="name"  class="control-label" style="${labelStyle}">Name</label>
    <div>
      <input name="name" type="text" required="true" class="form-control"  placeholder="Name" style="${inputStyle}" pattern="[A-Za-z\s]{2}" data-content="Please insert your first name">
    </div>
  </div>
  <div class="form-group">
    <label for="name" required="true" class="control-label" style="${labelStyle}">Surname</label>
    <div>
      <input name="surname" pattern="[A-Za-z\s]{2}" type="text" required="true" class="form-control" id="surname" placeholder="Surname" style="${inputStyle}" data-content="Please insert your surename">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label" style="${labelStyle}">Email</label>
    <div>
      <input type="text" required="true" class="form-control" name="email" placeholder="Email" style="${inputStyle}" pattern="[\w-\.]+@(?:[\w-]+\.)+[a-zA-Z]{2,4}" data-content="Please provide a valid email address, e.g. your.name@email.com">
    </div>
  </div>
  %if data.get('address').get('active'):
  <div class="form-group">
    <label for="address1"  class="control-label" style="${labelStyle}">Address</label>
    <div>
      <input name="address1" type="text" pattern="[\-A-Za-z\s]{2}" ${ 'required' if data.get('address').get('required') else ''} class="form-control" placeholder="Street" style="${inputStyle}" data-content="Please insert a valid street name">
    </div>
  </div>
  <div class="form-group">
    <label for="address2" class="control-label"></label>
    <div>
      <input name="address2" type="text" class="form-control" id="address2" placeholder="" style="${inputStyle}">
    </div>
  </div>
  %endif
  %if data.get('postcode').get('active'):
  <div class="form-group">
    <label for="postcode" style="${labelStyle}" class="control-label">Postcode</label>
    <div style="width:150px">
      <input type="text"  name="postcode"  ${ 'required' if data.get('postcode').get('required') else ''} class="form-control"  pattern="[a-zA-Z0-9]{4,10}" placeholder="Post Code" style="${inputStyle}" data-content="Please insert your PostCode without space, e.g. e153py">
    </div>
  </div>
  %endif
  %if data.get('phone').get('active'):
  <div class="form-group">
    <label for="phone" style="${labelStyle}" class="control-label">Phone</label>
    <div style="width:150px">
      <input type="text" name="phone" ${ 'required' if data.get('phone').get('required') else ''} class="form-control"  pattern="^\+?[0-9]{7,}" placeholder="+44..." style="${inputStyle}" data-content="Please insert a valid phone number, which includes your country code and no spaces e.g. 4401234567890">
    </div>
  </div>
  %endif
  %if data.get('dateOfBirth').get('active'):
  <div class="form-group" style="${labelStyle}">
    <label for="dateOfBirth" class="control-label">Date of Birth</label>
    <div>
      <input name="dateOfBirth" pattern="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/((19[2-9][0-9])|(200[0-9])|2010)" type="text" data-content="Please provide a valid date, e.g. 27/08/1979"  ${ 'required' if data.get('dateOfBirth').get('required') else ''} class="form-control" id="dateOfBirth" placeholder="DD/MM/YYYY" style="${inputStyle}">
    </div>
  </div>
  %endif
  <div class="form-vertical">
  %for question in data.get('questions'):
    <div class="form-group">
      <label for="answer${loop.index}" style="${labelStyle}" class="control-label">${question}</label>
      <div>
        <input id="answer${loop.index}" name="answer${loop.index}" type="text" required="true" class="form-control"  placeholder="Your Answer ..." style="${inputStyle}">
      </div>
    </div>
  %endfor
  </div>

  <div style="margin-top:20px">
    <div class="form-group">
      <button class="btn btn-primary" type="submit" style="float:left;width:100px;margin-left:0px; margin-right:20px;border-radius:0px">Continue</button>
      <div>
        <p>
        By submitting your entry, you accept our <a href="${data.get('tcUrl','#')}" target="_blank">Terms and Conditions</a> and <a href="${data.get('privacyPolicyUrl','#')}" target="_blank">'Privacy Policy'</a>, and, unless you opt out below, you agreed to being contacted by Daily Mail brands and carefully selected third parties.
        </p>
      </div>
    </div>
    %if data.get('contactPrimary').get('active') or data.get('contactThirdParty').get('active'):
      <h5 style="${labelStyle}">I do not wish to be contacted by</h5>
      %if data.get('contactPrimary').get('active'):
      <div class="checkbox">
        <label style="${labelStyle}">
          <input name="optOutPrimary" id="contactPrimary" type="checkbox"><strong>Daily Mail</strong>
        </label>
      </div>
      %endif
      %if data.get('contactThirdParty').get('active'):
      <div class="checkbox">
        <label style="${labelStyle}">
          <input name="optOutThirdParty" id="contactThirdParty" type="checkbox"><strong>Third Parties</strong>
        </label>
      </div>

    %endif
    </div>
  %endif
</form>
%else:
 <span>Error: No Action Url specified for Form</span>
%endif