<div>
	<h4>Required</h4>
	<p class="help-block">Action refers to the URL to the script that processes the form data. Every form needs to have a link to Terms and Conditions that apply to this form.</p>
	<div class="form-group">
		<label for="action">Action</label>
		<input type="text" class="form-control" id="action" placeholder="https://..." data-bind="value:data.action">
	</div>
	<div class="form-group">
		<label for="tcUrl">Terms and Conditions URL</label>
		<input type="text" class="form-control" id="tcUrl" placeholder="http://..." data-bind="value:data.tcUrl">
	</div>
	<div class="form-group">
		<label for="privacyPolicyUrl">Privacy and Policy URL</label>
		<input type="text" class="form-control" id="privacyPolicyUrl" placeholder="http://..." data-bind="value:data.privacyPolicyUrl">
	</div>
	<h4>Optional Form Elements</h4>
	<p class="help-block">Name, Surename and email are included by default. All other parts are optional.</p>
	<div class="form-group">
		<label for="action">Questions</label>
		<div data-bind="foreach:data.questions">
			<div class="input-group">
				<input class="form-control" type="text" data-bind="value:$data.text">
				<span class="input-group-addon"><button class="close" data-bind="click:$parent.removeQuestion">&times;</button></span>
			</div>
		</div>
		<button class="btn btn-primary" data-bind="click:addQuestion">Add Question</button>
	</div>
	<table class="table table-condensed">
		<thead>
          <tr>
            <th>Field</th>
            <th>Display</th>
            <th>Required</th>
          </tr>
        </thead>
        </tbody>
			<tr>
				<td>Title (Mr/Mrs)</td>
				<td><input type="checkbox" data-bind="checked:data.title.active"></td>
				<td></td>
			</tr>
			<tr>
				<td>Date Of Birth</td>
				<td><input type="checkbox" data-bind="checked:data.dateOfBirth.active" ></td>
				<td><input type="checkbox" data-bind="checked:data.dateOfBirth.required" ></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><input type="checkbox" data-bind="checked:data.address.active"></td>
				<td><input type="checkbox" data-bind="checked:data.address.required"></td>
			</tr>
			<tr>
				<td>Postcode</td>
				<td><input type="checkbox" data-bind="checked:data.postcode.active"></td>
				<td><input type="checkbox" data-bind="checked:data.postcode.required"></td>
			</tr>
			<tr>
				<td>Phone</td>
				<td><input type="checkbox" data-bind="checked:data.phone.active"></td>
				<td><input type="checkbox" data-bind="checked:data.phone.required"></td>
			</tr>
			<tr>
				<td>Contact by Daily Mail</td>
				<td><input type="checkbox" data-bind="checked:data.contactPrimary.active"></td>
				<td></td>
			</tr>
			<tr>
				<td>Contact by Third Party</td>
				<td><input type="checkbox" data-bind="checked:data.contactThirdParty.active"></td>
				<td></td>
			</tr>
        </tbody>
	</table>



	
	<h4>Styling</h4>
	<div class="form-horizontal">
		<div class="form-group colorpicker">
	        <label class="control-label col-xs-4">Label Color</label>
	        <div class="col-xs-8 input-group input-group">
	        	<input type="text" class="form-control" class="form-control" placeholder="Username" data-bind="colorpicker:data.labelColor" data-color-format="rgba">
	        	<span class="input-group-addon"><i data-bind="style:{ backgroundColor: data.labelColor }"></i></span>
        	</div>
      	</div>
		<div class="form-group colorpicker">
	        <label class="control-label col-xs-4">Input Background</label>
	        <div class="col-xs-8 input-group">
	        	<input type="text" class="form-control" class="form-control" placeholder="Username" data-bind="colorpicker:data.inputBackgroundColor" data-color-format="rgba">
	        	<span class="input-group-addon"><i data-bind="style:{ backgroundColor: data.inputBackgroundColor }"></i></span>
        	</div>
      	</div>
      	<div class="form-group colorpicker">
	        <label class="control-label col-xs-4">Input Text Color</label>
	        <div class="col-xs-8 input-group">
	        	<input type="text" class="form-control" class="form-control" placeholder="Username" data-bind="colorpicker:data.inputColor" data-color-format="rgba">
	        	<span class="input-group-addon"><i data-bind="style:{ backgroundColor: data.inputColor }"></i></span>
        	</div>
      	</div>
	</div>


</div>