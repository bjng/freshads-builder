<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
      xmlns:tal="http://xml.zope.org/namespaces/tal">
<head>
  <title>Freshads Login</title>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
  <meta name="keywords" content="python web application" />
  <meta name="description" content="pyramid web application" />

  <link rel="shortcut icon" href="/static/favicon.ico" />
  <link rel="stylesheet" href="/static/css/font-awesome.min.css" rel="stylesheet" />  
  <link rel="stylesheet" href="/static/css/bootstrap.min.css" type="text/css" media="screen" charset="utf-8" />  
  <link rel="stylesheet" href="/static/css/main.css" type="text/css" media="screen" charset="utf-8" />
  <script type="application/x-javascript" src="/static/js/require.js"></script>
     <script type="application/x-javascript" src="/static/js/lib/jquery-2.0.3.min.js"></script>
  <script type="application/x-javascript" src="/static/js/app.js"></script>

  <script type="text/javascript">
      require(['formValidator'],function(formValidator){
          $(document).on('submit',function(e){
              var valid = formValidator.validateForm( $(this) );
              if(!valid)
                e.preventDefault();
              return valid;
          });
      })
  </script>
</head>
<body>
  
  <div id="app"> 
    <div class="col-4-12">&nbsp;</div> 
    <div class="col-4-12 login center">
      <div class="logo">
        <img src="/static/img/fa-logo.png" />
      </div>  
      <form action="${url}" method="post" novalidate>
        <input type="hidden" name="came_from" value="${came_from}"/>

        <input class="form-control"  type="text" name="login" pattern="[\w-\.]+@(?:[\w-]+\.)+[a-zA-Z]{2,4}" data-content="Email incorrect!" data-placement="top" value="${login}" placeholder="Email@please.com" /><br/>
        <input class="form-control" type="password" name="password" value="${password}" placeholder="Password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" data-content="Password incorrect!" data-placement="top" />

        <input class="btn btn-primary login-button" type="submit" name="form.submitted" value="Log In"/>
      </form>
      %if message:
        <div class="alert alert-warning"><strong>Incorrect Password or User unknown</strong> Please try again.</div>
      %endif
      <p>Forgotten Password?
      Contact <a href="mailto:helpdesk@anmedia.co.uk?subject=Freshadsbuilder Password Request">helpdesk@anmedia.co.uk</a></p>
      <!--p><a href="forgot-password">Forgot your password?</a></p>
      <small>If you are not a registered user please contact <a href="mailto:xxxxx@xxxxxx.com">xxxxx@xxxxxx.com</a-->
    </div>
    <div class="col-4-12">&nbsp;</div> 
  </div>
  
</body>
</html>