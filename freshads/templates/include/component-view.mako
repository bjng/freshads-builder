<%
ad = env.get( 'ad' )
staticRoot = env.get('static_root')

c = env.get('component')
component_controller_root = env.get('component_controller_root','')
component_controller = '%s%s/%s-view.js' %( component_controller_root, c.get( 'type' ), c.get( 'type' ) )
template_path = 'freshads:templates/components/%s/%s-view.mako' %( c.get( 'type' ), c.get( 'type' ) )
css_path = '%scomponents/%s/main.css' %( staticRoot, c.get( 'type' ))
style = c.get('style')

import os
from pyramid.path import AssetResolver

componentsRoot = AssetResolver('freshads').resolve('static/components/').abspath();
hasCSS = os.path.exists( componentsRoot + c.get('type') + '/main.css' )
hasController = os.path.exists( componentsRoot + c.get('type') + '/' + c.get('type')+ '-view.js' )



%>
<div class="component" id="${ c.get('id') }" name="${ c.get('name') }" style="top:${ style.get('top') };left:${ style.get( 'left' ) };width:${style.get( 'width') };height:${style.get('height')};position:absolute;display:block;"><%include file="${template_path}"/></div>
% if hasCSS:
<link rel="stylesheet" href="${ css_path }" type="text/css" media="screen" charset="utf-8" />
% endif
% if hasController:
<script type="text/javascript">
	require( [ '${ component_controller }' ], function( c ){ new c( "#${str( c.get( 'id') ) }" );});
</script>
% endif
