
		</div>
		<div class="btn-group support">
			<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-question"></i></button>
				<div class="dropdown-menu stopPropagation">
				<!-- Help section -->
				<div class="help-expanded">
				    <div class="help-top">
				      <h2>Help</h2>
				      <p>Please contact <a href="mailto:support@fresh-ads.com">support@fresh-ads.com</a></p>
				    </div>
				    <!--
				    <div class="help-list">
				      <ul>
				        <li class="link1">link1</li>
				        <li class="link2">link2</li>
				        <li class="link3">link3</li>
				        <li class="link4">link4</li>
				      </ul>
				    </div>
				    <div id="link1" class="help-section">
				      <h3>Header</h3>
				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
				      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
				    </div>
				    <div id="link2" class="help-section">
				      <h3>Header</h3>
				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
				      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
				    </div>
				    <div id="link3" class="help-section">
				      <h3>Header</h3>
				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
				      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>    </div>
				    <div id="link4" class="help-section">
				      <h3>Header</h3>
				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac magna non augue porttitor scelerisque ac id diam. Mauris elit velit, lobortis sed interdum at, vestibulum vitae libero.</p>
				      <div class="toTop-cont"><span class="toHelpTop" title="Go to top"><i class="fa fa-angle-up"></i></span></div>
				    </div> -->
				</div>
			</div>
			</div>
			<a id="logout" class="btn btn-default logout" href="/logout"><i class="fa fa-sign-out"></i></a>
			
		</div>  
		  <!--
		  <script type="text/javascript">
		    // SCROLL FUNCTION 
		    function scrollToElement(selector, time, verticalOffset) {
		        time = typeof(time) != 'undefined' ? time : 1000;
		        verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		        element = $(selector);
		        offset = element.offset();
		        offsetTop = offset.top + verticalOffset;
		        $('html, .help-expanded').animate({
		            scrollTop: offsetTop
		        }, time);
		    }
		    
		    $('.help-list ul li').click(function () {
		      var targetId = '#' + $(this).attr('class');
		        scrollToElement(targetId);
		    });

		    $('.toHelpTop').click(function () {
		        scrollToElement('#help-top');
		    });
		  </script> -->
		<div id="confirm" class="modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Delete This</h4>
					</div>
					<div class="modal-body">
						Are you sure about deleting this?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger button-left" data-dismiss="modal">No. I need this!</button>
						<button type="button" class="btn btn-success">Sure! Go ahead.</button>
					</div>
				</div>
			</div>
		</div>
</body>
</html>