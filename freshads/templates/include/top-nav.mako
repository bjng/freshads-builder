% if user.get('group') == "superuser" or user.get('group') == "producermanager":
<div class="top-nav">
	<ul>
	  <li title="Menu"><a><i class="fa fa-bars"></i></a></li>
	  <li title="Home"><a href="/"><i class="fa fa-home"></i></a></li>
	  % if user.get('group') == 'superuser':
	  <li title="Users"><a href="/users"><i class="fa fa-users"></i></a></li>
	  % endif
	  <li title="Templates"><a href="/templates"><i class="fa fa-files-o"></i></a></li>
	  <li title="Clients"><a href="/clients"><i class="fa fa-globe"></i></a></li>
	</ul>
</div>
% endif