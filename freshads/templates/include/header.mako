<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
  <title>FreshAds Builder</title>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
  <meta name="keywords" content="python web application" />
  <meta name="description" content="pyramid web application" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0, user-scalable=no">
  <link rel="shortcut icon" href="/static/favicon.ico" /> 
  <link rel="stylesheet" href="/static/css/font-awesome.min.css" type="text/css" media="screen" charset="utf-8" />
  <link rel="stylesheet" href="/static/css/bootstrap.min.css" type="text/css" media="screen" charset="utf-8" />
  <link rel="stylesheet" href="/static/css/main.css" type="text/css" media="screen" charset="utf-8" />
  <link rel="stylesheet" href="/static/css/bootstrap-colorpicker.min.css" type="text/css" media="screen" charset="utf-8" />
  <link rel="stylesheet" href="/static/css/datepicker.css" type="text/css" media="screen" charset="utf-8" /> 
  <link rel="stylesheet" href="/static/css/redactor.css" type="text/css" media="screen" charset="utf-8" /> 
  <script type="application/x-javascript" src="/static/js/require.js"></script>
    <script type="application/x-javascript" src="/static/js/lib/jquery-2.0.3.min.js"></script>
  <script type="application/x-javascript" src="/static/js/app.js"></script>

</head>

<body>

	<div id="appView">
    <div id="messages"></div>
    
