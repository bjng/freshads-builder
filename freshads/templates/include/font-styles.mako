<style type="text/css">
	<% 	def camelToDash( key ):
			import re
			return re.sub('([A-Z]+)', r'-\1',key).lower()
	%>
	%for f in env.get('fonts'):
		@font-face{ font-family:${ str.split( f.get( 'filename' ), '.' )[0]};
			src: url("${ env.get('assets_root') + f.get('filename')}"); }
	%endfor
	%for s in env.get('ad').get('styleSheet'):
		.freshad ${s.get('name')}{
			%for k,v in s.get('style').items():
				${ camelToDash( k ) }:${ v };
			%endfor
		}
		
	%endfor
</style>