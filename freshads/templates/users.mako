<%include file="freshads:templates/include/header.mako"/>
<%include file="freshads:templates/include/top-nav.mako"/> 

<div id="#users" class="app-container">
	<header class="site-header">
		<div class="col-4-12 cell">
			<h1>Users</h1>
		</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<a class="logo" href="/">
				<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
			</a>
		</div>
	</header>

	<div class="content full-width">
		<div class="create-filter">
			<div class="col-4-12">&nbsp;</div>	
			<div class="col-4-12 center">
				<button type="button" class="btn btn-success" data-bind="click:createUser" data-toggle="modal" data-target="#editUser">
					Create New User <i class="icon-plus"></i>
				</button>
			</div>
			<div class="col-4-12">&nbsp;</div>
		</div>

		<div class="table-list col-10-12">
			<table class="table table-striped">
				<thead>
					<th><a data-filter="name" data-bind="click:sortTable">Username <span class="fa fa-sort"></span></a></th>
					<th><a data-filter="email" data-bind="click:sortTable">User Email <span class="fa fa-sort"></span></a></th>
					<th><a data-filter="group" data-bind="click:sortTable">Group <span class="fa fa-sort"></span></a></th>
					<th class="actions">Actions</th>
				</thead>
				<tbody data-bind="foreach:users">
					<tr>
						<td data-bind="text:name"></td>
						<td data-bind="text:email"></td>
						<td data-bind="text:group"></td>
						<td class="actions">
							<button class="btn btn-settings" data-bind="click:$parent.editUser" data-toggle="modal" data-target="#editUser"><i class="fa fa-cog"></i></button>
							<button class="btn btn-delete" data-role="delete" data-bind="click:$parent.removeUser"><i class="fa fa-trash-o"></i></button>
						</td>
					</tr>
				</tbody>	
			</table>
		</div>



		<div id="editUser" class="modal" >
			<div class="modal-dialog" data-bind="if:selectedUser">
				<div class="modal-content">
					<form role="form" novalidate data-bind="submit:saveUser">
						<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" data-bind="text: selectedUser()._id ? 'Edit User' : 'New User'">Edit User</h4>
					     </div>
						
						<div class="modal-body" data-bind="with:selectedUser">
							<div class="form-group">
								<label for="email">Email (used as login name)<span class="required">*</span></label>

								<input type="email" class="form-control" id="email" data-bind="value:email,valueUpdate:'keyup'" placeholder="Email" pattern="[\w-\.]+@(?:[\w-]+\.)+[a-zA-Z]{2,4}" data-content="Please enter a valid email address" data-placement="top" required>
							</div>

							<div class="form-group">
								<label for="name">Name<span class="required">*</span></label>

								<input type="text" class="form-control" id="name" placeholder="Enter Name" data-bind="value:name,valueUpdate:'keyup'" pattern="^[a-zA-Z]+[\sa-zA-Z]{1,30}$" data-content="Please enter a username of more than two characters" data-placement="top" required>
							</div>

							<div class="form-group">
								<label for="password">Password<span class="required">*</span></label>
								<input type="password" class="form-control" id="password" data-bind="value:password,valueUpdate:'keyup'" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" data-content="Please choose a password which contains at least eight characters, a capital letter and a digit" data-placement="top" required>
							</div>

							<div class="form-group">
								<label for="group">Group<span class="required">*</span></label>
								<select id="group" class="form-control" data-bind="value:group" required>
									<option value="producer">Producer</option>
									<option value="producermanager">Production Manager</option>
									<option value="accountmanager">Account Manager</option>
									<option value="superuser">Superuser</option>
								</select>
							</div>		
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</form >
				</div>
			</div>
		</div>

	</div>
	
</div>

<script type="text/javascript">
	require(['users'],function(C){
		new C('#users')
	})
</script>

<%include file="freshads:templates/include/footer.mako"/>