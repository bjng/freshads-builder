<ul class="dropdown-menu" role="menu">
	
	% if clients:
		<li role="presentation" class="dropdown-header">Clients By Name</li>
		% for client in clients:
		<li role="presentation"><a  href="/clients/${client.get('id')}">${client.get('name')}</a></li>
		% endfor

	% endif

	% if campaigns:
		<li role="presentation" class="dropdown-header">Brand By Name</li>
		% for campaign in campaigns:
		<li role="presentation"><a  href="/campaigns/${campaign.get('id')}">${campaign.get('name')}</a></li>
		% endfor
	% endif

	% if clients:
		<% clients.rewind() %>
		<li role="presentation" class="dropdown-header">Clients By Id</li>
		% for client in clients:
		<li role="presentation"><a  href="/clients/${client.get('id')}">${client.get('id')}</a></li>
		% endfor
	% endif

	% if campaigns:
		<% campaigns.rewind() %>
		<li role="presentation" class="dropdown-header">Brand By Id</li>
		% for campaign in campaigns:
		<li role="presentation"><a href="/campaigns/${campaign.get('id')}">${campaign.get('id')}</a></li>
		% endfor
	% endif






</ul>