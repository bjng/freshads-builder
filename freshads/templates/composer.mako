<%include file="freshads:templates/include/header.mako"/>
<link rel="stylesheet" href="${request.static_url('freshads:static/css/composer.css')}" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="${request.static_url('freshads:static/bundle/main.css')}" type="text/css" media="screen" charset="utf-8" />

<div id="${ data.get('id') }" class="composer">
  
  <div id="componentEditor" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Edit Component</h4>
        </div>
        <div class="modal-body" data-bind="stopBinding:true"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-right" data-bind="click:updateComponent,css: { disabled:  modelLocked }" >Save changes</button>
          <button type="button" class="btn btn-remove" data-bind="click:removeComponent, css: { disabled: modelLocked }" data-role="delete"><i class="fa fa-trash-o"></i></button>
        </div>
      </div>
    </div>
  </div>


  <div id="adboard">
    <div id="preview" class='freshad'></div>
    <canvas data-bind="visible:runMode"></canvas>
    <div id="componentsDropdown" class="dropdown">
      <div class="dropdown-menu" role="menu"  data-bind="foreach: componentsPrototypes">
        <a class="list-group-item sm" data-bind="attr:{ id : _id.$oid }, text:verboseName,click:$parent.createComponent"></a>
      </div>
    </div>
  </div>


  <div class="overlay top left" data-bind="if:adModel()">
    <!--p><small>Campaign Name: ${ parent.get('name') }<br />
    Campaign ID: ${ parent.get('id') }</small></p--!>
    <h2>${ data.get('name') }</h2>
      <div class="dropdown">
        <a id="more-ad-info" role="button" data-toggle="dropdown" href="#">More Ad Info <span class="fa fa-info-circle"></span></a>
        <ul role="menu" class="dropdown-menu expand-ad-info" aria-labelledby="more-ad-info">
          <li>ID: ${data.get('id')}</li>
          <li>Brand: ${ parent.get('name') }</li>
          <li>Campaign: ${ data.get('campaign') }</li>
          <li>BookingId: ${data.get('bookingId')}</li>
          <li>Insertion Date: <span data-bind="text:insertionToString"></span></li>
          <li>Modified: <span data-bind="text:modifiedToString"></span></li>
          <li>Modified By: ${ data.get('modifiedBy',{}).get('name','') }</li>
        </ul>
      </div>
  </div>


  <div class="overlay top center">
      <div class="smart-search">
        Please Enter Your Search below
        <div class="dropdown">
            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
        </div>
      </div>
  </div>


  <div  class="overlay top right" data-bind="if:adModel">

    <div class="btn-group-vertical">
      
      <div class="btn-group pull-right">

        <button type="button" class="btn btn-default dropdown-toggle" data-bind="css: { active: showGuides},  click:toggleGuides">Guides <i class="fa fa-th-large"></i>
        </button> 
        <div class="btn-group combo" data-bind="css: { active: showGrid}">
          <button type="button" class="btn btn-default dropdown-toggle" data-bind="click:toggleGrid">Grid <i class="fa fa-th"></i>
          </button>
          <div data-bind="with:adModel().data">
            <input type="number"  class="form-control" maxlength="3" min="0" max="100" step="5" data-bind="value:grid"></input>
          </div> 
        </div>
          
      </div>

      <div class="btn-group pull-right">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          Ad Settings <i class="fa fa-cog"></i>
        </button>
      	<div class="dropdown-menu settings-dropdown-menu stopPropagation" data-bind="if:adModel">
          <form   class="form-horizontal" role="form">
            <fieldset data-bind="with:adModel().style">
              <div class="form-group">
                <label class="control-label col-lg-6">Width</label>
                <div class="col-lg-6">
                  <input type="number" class="form-control" data-bind="value:width,valueUpdate:'keyup'" placeholder="0"/>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-6">Height</label>
                <div class="col-lg-6">
                  <input type="number" class="form-control" data-bind="value:height,valueUpdate:'keyup'" placeholder="0"/>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-6">Background Color</label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" data-bind="colorpicker:backgroundColor" placeholder="rgba(255,255,255,1)" data-color-format="rgba"/>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

      <div class="btn-group-vertical pull-right">

        <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-bind="click:toggleRun">Run Ad <i class="fa fa-rocket"></i>
          </button>
        </div>
        <div class="btn-group pull-right" >
          <button type="button" class="btn btn-default dropdown-toggle ad-share" data-toggle="dropdown">
            Share Proof <i class="fa fa-eye"></i>
          </button>
          <ul class="dropdown-menu share-dropdown-menu">
              <li role="presentation" data-bind="with:proof">
                <a href="#" target="blank" data-bind="text:'.../proof/'+id,attr:{href:'/proof/' + id}"></a>
              </li>
              <li>
                <a type="button" class="stopPropagation pull-right" data-bind="click:generateProof">Generate URL <i class="fa fa-refresh"></i>
                </a>
              </li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-bind="click:bundle">Download <i class="fa fa-arrow-circle-o-down"></i>
          </button>
        </div>

      </div>

   
      <div class="btn-group">
        <a href="/campaigns/${parent.get('id')}" class="btn btn-danger">Exit</a>
        <button type="button"   class="btn btn-primary pull-right" data-bind="click:save,css: { disabled: ! modelChanged() || modelLocked }">Save</button>
      </div>
      

    </div>

  </div>

 
  <div class="overlay bottom left" data-bind="visible:adModel">
    <div class="btn-group">
      <div class="btn-group dropup bucket" >
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cloud-upload"></i></button>
          <div id="bucket" class="dropdown-menu stopPropagation" data-bind="with:bucket">
              <div class="badge" data-bind="text:totalToString" ></div>
              <div class="upload">
                <input multiple="true" type="file"></input>
                <button type="button" class="btn  btn-primary btn-lg">Upload Assets <i class="icon-cloud-upload"></i></button>
              </div>
              <div data-bind="visible:hasFiles">
                <p>Please drag files to the screen or click the button above to upload files</p>
              </div>
              <div class="scroll">
                <ul class="list-group file-list" data-bind="foreach:groupedFiles">
                  <li class="list-group-item" data-bind="attr:{id:_id.$oid},css: { sequence: 'children' in $data}">
                    <span class="item-name" data-bind="text:filename,click:$parent.select"></span>
                    <button type="button" class="toggleSequence" data-bind="click:$parent.toggleSequence"><i class="fa fa-forward"></i></button>
                    <span class="item-size" data-bind="text:$parent.bytesToUnit( length )"></span>
                    <button type="button" class="close  " aria-hidden="true" data-bind="click:$parent.remove"><i class="fa fa-times"></i></button>
                    <div class="children-wrapper" style="display:none" data-bind="if: $parent.hasChildren($data)">
                      <ul class="list-group file-list" data-bind="foreach:children" >
                        <li class="list-group-item" data-bind="attr:{id:_id.$oid},css: { sequence: 'children' in $data}">
                        <span class="item-name" data-bind="text:filename,click:$parents[1].select"></span>
                        <span class="item-size" data-bind="text:$parents[1].bytesToUnit( length )"></span>
                        <button type="button" class="close  " aria-hidden="true" data-bind="click:$parents[1].remove"><i class="fa fa-times"></i></button>
                      </ul>
                    </div>
                  </li>
                </ul>
                <ul class="list-group running-list" data-bind="foreach:running">
                  <li class="list-group-item">
                    <div class="progress-bar" data-bind="style:{width: progress() * 100 + '%'}"></div>
                    <span  class="item-name" data-bind="text:filename"></span>
                    <span class="item-size" data-bind="text:$parent.bytesToUnit( length )"></span> 
                  </li>
                </ul>
                <ul class="list-group queue-list" data-bind="foreach:queue">
                  <li class="list-group-item">
                    <span class="item-name" data-bind="text:filename"></span>
                    <span class="item-size" data-bind="text:$parent.bytesToUnit( length )"></span>
                  </li>
                </ul>
              </div>
          </div>
      </div>

      <div  class="btn-group dropup">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-font"></i></button>
        <div id="fontManager" class="dropdown-menu stopPropagation" data-bind="with:fontManager">

          <div data-bind="with:tagStyle">

            <div >
              <div class="well">
              <span data-bind="text:name"></span>: 
              <input data-bind="value:$parent.placeHolder,style:{ fontFamily:style.fontFamily,fontSize:style.fontSize,fontWeight:style.fontWeight,letterSpacing:style.letterSpacing,}"></input>
              </div>
            </div>

            <form class="form-horizontal">
              <div class="form-group">
                <label class="col-lg-6 control-label">Font Size</label>
                <div class="col-lg-6">
                  <input data-bind="value:style.fontSize,valueUpdate: 'keyup'" type="text" class="form-control input-sm" placeholder="20px" size="5" maxlength="5" pattern="[0-9A-Za-z]+"></input>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-6 control-label">Line Height</label>
                <div class="col-lg-6">
                  <input data-bind="value:style.lineHeight,valueUpdate: 'keyup'" type="text" class="form-control input-sm" placeholder="20px" size="5" maxlength="5" pattern="[0-9A-Za-z]+"></input>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-6 control-label">Letter Spacing</label>
                <div class="col-lg-6">
                  <input data-bind="value:style.letterSpacing,valueUpdate: 'keyup'" type="text" class="form-control input-sm" placeholder="1px" size="5" maxlength="5" pattern="[0-9A-Za-z]+"></input>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-6 control-label">Color</label>
                <div class="col-lg-6">
                  <input data-bind="value:style.color,valueUpdate: 'keyup'" type="text" class="form-control input-sm" size="7" placeholder="#000000" maxlength="7" pattern="[0-9A-Fa-f#]+"></input>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-6 control-label">Weight</label>
                <div class="col-lg-6">
                  <select class="form-control col-lg-4" data-bind="value:style.fontWeight,options:$parent.weights"></select>
                </div>
              </div>
              <div class="form-group"> 
                <label class="col-lg-6 control-label">Font Family</label>
                <div class="col-lg-6"> 
                  <select class="form-control" data-bind="value:style.fontFamily,options:$parent.fonts,optionsValue:'name'"></select>
                </div>
              </div>   
            </form>
          </div> 
          <span class="help-block">Select a tag:</span>
          <div class="btn-group btn-group-justified" data-bind="foreach:tags">
            <a role="button" class="btn btn-default" data-bind="text:$data,click:$parent.selectedTag">
            </a>
          </div>
          
        </div>
     
      </div>
      
      <div class="btn-group dropup">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-expand"></i>
        </button>
        <div class="dropdown-menu">
          <form role="form">

            <div class="form-group">
              <h4 for="group">Assign Expandable</h4>
              <span class="help-block">Select another Ad from this campaign to be bundled with this ad. Drop the created URL in any URL field.</span>
              <div data-bind="with:selectedExpandable">
                <div data-bind="text:$data.$oid + '.html'" class="well"></div>
              </div>
              <select id="group" class="form-control" value="526a44b2e83302559bf0c7d1"  data-bind="options:adSiblings,optionsText:'name',optionsValue:'_id',value:selectedExpandable">
              </select>
            </div>

          </form>
         
        </div>
      </div>

  </div>


</div>



<script type="text/javascript">
	require(['app','composer'],function(app,C){
		new C("#${ data.get('id')}")
	})
</script>

<%include file="freshads:templates/include/footer.mako"/>
