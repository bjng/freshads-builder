<% 
	ad = env.get('ad') 
	style = ad.get('style',{})
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
	<link rel="stylesheet" href="/static/bundle/main.css" type="text/css" media="screen" charset="utf-8" />
	<script type="application/x-javascript" src="/static/js/require.js"></script>
	<script type="application/x-javascript" src="/static/js/lib/jquery-2.0.3.min.js"></script>
	<script type="application/x-javascript" src="/static/bundle/fa.js"></script>
	<script type="text/javascript">
		fa('config',{ disabled: true})
	</script>
	<script type="text/javascript">
		var adWidth = ${ style.get( 'width') };
		var scale = adWidth/document.documentElement.clientWidth;

		var viewPortTag=document.createElement('meta');
		viewPortTag.id="viewport";
		viewPortTag.name = "viewport";
		viewPortTag.content = "width=768, initial-scale=" + scale + ", maximum-scale=3, user-scalable=yes";
		document.getElementsByTagName('head')[0].appendChild(viewPortTag);
	</script>
	<%include file="freshads:templates/include/font-styles.mako"/>
</head>

<body style="margin:0px;padding:0px;background-color:#333">
<div style="width:100%">

	<div class="freshad" id="${str( ad.get('_id') )}" style="margin-left:auto;margin-right:auto;position:relative;width: ${ style.get( 'width', 768) }px; height: ${ style.get( 'height', 1024 )}px;background-color:${ style.get('backgroundColor')};overflow:hidden;border: solid 1px">
		% for c in env.get('ad').get( 'components' ):
		<% env[ 'component' ] = c %>
		<%include file="freshads:templates/include/component-view.mako"/>
		% endfor
	</div>
	
</div>
<script type="application/x-javascript" src="/static/bundle/freshad.js"></script>
	
</body>
</html>