<% 
	ad = env.get('ad') 
	style = ad.get('style')
	analytics_url = env.get('analytics_url')
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1"/>
	<link rel="stylesheet" href="main.css" type="text/css" media="screen" charset="utf-8" />

	<script type="application/x-javascript" src="require.js"></script>
	<script type="application/x-javascript" src="jquery-2.0.3.min.js"></script>
	<script type="application/x-javascript" src="fa.js"></script>
	<script type="text/javascript">
		fa('config',{trackingUrl:'${ analytics_url }'});
	</script>
	<script type="text/javascript">
		var adWidth = ${ style.get( 'width') };
		var cw = document.documentElement.clientWidth;
		var scale = cw/adWidth;
		document.getElementById("viewport").setAttribute("content", "width="+adWidth+", initial-scale=" + scale + ", maximum-scale=" + scale + ", user-scalable=1");
	</script>

	<%include file="freshads:templates/include/font-styles.mako"/>
</head>
<body style="overflow:hidden;margin:0px;padding:0px;width:100%;height:100%;background-color:${ style.get('backgroundColor')}">
<div class="freshad" id="${str( ad.get('_id') )}">
	<div class="landscape-escape">
		<div></div>
	</div>
	<div class="content" style="width: ${ style.get( 'width') }px; height: ${ style.get( 'height' )}px;background-color:${ style.get('backgroundColor')};overflow:hidden">

	% for c in env.get('ad').get( 'components' ):
	<% env[ 'component' ] = c %>
	<%include file="freshads:templates/include/component-view.mako"/>
	% endfor

	</div>
</div>
<script type="application/x-javascript" src="freshad.js"></script>
</body>
</html>