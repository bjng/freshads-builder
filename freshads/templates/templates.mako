<%
	# disallowCreate is also controls the duplicate button
	group             = user.get('group')
	disallowCreate    = group == 'accountmanager' or group == 'producer' 
	disallowSettings  = group == 'accountmanager' or group == 'producer' 
	disallowDelete    = group == 'producer' or group == 'accountmanager'
%>
<%include file="freshads:templates/include/header.mako"/>
<%include file="freshads:templates/include/top-nav.mako"/> 
	

<div id="#templates" class="app-container">
	% if user.get('group') == "superuser":
		<div class="back-to-area" style="position: relative;left: 40px; width:30%;"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	% elif user.get('group') == "producermanager":
		<div class="back-to-area" style="position: relative;left: 40px; width:30%;"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	% else:
		<div class="back-to-area"><h2><a class="back-to" href="/clients/"><span class="fa fa-reply"></span> Clients</a></h2></div>
	%endif
	
	<header class="site-header full-width">
		<div class="col-4-12 cell">
				<h1></h1>
				<span></span>
		</div>
		<div class="col-4-12 cell">
			<div class="smart-search">
		        Please Enter Your Search below
		        <div class="dropdown">
		            <input class="form-control" data-toggle="dropdown" placeholder="Name, ID or Booking-ID"></input>
		        </div>
	      	</div>
		</div>
		<div class="col-4-12 cell right">
			<a class="logo" href="/">
        		<img src="${request.static_url('freshads:static/img/fa-logo.png')}" />
			</a>
		</div>
	</header>

	<div class="content full-width">
		<div class="create-filter">
			
			<div class="col-4-12">&nbsp;</div>
			<div class="col-4-12 center">
				
				% if not disallowCreate :
				<button type="button" class="btn btn-success" data-bind="click:createItem" data-toggle="modal" data-target="#editItem">
					Create New Template <i class="fa fa-plus"></i>
				</button>
				% else:
					&nbsp;
				% endif

			</div>

			<div class="col-4-12 right">
				<label>Filter By</label>
				<div class="btn-group">
				    <button data-filter="modified" type="button" class="btn btn-default" data-bind="click:setFilter">DATE</button>
					<button data-filter="name" type="button" class="btn btn-default" data-bind="click:setFilter">A-Z</button>
				</div>
			</div>
		</div>

		<div class="tile-list col-10-12" data-bind="foreach:items">
			<div class="tile temp">
				
				% if not disallowSettings :
				<button class="btn btn-settings" data-bind="click:$parent.editItem" data-toggle="modal" data-target="#editItem"><i class="fa fa-cog"></i></button>
				% endif

				% if not disallowDelete : 
				<button class="btn btn-delete" data-bind="click:$parent.removeItem" data-role="delete"><i class="fa fa-trash-o"></i></button>
				% endif

				<div class="template-title">
					<h1 data-bind="text:name, attr:{title:name}">Item</h1>
				</div>

				<div class="template-device-area">
					<div class="device-border">
						<span class="" data-bind="style:{width:$parent.getAdWidth($data),height:$parent.getAdHeight($data)}"></span>
					</div>
				</div>

				<div class="template-info adformat">
					w<span data-bind="text:style.width"></span> x 
					h<span data-bind="text:style.height"></span>
				</div>

			</div>
		</div>

		<div id="editItem" class="modal" >
			<div class="modal-dialog" data-bind="if:selectedItem" >
				<div class="modal-content">
					<form role="form" novalidate data-bind="submit:saveItem">
						<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" data-bind="text: selectedItem()._id ? 'Edit Template' : 'New Template'">Edit Template</h4>
					     </div>
					
						<div class="modal-body" data-bind="with:selectedItem">
							<div class="form-group">
								<label for="name">Template Name</label>
								<input type="text" class="form-control" id="name" placeholder="Enter Name" data-bind="value:name,valueUpdate:'keyup'" pattern="^\w+[\s\w]{1,30}\w$" data-content="Please enter a name of more than two characters" data-placement="top" required />
							</div>
							<div class="form-group">
								<label for="width">Width</label>
								<input type="text" class="form-control"  id="width" placeholder="768" data-bind="value:style.width,valueUpdate:'keyup'" pattern="^[0-9]{3,4}$" data-content="Please enter a number larger than 99" data-placement="top" required />
							</div>
							<div class="form-group">
								<label for="height">Height</label>
								<input type="text" class="form-control" id="height" placeholder="1024" data-bind="value:style.height,valueUpdate:'keyup'" pattern="^[0-9]{3,4}$" data-content="Please enter a number larger than 99" data-placement="top" required />
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	require(['templates'],function(C){
		new C( "#templates" )
	})
</script>

<%include file="freshads:templates/include/footer.mako"/>