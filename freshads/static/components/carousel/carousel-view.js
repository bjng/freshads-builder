define( [], function(){
	
	if(! ('easeOutQuad' in $.easing) ){
		$.easing.easeOutQuad = function (x, t, b, c, d) {
			return -c *(t/=d)*(t-2) + b;
		};

	}

	return function( _view ){


		var self          = this;

		var _view          = $(  _view );
		var _carousel      = $('ul', _view);
		var _loop         = true;
		var _itemWidth    = 0;
		var _currentIndex = 0;
		var _currentElement = null;

		//event handler switch for desktop and mobile
		var _hasTouch = 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch;
		var _startEvent = _hasTouch ? 'touchstart': 'mousedown';
		var _endEvent = _hasTouch ? 'touchend': 'click';
		var _leaveEvent = _hasTouch ? 'touchleave' : 'mouseleave';
		var _moveEvent = _hasTouch ? 'touchmove' : 'mousemove';

		//resize handler
		_view.on('changed', updateDisplay );
		$( window ).resize( updateDisplay );

		//item flicker sometimes if these css are not set
		/*_carousel.children('li').css({
			'-webkit-backface-visibility' : 'hidden', 
			'-moz-backface-visibility' : 'hidden',
			'-ms-backface-visibility'  : 'hidden'
		})
	
		*/

		
		_view.on(_startEvent, function(e) {

			e.preventDefault();
			_currentElement = _carousel.children('li').get()[0];

      		var cx = _getEventX(e),
    		prependOffset=0,
    		dx=0, 
    		previous=false,
    		isSwipe = false;
   


      		$(document).on(  _moveEvent, function (e){
      			e.isSwipe = true;
	        	e.preventDefault();

				dx = _getEventX(e) - cx; 
	        	
	        	previous = dx>0;
	        	var _carouselPos = _carousel.position;
	        	if(previous){
	        		if( $(_currentElement).index() == 0 ){
	        			prependOffset -= _itemWidth;
	        			_carousel.prepend( _carousel.children('li').last()  );
      				}
	        	}
	        	else{
	        		if( $(_currentElement).index() != 0 ){
	        			prependOffset=0;
	        			_carousel.append( _carousel.children('li').first() );
	        		}
	        	}
	        	_animate( { 'left': (dx + prependOffset) + 'px' },0 );// translate3d(' + (dx + prependOffset) + 'px,0,0)',0 ); 
	      	})

	      	_view.on(_endEvent + ' ' + _leaveEvent,function(e){
	      		if(isSwipe){
	      			e.preventDefault();
	      			e.stopImmediatePropagation();
	      		}
	      		isSwipe = false;
	      		
	      		$(document).off( _moveEvent );
	      		_view.off( _endEvent + ' ' + _leaveEvent );

	      		e.preventDefault();
	      		var moved = Math.abs(dx) > 0;
	    		if( moved ){

		      		if(previous){
		      			_setIndex(_currentIndex-1);
		      			_animate( {left:'0px'}, 200);
		      		}
		      		else{
		      			_setIndex(_currentIndex+1);

		      			_animate( {left: - _itemWidth + 'px'}, 200, function(){
		      				_carousel.append( _carousel.children('li').first() );
		      				_animate( {left:'0px'},0);

		      			});
		      		}
		      	}
		      	else if( e.target.tagName.toUpperCase() == 'A' ) {
		      		var url = $(e.target).attr('href');
		      		if(url)
		      			window.open( url, '_blank' );
		      	}
		      	

	     		
	     	
	      	})
	    })

		function _getEventX( e ){
			if( 'clientX' in e)
				return e.clientX;
			else if( 'touches' in e )
				return e.touches[0].clientX;
			else if( 'originalEvent' in e && 'changedTouches' in e.originalEvent )
			 	return e.originalEvent.changedTouches[0].clientX;
			else if( 'originalEvent' in e && 'touches' in e.originalEvent )
				return e.originalEvent.touches[0].pageX;

			return 0
		}


		function _setIndex(i){
			if ( ! _loop ) 
				i = Math.max(0,Math.min(i, items.length-1));

			_currentIndex = i;

		}



		function updateDisplay(){
			_itemWidth = _view.width();	
			_carousel.children('li').width(_itemWidth);
			_carousel.width( _carousel.children('li').length * _itemWidth);
		}

		var _isAnimating = false
		function _animate(transform,duration,callback){
			if(_isAnimating) return;
			if(duration == 0){
				//_carousel[0].style.left = transform.left;
				_carousel.css( transform );
				if( callback )
					callback();
			}
			else{
				_isAnimating = true;
				_carousel.animate( transform, duration, 'easeOutQuad', function(){
					_isAnimating = false;
					if( callback )
						callback();
				});
			}
			/*
			var css = {
				'transition': 'all '+ duration +'ms ease-out',
				'-webkit-transition': 'all '+ duration + 'ms ease-out',
	 			'-moz-transition': 'all '+ duration +'ms ease-out',
	  			'-o-transition': 'all '+ duration +'ms ease-out',
	  			'transform': transform,
	  			'-webkit-transform': transform,
	  			'-moz-transform': transform ,
	  			'-o-transform': transform }	;
			_carousel.css(css);
			*/
		

		}



		updateDisplay();
	}

});