define( [ 'lodash','knockout'], function( _,ko ){
	return function( view, data ){

		function Story( item ){
			item = item ? item : {};
			return {
				title: ko.observable( item.title ? item.title : "" ),
				index: item.index ? item.index : 0,
				image: ko.observable( item.image ? item.image : null ),
				videos: ko.observableArray( item.videos ? item.videos : [] ),
				url: ko.observable( item.url ? item.url : "" ),
			}
		}


		var self = this;


		self.data = data;

		$.each(self.data.components, function(i){
			self.data.components[ i ] = new Story( this );
		})


		self.components = ko.observableArray( self.data.components );

		self.createStory = function create(){
			self.components.push( new Story()  );
		}

		if( self.data.components.length == 0 )
			self.createStory();


		self.removeStory = function( item ){
			self.components.remove( item );
		}
		
		ko.applyBindings( self, view );
	}
})
