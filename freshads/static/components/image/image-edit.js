define( ['jquery','lodash','knockout'], function( $,_,ko){

	return function( view, model ){

			var self = this;
			var view = $( view );

			function C(){

				this.image = ko.observable( model.data.image );
				this.url = ko.observable( model.data.url );

				this.image.subscribe( function(img){
					model.data.image = img;
				});
				this.url.subscribe( function(url){
					model.data.url = url
				})

			}
			self.image = model.data.image;
			self.url = model.data.url;

			
			ko.applyBindings( new C(), view[0] );

	}

})