define( [], function(){

	return function( view, model ){
		$('form',view).submit( function(e){

			e.preventDefault();
			e.stopImmediatePropagation();
			
			var formValid = true; //optimistic
			$('input',this).each( function(){
				var input = $(this);
				var val = input.val();
				var pattern = input.attr('pattern');
				var required = input.attr('required');
				var valid   = true;
	

				//if ( name.val() == null ) alert('required');
				var input = $(this);
			
				if( required  && val.length == 0 ){
					valid = false
				}
				else if( pattern && pattern.indexOf('#') == 0){
					var target = $( pattern,view );
					valid = target.val() == val;
				}
				else if(pattern && val.length > 0){
					var match = input.val().match( pattern );
					valid = match && match.length != 0;
				}
				if(valid) return;

				formValid = false;
					
				var tooltip = _createErrorOverlay( this );
				input.addClass('error');

				input.one('mousedown', function(){
					tooltip.remove();
					input.removeClass('error');
				});
				
			});

			if(!formValid){
				console.log('not valid');
				
			}

			


		});
	}


	function _createErrorOverlay( node ){
		var content = $(node).data('content');
		if(!content) return null;

		var input = $(node);
		var ad = $(node).parents('.freshad');
		var adPos = ad.offset();

		var message = $('<div class="message"/>');
			message.html( content );//$(this).data('tooltip');

		var tooltip = $('<div class="tooltip top"/>');
			tooltip.addClass();
			tooltip.append($('<div class="arrow"/>'));
			tooltip.append( message );

			ad.append( tooltip );
		
		var pos = input.offset();
		var css={ left:pos.left - adPos.left, top:pos.top - adPos.top };
		css.left -= .5* ( tooltip.outerWidth() - input.outerWidth());
		css.top -= tooltip.outerHeight();//- tooltip.children('.arrow').height;
		tooltip.css(css);

		return tooltip;

	}

	
});