define( ['lodash','knockout'],function(_,ko){

	return function(view,model){

		//Helper Model for questions, see http://stackoverflow.com/questions/15749572/how-can-i-bind-an-editable-ko-observablearray-of-observable-strings
		var Question = function(text){
		    this.text = ko.observable(text);
		};
		
		Question.prototype.toJSON = function(){
		    return ko.unwrap( this.text );
		};

		//Module definitions

		var self = this;

		//preprocessing and mapping to knockout
		model.data = ko.mapping.fromJS( model.data );

		model.data.questions( _.map( model.data.questions(), function(item){
				return new Question( item );
			}) 
		);

		//public and bindable variables for this module
		self.data = model.data;

		self.addQuestion = function(){
			self.data.questions.push( new Question('') );
		}

		self.removeQuestion = function( question ){
			self.data.questions.remove( question);
		}
		
		//applying of bindings via knockout
		ko.applyBindings( self, $(view)[0] );


	}

});