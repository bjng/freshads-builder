define( [ 'jquery', 'knockout', 'app' ], function( $, ko, app ){
	
	return function(view, model){
		var self = this;
		var view = $(view)

		//var mapping = { ignore: ['components'] };
		model.data = ko.mapping.fromJS( model.data );	
	

		ko.applyBindings( model.data, view[0] );


	}



});