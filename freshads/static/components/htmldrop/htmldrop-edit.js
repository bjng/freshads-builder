define(['jquery','knockout'],function( $,ko ){
	return function(view,model){
		var view = $(view);

		
		model.data = ko.observable( model.data );
		
		this.data = model.data;
		ko.applyBindings( this,view[0]);
	}
})