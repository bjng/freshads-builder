define( [], function(){

	//event mapping
	var _hasTouch = 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch;
	var _moveEvent = _hasTouch ? 'touchmove' : 'mousemove';
	var _startEvent = _hasTouch ? 'touchstart': 'mousedown';
	var _endEvent = _hasTouch ? 'touchend': 'mouseup';



	//helper function to get mouse/touch x on different devicec
	function _getEventX( e ){
		if( 'clientX' in e)
			return e.clientX;
		else if( 'touches' in e )
			return e.touches[0].clientX;
		else if( 'originalEvent' in e && 'changedTouches' in e.originalEvent )
		 	return e.originalEvent.changedTouches[0].clientX;
		else if( 'originalEvent' in e && 'touches' in e.originalEvent )
			return e.originalEvent.touches[0].pageX;

		return 0
	}


	//controller
	return function( id ){
		
		var view = $(id);
		var frames = $( '.list-group-turntable',view).children();

		frames.eq(0).show();

		var turnPixels = 400;


		var _cx,_ct; //prev x, prev t
		var _a=0; //acceleration, time

	

		view.on( _startEvent, function(e){
			_cx = _getEventX(e);
			$(document).on( _moveEvent, rotate )
				.one( _endEvent, function(){
					$(document).off( _moveEvent, rotate );
				});
		})




		
		function rotate( e ){
			e.preventDefault();
			
			var x = _getEventX( e ); 
			_a +=_cx-x;

			var f = _a%turnPixels;

			var fpp = turnPixels/frames.length;
			var df = Math.round(  f/fpp )
			var moveFrames = df < 0 ?  df + frames.length - 1 : df;
		

			_cx = x;


			setIndex( moveFrames );
			
		}

		var _i = 0;
		function setIndex( index ){
			
			frames.eq(_i).hide();
			
			_i = index < 0 ? frames.length - 1 
				: ( index >= frames.length ? 0 : index );

			frames.eq( _i ).show();

		}

		function next(){
			setIndex( _i+1)
		}

		function previous(){
			
			setIndex( _i-1 )

		}

	}

});