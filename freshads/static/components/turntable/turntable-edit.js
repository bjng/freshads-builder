define( ['lodash','knockout'], function(_,ko){
	return function( view,model ){

		var self = this;
		var bucket = app.composer.bucket;
		
		self.data = { 
			images: ko.observableArray( model.data.images || []) 
		};

		model.data = self.data;

		self.preview = ko.computed(function(){
			var images = self.data.images();
			if( images && images.length > 0 )
				return '/' + app.filesRoot + bucket.id + '/' + images[0].filename ;
			
			return '';
		});

		self.removeAll = function(){
			sef.data.images( null );
		}

		

		self.data.images.subscribe( function( files ){
			
		});	

	


		ko.applyBindings( self, view );
	}
});