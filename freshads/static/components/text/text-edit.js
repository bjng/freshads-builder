define( [ 'jquery', 'knockout', 'jquery.hotkeys', 'wysiwyg', 'color-picker' ], function( $, ko ){

	return function( view, model ){

		var self = this;
		self.view = $( view );

		self.fonts = window.app.composer.fontManager.fonts;
		self.tags = window.app.composer.fontManager.tags;

		
		ko.applyBindings( self, view );
		$('a[title]').tooltip({container:'body'});
		var wysiwyg = $('#editor').wysiwyg();
		
		function updateData(){
			//Set the current data
			model.data = wysiwyg.cleanHtml();
			//console.log(model.data);
	
		}

		wysiwyg.keyup( updateData );
		self.view.click( updateData );

		//Retrive past data
		wysiwyg.html(model.data);


		// Initiate colorpicker
		$('.colorpicker').colorpicker().on('changeColor', function(ev){
			$(this).val( ev.color.toHex() );
			//console.log(ev.color.toHex() );
		});
		
		window.prettyPrint && prettyPrint();
		$('.dropdown-menu input').click(function() {return false;})
			.change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
			.keydown('esc', function () {this.value='';$(this).change();
		});

		
			$('.toggleBackground span').on('click', function(){
				$('#editor').css("background-color",$(this).attr("data-bg"));
				//console.log($(this).attr("data-bg"));
				$('.toggleBackground span').css({ 'border-color': '#ededed'});
				$(this).css({ 'border-color': '#428bca'});
			});
		
		
	}

} )