define( [], function(  ){


	function VideoPlayer( view ){
		if(!view || view == undefined ){
			throw new Error( 'view required')
		}
		var self = this;
		this.view = $( view );
		this.video = $('video',this.view)[0];
		
		this._resized = false; //ff workaround, ff throws connections error after resize, see onError below
		this._onResize = function(e){ // resize event needs to be switched on and off, this is why we need to save the callback, see enterFullscreen below
			this._resized = true;
			self.onResize(e);
		};
		this._videoSizeReady = false; // kindle bugfix for video size, correct sie is only available after playing, see onTimeUpdate below
		this._isSeeking = false; 
		this.initEvents(this.view,this.video);
		this.onResize();
	};
	VideoPlayer.prototype = {
		initEvents: function( view, video){


			var self = this,
				moveEvent = this.isTouchDevice() ? 'touchmove' : 'mousemove',
				startEvent = this.isTouchDevice() ? 'touchstart' : 'mousedown',
				endEvent = this.isTouchDevice() ? 'touchend': 'mouseup',
				leaveEvent = this.isTouchDevice() ? 'touchleave' : 'mouseleave',
				clearedViewTimeout;


			view
				.on(startEvent,function(){

					clearTimeout( clearedViewTimeout);
					if(!self._videoSizeReady) return;
					$( '.hud').fadeIn( 400 );
					if( view.hasClass('micro') )
						video.pause();
				})
				.on( endEvent, function(){
					clearedViewTimeout = setTimeout(function(){
						if(!self.isPaused())
							$( '.hud', view ).fadeOut( 400 );
					},3000)
				});

			$(video)
				.on('pause', function(e){
					self.onPause();	
				})
				.on('play', function(e){
					self.onPlay();	
				})
				.on('timeupdate',function(e){
					self.onTimeUpdate();
				})
				.on('ended', function(e){
					self.onEnded();
				});
			
			$( 'source', view ).on( 'error',function(e){
				self.onError(e);
			})

			$('.superPlay',view).on('click', function(e){
				if(self.isPaused()){
					if( view.hasClass('micro') )
						self.enterFullscreen();
					video.play();
				}
				else{
					video.pause();
				}
			});

			$('.controls .btn-play-pause',view).click( function(e){
				if( self.isPaused() )
					video.play();
				else
					video.pause();
			});
			$('.controls .btn-fullscreen',view).click( function(e){
				self.enterFullscreen();
			});
			
			
			$('.seek-bar',view).on( startEvent, function(e) {
		  		

				var w = $(this).width(),
					x,
					handle = $('.seek-handle',this),
					seekbar = $(this),
					wasPlaying = !self.isPaused();
				
			
				x = e.clientX || e.originalEvent.touches[0].pageX || e.touches[0].clientX || e.originalEvent.changedTouches[0].clientX;
				x = x - $(this).offset().left;

				video.currentTime = (x/w) * ( self.video.duration || O );

				$(this.video).off('pause');
				video.pause();
				self.view.addClass('paused');
				self.view.addClass('seeking');
				$(this.video).on('pause',function(){ self.onPause() });

			

				var onMove = function(e){ //sets new position after move
					x = e.clientX || e.originalEvent.touches[0].pageX || e.touches[0].clientX || e.originalEvent.changedTouches[0].clientX;
					x = x - seekbar.offset().left; //local x
					x = Math.max(0,Math.min( w, x )); //boundaries left right
					handle.css( 'left', x);
				};

				$(document).on(moveEvent, onMove);
				$(document).one(endEvent, function(){
					$(this).off(moveEvent,onMove); // remove move listener when done 
					video.currentTime = (x/w) * (video.duration || 0 );
					self.view.removeClass('seeking');
					if(wasPlaying){
						video.play()
					}
				});
				
	
			});

			view.on('changed', self._onResize );
			$(window).on('resize', self._onResize  );
		},
		onError: function(e){
			if(this._resized){
				this._resized = false;
				return;
			}
			this.view.addClass('error');
			this.view.removeClass('playing');
			this.view.removeClass('paused');
		},
		onPlay: function(){
			if(this._videoSizeReady){
				this.view.addClass('playing');
				this.view.removeClass('paused');
			}
			
		},
		onPause: function(){
			this.view.addClass('paused');
			this.view.removeClass('playing');
		},
		onTimeUpdate: function(){

			if( this.video.currentTime >= 0.10 && !this._videoSizeReady  ){
				this._videoSizeReady = true;
				this.view.addClass('playing');
				this.view.removeClass('paused');
			}
			var value =  this.video.currentTime / this.video.duration;
			// Update the slider value
			$( '.seek-handle', this.view).css( 'left', value * $('.seek-bar',this.view).width() );
			
			//At the end of a video
			if(this.video.currentTime == this.video.duration){
				this.onEnded();
			}
		},
		onEnded: function(){
		    if (! this.isPaused() )
		    	this.video.pause();
		   	this.video.currentTime = 0;
		   	$('.hud',this.view).show();
		   	this.view.removeClass( 'playing' );
		   	this.view.removeClass( 'paused' );
		},
		enterFullscreen:function(){
			var self = this;
			var video = this.video;
			this.view.addClass('fullscreen');

			//resize event workaround, needs to be switched off and on again
			$(window).off('resize', self._onResize);
			$(video).one('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
				$(video).one('webkitfullscreenchange mozfullscreenchange fullscreenchange',function(e){
		 			video.controls = false;
		 			self.onResize(); 
		 			$(window).on('resize', self._onResize); 
				});			 	    
			});

			if (video.webkitRequestFullScreen) {
			    video.webkitRequestFullScreen(); // Chrome and Safar
			} 
			else if (video.mozRequestFullScreen) {
				video.mozRequestFullScreen(); // Firefox
			} 
			else if (video.requestFullscreen) {
				video.requestFullscreen(); // Other
			} 
			else if (video.msRequestFullscreen) {
				video.msRequestFullscreen(); // IE
			}
		
			setTimeout(function(){
				video.play();
				video.controls = true;
			},1000);
		},
		exitFullscreen: function(){
			this.view.removeClass('fullscreen');
		},
		addSource: function(url,type){
			var self = this;
			var sourceHtml = '<source src="' + url + '" type="' + type + '">';
			$(this.video).append(sourceHtml);
			$( 'source' ).last().on( 'error',function(e){
				self.onError();
			});
		},
		onResize: function(e){
			var view = this.view;
	
			var w = $('.poster', view).width() || view.width(); //kindle doesn't have right size in beginning, so we need to get it from poster
			var h = $('.poster', view).height() || (w / 16) * 9;

			this.view.height( h );

			this.video.controls = false;
				
			if ( w < 201) {
				view.addClass('micro');
				view.removeClass('large medium small');
			}
			else {
				$('.video-container', view).removeClass('micro');
				if( w >= 350) {
					view.removeClass('small medium');
					view.addClass('large');
				}
				if( w < 350) {
					view.removeClass('small large');
					view.addClass('medium');
				}
				if ( w < 250) {
					view.removeClass('medium large');
					view.addClass('small');
				}
			}
		},
		isPaused: function(){
			return this.video.paused;
		},
		isTouchDevice: function(){
			return 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch;
		}
	} 

	return function( view, model ){

		var videoContainer = $('.video-container', $( view ) ); 
		var videoPlayer = new VideoPlayer(  videoContainer );

		brightcoveToken = $('.video-container',view).data('brightcovetoken');
		brightcoveToken = brightcoveToken == "None" ? null : brightcoveToken;
		brightcoveId =$('.video-container',view).data('brightcoveid');
		brightcoveId = brightcoveId == "None" ? null : brightcoveId;

	
		//get Brightcove source
		if( brightcoveToken && brightcoveId ){
			$.ajax({
				url: 'http://api.brightcove.com/services/library?command=find_video_by_id&token=' + brightcoveToken + '&video_id=' + brightcoveId + '&media_delivery=http&fields=FLVURL%2Crenditions&callback=?', 
				dataType : "jsonp",
				success: function(data) {
					videoPlayer.addSource( data.FLVURL,'video/mp4');							
				},
				error: function( data ){
					videoPlayer.onError();
				}

			});
		}
	
	} // end return


} )