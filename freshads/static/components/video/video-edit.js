define( [ 'lodash', 'knockout'], function( _, ko ){
		
	return function(view, model){
		var self = this;
		var view = $(view)

		
		
		//console.log('m',model);
		var mapping = {
    		'copy': ["videoFiles",'backdropImage']
		}

		model.data = ko.mapping.fromJS(model.data, mapping);
		model.data.videoFiles = ko.observableArray( model.data.videoFiles );
		model.data.backdropImage = ko.observable( model.data.backdropImage );
		self.data = model.data;

		var assetsroot = window.app.filesRoot + window.app.composer.id['$oid'] + '/';

		
		self.hasSources = ko.observable(false);
		self.validSources = ko.observableArray();

		model.data.videoSrcMp4.subscribe(function(video){
			updateSources();
		});
	
		model.data.videoSrcWebM.subscribe(function(video){
			updateSources();
		}); 

		model.data.videoFiles.subscribe( function(videos){
			updateSources();
			//console.log("VideoFiles");
		});

		//Removing the video file from selected
		self.removeVideoFile = function( file ){
			self.data.videoFiles.remove( file );
		}

		function updateSources(){

			var sources = [], 
				mp4 = ko.unwrap( model.data.videoSrcMp4 ), 
				webm = ko.unwrap( model.data.videoSrcWebM() );
			
			if( mp4 || webm ){
				if( mp4)
					sources.push(  { filename:mp4, contentType:'video/mp4' } )
				if( webm )
					sources.push(  { filename:webm, contentType:'video/webm' } )
			}
			else if( ko.unwrap( model.data.videoFiles ) ){
				var files = ko.unwrap( model.data.videoFiles );

				sources = _.map( files, function(video){
					return { filename: assetsroot + video.filename, contentType: video.contentType }
				})
			}

			self.validSources( sources )
			self.hasSources( sources.length > 0 );

		}


		updateSources();

		ko.applyBindings( self, view[0] );

	}



});