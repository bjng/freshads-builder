	define( ['lodash','mongo', 'knockout','formValidator'], function(_,mongo,ko,formValidator ){
	return function( view ){
		var view = $( view );
		var self = this

		self.users = ko.observableArray();

		self.selectedUser = ko.observable();

		self.query = ko.observable( '' );

		var filter= '$natural';

		var _queryTimeout;
		self.query.subscribe( function(query){
			clearTimeout( _queryTimeout );
			$( '.fa-smart-search .dropdown',self.view ).html('');
			if( self.query().length < 4 ) return;
			_queryTimeout = setTimeout( function(){
				$( '.fa-smart-search .dropdown',self.view ).load( '/search?q=' + self.query() )
			},800);

		})

		function User() { 
			this.name=""; 
			this.email="";
			this.password="";
			this.group="producer"
		};

		var _filter = {'modified':-1};
		self.setFilter = function(value,e){
			var filter = $(e.target).data('filter');
			_filter = {}
			_filter[ filter ] =  filter == 'modified' ? -1 : 1;
			updateList()
		}

		function updateList(){
			mongo( 'users').find().sort( _filter ).run( function( result){
				self.users( result );
			})
		}

		self.removeUser = function( user ){
			mongo( 'users' ).remove( {'_id':user._id} ).run( function( result ){
				self.users.remove( user._id );
				updateList();
			})
		}

		self.createUser = function(){

			self.editUser( new User() );

		}

		self.editUser = function( user ){
			
			var clone = _.clone( user );
			self.selectedUser( clone );

		}

		self.saveUser = function(form){
			var valid = formValidator.validateForm( form );
			if( !valid )
				return

			var user = self.selectedUser();
			var cleanedData = ko.mapping.toJS( user );
			var id = ko.unwrap( user.id );
			mongo( 'users' ).save( cleanedData ).run( function( result ){
				if( _.has( result, 'error') )
					return app.danger( result.error )

				var i = _.findIndex( self.users(), { id: result.id } );
				if(i > -1 )
					self.users.splice( i, 1, result );
				else
					updateList();
				self.selectedUser( null );
				
			});
		}

		self.cancelUser = function( user ){
			self.selectedUser(null);
		}

		self.sortTable = function( item, e) {

			var users  = ko.unwrap(self.users);
			var sort   = $(e.currentTarget).data('filter');
			
			users = _.sortBy(users,sort);
			self.users(users);
		}


		updateList();

		ko.applyBindings( self, view[0] )
	}

})