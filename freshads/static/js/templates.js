define( ['lodash','mongo', 'knockout','formValidator'], function(_,mongo,ko,formValidator ){
	return function( view ){
		var view = $( view );
		var self = this

		self.items = ko.observableArray();

		self.selectedItem = ko.observable();

		self.query = ko.observable( '' )

		var filter= '$natural';

		var _queryTimeout;
		self.query.subscribe( function(query){
			clearTimeout( _queryTimeout );
			$( '.fa-smart-search .dropdown',self.view ).html('');
			if( self.query().length < 4 ) return;
			_queryTimeout = setTimeout( function(){
				$( '.fa-smart-search .dropdown',self.view ).load( '/search?q=' + self.query() )
			},800)

		})

		self.getAdWidth = function( ad ){
			var w = ad.style.width;
			var percentage = 100/768 * w;
			return percentage + '%';
		}

		self.getAdHeight = function( ad ){
			var h = ad.style.height;
			var percentage = 100/1024 * h;
			return percentage + '%'
		}

		function Template(){ 
			this.name="";
			this.style={ width:"768",height:"1024"}
		};
		

		var _filter = {'modified':-1};
		self.setFilter = function(value,e){
			var filter = $(e.target).data('filter');
			_filter = {}
			_filter[ filter ] =  filter == 'modified' ? -1 : 1;
			updateList()
		}


		function updateList(){
			mongo( 'templates').find().sort( _filter ).run( function( result){
				self.items( result );
			})
		}

		self.removeItem = function( item ){
			mongo( 'templates' ).remove( {'_id':item._id} ).run( function( result ){
				self.items.remove( item._id );
				updateList();
			})
			
		}

		self.createItem = function(){
			self.editItem( new Template() );
		}

		self.editItem = function( item ){
			var clone = _.clone( item );
			self.selectedItem( clone )
		}

		self.saveItem = function(form){
			var valid = formValidator.validateForm( form );
			if(!valid)
				return;
			
			var item = self.selectedItem();
			var cleanedData = ko.mapping.toJS( item );
			var id = ko.unwrap( item.id );
			mongo( 'templates' ).save( cleanedData ).run( function( result ){
				if( _.has( result, 'id')){
					var i = _.findIndex( self.items(), { id: result.id } );
					if(i > -1 )
						self.items.splice( i, 1, result );
					else
						updateList();
					self.selectedItem( null );
				}
			})
		}

		self.cancelItem = function( item ){
			self.selectedItem(null);
		}

		updateList();




		ko.applyBindings( self, view[0] )
	}

})