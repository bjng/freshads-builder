	define( ['lodash','mongo', 'knockout','formValidator', 'datepicker'], function(_,mongo,ko,formValidator ){
	return function( view ){
		var view = $( view );
		
		var id = { '$oid': window.location.pathname.split('/')[2] };

		var self = this

		self.items = ko.observableArray();

		self.templates = ko.observableArray();

		self.selectedItem = ko.observable();

		self.query = ko.observable( '' )

		var filter= '$natural';

		//DATEPICKER
		var date = new Date();
		var dateString = [date.getDate(),date.getMonth() + 1,date.getFullYear()].join('-');  
		$('#dp2').datepicker({
			format: 'dd-mm-yyyy'
		}).on('changeDate', function(e){
			self.insertionDate( $(this).find('input').val()  );
		});
		$('#dp2').datepicker('setValue', dateString);

		var _queryTimeout;
		self.query.subscribe( function(query){
			clearTimeout( _queryTimeout );
			$( '.fa-smart-search .dropdown',self.view ).html('');
			if( self.query().length < 4 ) return;
			_queryTimeout = setTimeout( function(){
				$( '.fa-smart-search .dropdown',self.view ).load( '/search?q=' + self.query() )
			},800)

		})

		function Ad(){
			this.name = "";
			this.data = { grid:48,guides:[] };
			this.style = { backgroundColor: 'rgba(255,255,255,0)', width:768, height:1024 };
			this.campaign = '';
			this.bookingId = '';
			this.insertionDate = '';
			this.parent = {'$oid':id};
			this.components = [];
			this.styleSheet = [];

		};



		self.getAdWidth = function( ad ){
			var w = ad.style.width;
			var percentage = 100/768 * w;
			return percentage + '%';
		}

		self.getAdHeight = function( ad ){
			var h = ad.style.height;
			var percentage = 100/1024 * h;
			return percentage + '%'
		}


		var _filter = {'modified':-1};
		self.setFilter = function(value,e){
			var button = $(e.currentTarget);
			var filter = button.data('filter');
		
			var direction = parseInt( button.attr('data-direction') );
			if( filter in _filter){//toggle ascending descending
				direction = -1 * direction;
				button.attr('data-direction', direction);
			}
		
			_filter = {};
			_filter[filter] = direction;
			button.addClass('active');
			button.siblings().removeClass( 'active' );
			updateList()
		
		}


		function updateList(){
			mongo( 'ads').find( { parent:id }, { components:0 } ).sort( _filter ).run( function( result){
				self.items( result );
			})
		}

		self.applyTemplate = function( template){
			var item = ko.unwrap( self.selectedItem )
			if( item && !item.id ){
				item.style.width = template.style.width;
				item.style.height = template.style.height;

				if( self.items() ){//check if name exists
					var nameEx = new RegExp( template.name + '\\s?(\\d*)' );
					var count = 0;
					_.each(self.items(),function(item){
						var match = item.name.match( nameEx );
						if(match && match.length > 1) {
							count = Math.max(count, parseInt(match[1] || 0) + 1 );
						}	
					})
				}

				item.name = template.name + ( count > 0 ? ' ' + count : '' );              
				
			}
			self.selectedItem( item );
		}

		self.removeItem = function( item ){
			mongo( 'ads' ).remove( {'_id':item._id} ).run( function( result ){
				self.items.remove( item._id );
				updateList();
			})
			
		}

		self.copyItem = function( item ) {
				// FUNCTIONALITY NEEDED
		}	

		self.gotoAnalytics = function( item ) {
			window.location.href = "/analytics/" + item.id;
		}

		self.createItem = function(){
			self.editItem( new Ad() );
		}

		self.editItem = function( item ){
			if( _.has(item,'_id') ){
				mongo('ads').findOne( { _id: item._id } ).run( function( model ){
					self.selectedItem( model );
				})
			}
			else{
				self.selectedItem( _.clone(item,true) );
			}
			
		}

		self.toTimeString = function(bsonDate){
			var date = new Date(bsonDate.$date);

			function twoDigit(n){
				return n<10? '0'+n:''+n;
			}

			return twoDigit(date.getDate()) + '/' + twoDigit((date.getMonth() + 1)) + '/' + date.getFullYear() + ' ' + twoDigit(date.getHours()) + ':' + twoDigit(date.getMinutes()) + ':'  + twoDigit(date.getSeconds());
		}

		self.saveItem = function(form){
			var valid = formValidator.validateForm( form );
			if(!valid)
				return;

			var item = self.selectedItem();
		
			var cleanedData = ko.mapping.toJS( item );
			var id = ko.unwrap( item.id );
			mongo( 'ads' ).save( cleanedData ).run( function( result ){
				if( _.has( result, 'id')){
					var i = _.findIndex( self.items(), { id: result.id } );
					if(i > -1 )
						self.items.splice( i, 1, result );
					else
						updateList();
					self.selectedItem( null );
				}
			})
		}

		self.cancelItem = function( item ){
			self.selectedItem(null);
		}

		updateList();

		mongo('templates').find().run( function( templates ){
			self.templates(templates);
			
		})

		ko.applyBindings( self, view[0] )
	}

})