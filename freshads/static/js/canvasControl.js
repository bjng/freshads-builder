define(['lodash','knockout'], function(_,ko) {

	return function( composer, canvas ){


		var self=this;

		_adModel = composer.adModel;
		_adModel.subscribe( function(model){
			model.data.grid.subscribe( function( size ){
				self.updateDisplay();
			})
			model.style.width.subscribe( function( w ){
				self.updateDisplay()
			})
			model.style.height.subscribe( function( h ){
				self.updateDisplay()
			})
			self.updateDisplay();
		})

		self.snapGrid = true;
		self.snapGuides = true;
		self.activeComponent;
		self.selection;

		var _canvas = canvas;
		var _ctx = canvas.getContext('2d');

		var _showGrid = true;
		var _showGuides = true;
		
		

		var _selectionToolRectangle;

		var _background;
		
		var _activeGuide;
		var _activeHandle;		
		var _handleBounds;

		var _handleTopLeft = new Circle(0,0,12), 
			_handleTopRight=new Circle(0,0,12), 
			_handleBottomLeft=new Circle(0,0,12), 
			_handleBottomRight=new Circle(0,0,12),
			_handleMove = new Circle(0,0,12),
			_handleUp = new Circle(0,0,12),
			_handleDown = new Circle(0,0,12);

		var _virtualMove;//needed for snapping when moving _handle Bounds


		self.onSelection;
		self.onSelectionComplete;

		self.onHandleComplete;

		self.tools = {}

		self.tools.selection = new SelectionTool( _canvas);
		self.tools.selection.select();

//MOUSE EVENTS	

		/* onMouseMove checks if cursor is over component, handle or guide 
		and sets them temporarely as active if hittest is true
		*/
		self.tools.selection.onMouseMove = function( tool ){
		
			_activeGuide = _hitTestGuides( tool.position);
			
			_activeHandle = _hitTestHandles( tool.position );

			
			if(!_activeHandle){

				self.activeComponent = _hitTestComponents( tool.position );

			}
			if(self.activeComponent){
				_handleBounds = _localToGlobal( _getComponentBounds( self.activeComponent ) ).abs();
			}
		
			self.updateDisplay();

			
		}


		/*
			creates new guide after holding down button for 1 sec on ad bounds.
		*/
		var _addGuideTimeout;
		self.tools.selection.onMouseDown = function( tool ){
			self.selection = null;
			var adBounds = _getAdBounds()
			var hitFormat = adBounds.hitTest( tool.position );
			if (hitFormat && !_activeGuide){
				_addGuideTimeout  = setTimeout( function(){ //add guide if mouse pressed a few seconds
					var guide = { x:tool.position.x  - adBounds.left(),y:0 };
					_adModel().data.guides.push( guide );
					_activeGuide = guide;
					self.updateDisplay();	
				},1000);
			}
			

			_virtualMove = null;

			self.updateDisplay();
		}

		
		/*
			Monitor active guide 
			Monitors resizing of component via active handle, including grid snapping

		*/
		self.tools.selection.onMouseDrag = function( tool ){
			
			clearTimeout( _addGuideTimeout );
			var adBounds = _getAdBounds();
			var g = ko.unwrap( _adModel().data.grid );

			if(_activeGuide){
				var dx = Math.abs( tool.delta.x ), dy = Math.abs( tool.delta.y );
				
				var nPos = tool.position;//new local position for guide
				
				if( self.snapGrid ){
					self.snapGuides=false;
					nPos = _snapPoint( tool.position );
					self.snapGuides=true;
				}

				nPos = _globalToLocal( nPos );


				var isHorizontal = _activeGuide.x == 0;
				//var mouseIsX = dx > dy;

				_activeGuide.x = isHorizontal ? 0 : nPos.x;
				_activeGuide.y = isHorizontal ? nPos.y : 0;

				swap = isHorizontal && dx > dy  && dx > 5 || !isHorizontal && dy > dx && dy > 5;
				
				if(swap){ //swap horizontal or vertical
					_activeGuide.x = isHorizontal ? nPos.x : 0;
					_activeGuide.y = isHorizontal ? 0 : nPos.y;
				}	
				
				if(nPos.x <=0 || nPos.y <= 0 || nPos.x >= adBounds.width || nPos.y >= adBounds.height){ //hide and remove, guides with negative values will be removed
					_activeGuide.x = -1;
					_activeGuide.y = -1;
				}

			}
			else if( _activeHandle && _activeHandle != _handleUp && _activeHandle != _handleDown ){ // move and snap handleBounds
				//resize
				if( !_virtualMove){ //for snapping, move a virtual object without snapping
					_virtualMove = _activeHandle == _handleMove ? _handleBounds.clone() : _activeHandle.clone();
				}
				_virtualMove.x+=tool.delta.x;
				_virtualMove.y+=tool.delta.y;
				
				var snapped = _snapPoint( _virtualMove );
				switch (_activeHandle){
					case _handleMove:
						_handleBounds = snapped;
					break;
					case _handleTopLeft:
						_handleBounds.left(  snapped.x );
						_handleBounds.top( snapped.y );
					break;
					case _handleTopRight:
						_handleBounds.right( snapped.x  );
						_handleBounds.top( snapped.y );
					break;
					case _handleBottomRight:
						_handleBounds.right( snapped.x );
						_handleBounds.bottom( snapped.y );
					break;
					case _handleBottomLeft:
						_handleBounds.left( snapped.x );
						_handleBounds.bottom( snapped.y );
					break;
				}



			}
			else{ //create Selection'
				_selectionToolRectangle = tool.dragBounds.clone();
				var mousePoint = new Point( _selectionToolRectangle.right(), _selectionToolRectangle.top() );
				mousePoint = _snapPoint( mousePoint );
				_selectionToolRectangle.abs();
				self.selection = _globalToLocal( _selectionToolRectangle.diff( adBounds ).abs() );
				//_selection = _snapRectangleToGrid( self.selection );
				if(self.onSelection){
					self.onSelection( self );
				}
			}
			self.updateDisplay()
		}

		/* 
		Releases and resets handles after drag, 
		updates Guides data if guide was created or moved
		updates component bounds if component was resized
		*/
		self.tools.selection.onMouseUp = function( tool ){
			
		
			var adBounds = _getAdBounds();

			clearTimeout( _addGuideTimeout );

			//_selection = null;

			if(_activeGuide){

				var remove = !adBounds.hitTest( _localToGlobal( new Point( _activeGuide.x,_activeGuide.y ) ) );
				if( remove ){
					_adModel().data.guides.splice( _adModel().data.guides.indexOf( _activeGuide ),1 );
					self.updateDisplay();
				}

			}
			_selectionToolRectangle = null;
			
			if(self.onSelectionComplete && self.selection){
				self.onSelectionComplete( tool )
			}

			if( self.onActiveComponent){
				self.onActiveComponent( self.activeComponent );
			}
			if( _handleBounds && _activeHandle ){
				var gb = _globalToLocal( _handleBounds );
				
				self.activeComponent.style.width = ( (100/adBounds.width) * gb.width ) + '%';
				self.activeComponent.style.height =  ( (100/adBounds.height) * gb.height ) + '%';
				self.activeComponent.style.left =  ( (100/adBounds.width) * gb.x ) + '%';
				self.activeComponent.style.top = ( (100/adBounds.height) * gb.y ) + '%';
				
				if(self.activeComponent && (_activeHandle == _handleUp || _activeHandle == _handleDown ) ){
					var components = _adModel().components();
			
					var from = components.indexOf( _.find(components,{id:self.activeComponent.id } ) ), 
						l = components.length,
						to = from;
					to += _activeHandle == _handleUp ? 1 : -1;				
					to = Math.max( 0, Math.min( to, l-1));
				
					var swap = components[to];
					components[ to ] = components[from];
					components[from] = swap;
					_adModel().components( components );
					
				}
				_adModel().components.valueHasMutated();
			}
			self.updateDisplay();
		}




		/*
			snaps a global point or similar to the grid. 
			Similar means it needs clone() and x,y properties, i.e. regular shapes
		*/
		function _snapPoint( pointSimilar ){ //needs to be global,cloneable and have x,y
			var p = pointSimilar.clone();
			var lp = _globalToLocal( p );
			var closest = new Point(0,0);

			var threshold = 10;//snapp threshold in px

			var adBounds = _getAdBounds();

			if(self.snapGrid){//snap Grid
				

				var g = _adModel().data.grid();
				closest.x = Math.round( lp.x/ g ) * g;
				closest.y = Math.round( lp.y/ g ) * g;
				
			}
			if(self.snapGuides){//snap Guides
				_.each( _adModel().data.guides, function( guide ){ 
					if(guide.y == 0)//vertical guide
						closest.x = Math.abs( guide.x - lp.x) < Math.abs(closest.x - lp.x) ? guide.x : closest.x;
					else
						closest.y = Math.abs( guide.y - lp.y) < Math.abs(closest.y - lp.y) ? guide.y : closest.y;
				});

			}

			//only if closest is in threshold range
			lp.x = Math.abs( closest.x - lp.x ) / threshold < 1  ? closest.x: lp.x;
			lp.y = Math.abs( closest.y - lp.y ) / threshold < 1  ? closest.y : lp.y;

			//snap Ad Bounds anyway
			lp.x = Math.max( 0, Math.min( adBounds.width,lp.x ) );
			lp.y = Math.max( 0, Math.min( adBounds.height,lp.y ) );

			p = _localToGlobal(lp);

			
			return p
		}

		



// HIT TESTS

		function _hitTestHandles( point ){
			if(!_handleBounds) return null;
			return _handleTopLeft.hitTest( point ) ? _handleTopLeft  : 
				_handleTopRight.hitTest( point ) ? _handleTopRight  : 
				_handleBottomLeft.hitTest( point ) ? _handleBottomLeft  : 
				_handleBottomRight.hitTest( point ) ? _handleBottomRight  : 
				_handleMove.hitTest( point ) ? _handleMove  : 
				_handleUp.hitTest( point ) ? _handleUp  : 
				_handleDown.hitTest( point ) ? _handleDown  : 
				null;

		}

		function _hitTestComponents( point ){
			var components = ko.unwrap(_adModel().components);
			var p = _globalToLocal( point );

			var i = components.length,
				c;
			while(i--){
				c = components[i];
				var bounds = _getComponentBounds( c );
				if( bounds.hitTest( p ) ){
					 return c
				}
			}


			return null;
		}


		function _hitTestGuides( point  ){
			var guides = ko.unwrap( _adModel().data.guides );
			if(!guides) return null

			var p =  _globalToLocal( point );
			
			var hit;
			var tolerance = 5;
			var i = guides.length,g;
			while(!hit && i--){
				g = guides[i];
				hit = ( g.x == 0 ? p.y >= g.y - tolerance && p.y <= g.y + tolerance
						: p.x >= g.x - tolerance &&  p.x <= g.x + tolerance )
					? g : null;
			}
			return hit;
			
		}


		

		self.getFormat = function(){
			return _ad;
		}

		self.updateDisplay = function( e ){
			

			if(!_canvas) return;

			self.clear();
			document.body.style.cursor = "auto"

			if(! ko.unwrap(_adModel))
				return;

			var adBounds = _getAdBounds();
			var grid = ko.unwrap( _adModel().data.grid );
			var guides = ko.unwrap( _adModel().data.guides );


			var w = $(canvas).parent()[0].clientWidth,
				h = $(canvas).parent()[0].clientHeight;

				w = Math.max( w,adBounds.width);
				h = adBounds.y * 2 + Math.max( h,adBounds.height); 


			//retina and speed workarounds IMPORTANT -----------
			
			var devicePixelRatio = window.devicePixelRatio || 1,
        	backingStoreRatio = _ctx.webkitBackingStorePixelRatio ||
                            _ctx.mozBackingStorePixelRatio ||
                            _ctx.msBackingStorePixelRatio ||
                            _ctx.oBackingStorePixelRatio ||
                            _ctx.backingStorePixelRatio || 1,

        	ratio = devicePixelRatio / backingStoreRatio;

			_canvas.width = w * ratio;
            _canvas.height = h * ratio;
			_canvas.style.width = w + 'px';
        	_canvas.style.height = h + 'px';
         
			_ctx.scale(ratio, ratio);
            
			// rendering -------------------

			if(_showGrid)
				_drawGrid( adBounds, grid,'rgba(100,100,100,0.4)',1 );
			//background
			_drawRect( adBounds,1,'rgba(255,255,255,.8)',null);
			
			if(_showGuides)
				_drawGuides(adBounds, guides, 1)
			if(self.selection)
				_drawRect( _localToGlobal( self.selection ), 1, 'rgba(255,255,255,.6)', 'rgba(255,255,255,0.1)', 1 );
			if(_selectionToolRectangle)
				_drawRect( _selectionToolRectangle, 1, 'rgba(255,255,255,.3)', null, 1 );

			if( !self.activeComponent && !_activeHandle  ){
				_handleBounds = null;
			}
			else if( _handleBounds){
				_handleBounds.abs();
				_handleTopLeft.x = _handleBounds.x; _handleTopLeft.y = _handleBounds.y;
				_handleTopRight.x = _handleBounds.right(); _handleTopRight.y = _handleBounds.y;
				_handleBottomLeft.x = _handleBounds.x; _handleBottomLeft.y = _handleBounds.bottom();
				_handleBottomRight.x = _handleBounds.right();  _handleBottomRight.y = _handleBounds.bottom();
				_handleMove.x = _handleBounds.x + .5* _handleBounds.width; _handleMove.y = _handleBounds.top() + .5*_handleBounds.height;
				_handleDown.x = _handleBounds.right(); _handleUp.y = _handleBounds.y + .5* _handleBounds.height - 10;
				_handleUp.x = _handleDown.x; _handleDown.y = _handleUp.y + 20;

				_drawRect( _handleBounds, 1, 'rgba(255,100,0,1)',null,1 );
				_drawCircle( _handleTopLeft,1,null, 'rgba(255,100,0,.7)',1 );
				_drawCircle( _handleTopRight,1,null, 'rgba(255,100,0,.7)',1 );
				_drawCircle( _handleBottomRight,1,null, 'rgba(255,100,0,.7)',1 );
				_drawCircle( _handleBottomLeft,1,null, 'rgba(255,100,0,.7)',1 );
				_drawCircle( _handleMove,1,null, 'rgba(255,100,0,0.7)', 1 );
				_drawCircle( _handleUp,1,'rgba(0,0,0,0.7)', 'rgba(255,255,255,0.7)', 1 );
				_drawCircle( _handleDown,1,'rgba(0,0,0,0.7)', 'rgba(255,255,255,0.7)', 1 );

				document.body.style.cursor = _activeHandle == _handleMove ? 'move' :
					_activeHandle == _handleTopLeft ? 'nw-resize' : 
					_activeHandle == _handleTopRight ? 'ne-resize' : 
					_activeHandle == _handleBottomLeft ? 'sw-resize' :
					_activeHandle == _handleBottomRight ? 'se-resize' :
					_activeHandle == _handleUp ? 'pointer' :
					_activeHandle == _handleDown ? 'pointer' :
					'auto';

			}

			//$(_canvas).parent().css({ overflow:'hidden', width:w,height:h})

		
		}

		function _getComponentBounds( comp ){
			var adBounds = _getAdBounds();
			var r = new Rectangle( 0,0,0,0);
			r.x = Math.floor( (adBounds.width/100) * parseFloat( comp.style.left ) );
			r.y = Math.floor( (adBounds.height/100) * parseFloat( comp.style.top ) );
			r.width = Math.floor( ( adBounds.width/100) * parseFloat( comp.style.width ) );
			r.height = Math.floor( (adBounds.height/100) * parseFloat( comp.style.height ) );
			return r;
		}

		function _getAdBounds(){

			var w = $(canvas).parent()[0].clientWidth,
				h = $(canvas).parent()[0].clientHeight;

			var b = new Rectangle(0,0, 
				parseInt( ko.unwrap( _adModel().style.width ) ), 
				parseInt( ko.unwrap(  _adModel().style.height ) ) 
			);
			b.x = parseInt( .5* ( w - b.width ) );
			b.y = 140;// parseInt( .5* (window.innerHeight - b.height) );
			return b
		}

		self.clear = function(){
			//_ctx.clearRect( 0, 0,  _canvas.width, _canvas.height )
		}

		self.kill = function(){
			clearInterval( _tick )
		}

		function onResize( e ){
            self.updateDisplay()
		}

		function getPixelRatio(){
			
			var isRetina = true;
			var ratio = isRetina ? 2:1;

            return ratio
		}


		function _drawBackground( x,y,w, h ){
			_ctx.beginPath();
			var offset = _getCenterOffset(w,h);
			
			_ctx.lineWidth = 1;
			_ctx.strokeStyle = 'rgba(255,255,255,0.5)';
			_ctx.rect(offset.x, offset.y, w, h);
			_ctx.stroke();
			
		}



		function _drawGrid( fb,size,lineStyle,ratio ){//grid size and boundaries
			fb = fb.clone().scale( ratio,ratio );
			size *=1;
			var d = size;
			for( d;d<fb.width;d+=size)
				_drawLine( new Point(fb.x + d,fb.y),new Point(fb.x + d,fb.y + fb.height ),1,lineStyle)
			
			d = size;
			for( d;d<fb.height;d+=size)
				_drawLine( new Point(fb.x,fb.y+d),new Point(fb.x+fb.width,fb.y + d ),1,lineStyle)

		}


		function _drawLine( start,end, width, style ){
			_ctx.beginPath();
			_ctx.lineWidth = width;
			_ctx.strokeStyle = style;
			_ctx.moveTo( start.x, start.y );
			_ctx.lineTo( end.x, end.y );
			_ctx.stroke();
		}

		function _drawCircle( circle,  width, strokeStyle, fillStyle, scale ){
			if(!circle) return
			scale = scale || 0;
			circle = circle.clone().scale( scale );
			_ctx.beginPath();
			_ctx.arc( circle.x, circle.y, circle.r * scale * 0.5, 0, 2 * Math.PI, false);
			if(fillStyle){
				     _ctx.fillStyle = fillStyle;
					_ctx.fill();
			}
			_ctx.lineWidth = strokeStyle;
			if(strokeStyle){
				_ctx.strokeStyle = '#003300';
				_ctx.stroke();
			}

		}

		function _drawRect( rect, strokeWidth, strokeStyle, fillStyle, scale ){
			if(!rect) return;
			//rect = rect.clone().scale( scale,scale );
			_ctx.beginPath();
			_ctx.rect( rect.x,rect.y,rect.width,rect.height);
			if(fillStyle){
				_ctx.fillStyle=fillStyle;
				_ctx.fill();
			}
			_ctx.lineWidth = strokeWidth;
			if(strokeStyle){
				_ctx.strokeStyle = strokeStyle;
				_ctx.stroke();
			}	
		}

		function _globalToLocal( object ){
				var _adBounds = _getAdBounds();
				object = object.clone();
				object.x -= _adBounds.x;
				object.y -= _adBounds.y;
				return object;
		}

		function _localToGlobal( object ){
			var adBounds = _getAdBounds();

			object = object.clone();
			object.x += adBounds.x;
			object.y += adBounds.y;
			return object;
		}


		function _drawGuides( bounds, guides ){// Arrays with x and y definitions
			if(!guides) return;
			var i,g,w,h;
			for( i=0; i < guides.length; i++ ){
				g = new Point( guides[i].x, guides[i].y);
				if( ! bounds.hitTest( _localToGlobal( g ) ) )
					continue;
				start =  g.x == 0 ? new Point( g.x, g.y + bounds.y ) : new Point( g.x + bounds.x, g.y );
				end = g.x == 0 ? new Point( canvas.width + bounds.x , g.y + bounds.y) : new Point( g.x + bounds.x, canvas.height + bounds.y );
				_drawLine(	start,end, 1,guides[i] == _activeGuide ? 'rgba(100,180,255,1)' : 'rgba(100,180,255,0.5)' );
			}
		}



		self.showGuides = function( bool ){
			_showGuides = bool;
			self.updateDisplay();
		}

		self.showGrid = function( bool ){
			_showGrid = bool;
			self.updateDisplay();
		}

		var _active = true;
		self.isActive = function(){
			return _active;
		}

		var _active = true;
		self.active = function( active ){
			if(_active == active ) return
			_active = active;

			if(active )
				this.tools.selection.select();

			else{
				document.body.style.cursor = "auto"
				this.tools.selection.deselect();
			}
			_active = active;
			self.updateDisplay();
		}
		
		window.addEventListener( 'resize', onResize );

		return self;

	}


		function Point( x,y ){
			this.x = x;
			this.y = y;

			this.floor = function( ){
				this.x = Math.floor( this.x );
				this.y = Math.floor( this.y );
				return this;
			}

			this.clone = function(){
				return new Point( this.x,this.y);
			}

			this.minus = function( point ){
				this.x -= point.x;
				this.y -= point.y; 
				return this
			}

			this.plus = function( point ){
				this.x += point.x;
				this.y += point.y; 
				return this
			}

		}

		function Rectangle(x,y,width,height){
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;

			this.left = function( val ){
				if(val != undefined ){
					this.width = this.width + ( this.x - val );
					this.x = val;
				}
				return this.width < 0 ? this.x + this.width : this.x;
			}

			this.top = function( val ){
				if(val != undefined ){
					this.height = this.height + ( this.y - val );
					this.y = val;
				}
				return this.height < 0 ? this.y + this.height : this.y;
			}

			this.right = function( val ){
				if(val != undefined ){
					if( val < this.x) this.x = val;
					else this.width = val - this.x;
				}
				return this.width < 0 ? this.x : this.x + this.width;
			}


			this.bottom = function( val ){
				if(val != undefined ){
					if( val < this.y) this.y = val;
					else this.height = val - this.y;
				}
				return this.height < 0 ? this.y : this.y + this.height;
			}

			this.clone = function(){
				return new Rectangle( this.x,this.y,this.width,this.height );
			}

			this.scale = function( x,y){
				this.x *= x;
				this.y *= y;
				this.width *= y;
				this.height *= y;
				return this;
			}

			this.diff = function( B ){
				var diff = this.clone();

				diff.x = Math.min( B.right(), Math.max( this.left(), B.left() ) );
				diff.y = Math.min( B.bottom(), Math.max( this.top(), B.top() ) );
				diff.width = Math.max(0, Math.min(B.right(),this.right()) - Math.max(B.left(),this.left() ));
				diff.height = Math.max(0, Math.min(B.bottom(),this.bottom()) - Math.max(B.top(),this.top() ));
   
		
				return diff;
			}

			this.abs = function( ){
				if( this.width < 0){
					this.x += this.width;
					this.width = Math.abs( this.width);
				}
				if( this.height < 0){
					this.y += this.height;
					this.height = Math.abs( this.height);
				}
				return this;

			}

			this.hitTest = function( point ){
				return ( point.x >= this.x && point.x - ( this.x + this.width)  <= 0 ) &&  ( point.y >= this.y && point.y  - (this.y + this.height) <= 0  ); 
			}
		}




		function Circle( x,y,r ){

			this.x = x;
			this.y = y;
			this.r = r;

			this.scale = function( scale ){
				this.r *= scale;
				this.x *= scale;
				this.y *= scale;
				return this;
			}

			this.hitTest = function( point ){
				var dx = Math.abs( point.x - this.x ),dy = Math.abs( point.y - this.y );
				return dx < this.r && dy < this.r;
			}

			this.clone = function(){
				return new Circle( this.x, this.y, this.r);
			}


		}
		


		function SelectionTool( view ){

			var self = this;

			self.drag = false;
			self.position = new Point( 0,0 );
			self.delta = new Point( 0,0 );
			self.dragBounds = new Rectangle( 0, 0, 0, 0);

			self.onMouseDown;
			self.onMouseUp;
			self.onMouseMove;
			self.onMouseDrag;
	

			function _onMouseMove( e ){
				_parseEvent(e);

				if(self.drag){
					self.dragBounds.width = self.position.x - self.dragBounds.x;
					self.dragBounds.height = self.position.y - self.dragBounds.y;
					if( self.onMouseDrag )
						self.onMouseDrag( self )
				}
				else if(self.onMouseMove ){
					self.onMouseMove( self )
				}

			}

			function _onMouseDown( e ){
				_parseEvent(e);
				self.drag = true;
				self.dragBounds.x = self.position.x;
				self.dragBounds.y = self.position.y;
				if(self.onMouseDown)
					self.onMouseDown( self );
			}

			function _onMouseUp( e ){
				_parseEvent(e);
				self.drag = false;
				if(self.onMouseUp)
					self.onMouseUp( self );
			}

			function _onMouseUp( e ){
				_parseEvent(e);
				self.drag = false;
				if(self.onMouseUp)
					self.onMouseUp( self );
			}

			function _parseEvent( e ){
				self.delta.x = (e.offsetX || e.layerX )  - self.position.x;
				self.delta.y = (e.offsetY  || e.layerY )  - self.position.y;
				self.position.x =  e.offsetX || e.layerX;
				self.position.y = e.offsetY  || e.layerY;
			}



			self.select = function(){
				view.addEventListener( 'mousedown', _onMouseDown);
				view.addEventListener( 'mouseup', _onMouseUp);
				view.addEventListener( 'mousemove', _onMouseMove);
				
			}


			self.deselect = function(){
				view.removeEventListener( 'mousedown', _onMouseDown);
				view.removeEventListener( 'mouseup', _onMouseUp);
				view.removeEventListener( 'mousemove', _onMouseMove);
			}


		
			
		}


});