define( [ 'lodash','knockout', 'chart', 'mongo', 'datepicker'], function(_,ko,Chart,mongo){
	

	//Day Helper Class
	function Day( data ){
	
	
		if(data){
			var unique = _.find( data, {n:'Unique Impression'} ) || {};
			var impressions = _.find( data, {n:'Impression'} ) || {};
			
		
			this.uniqueCount =  unique.c || 0;
			this.count =  impressions.c || 0;


			
			var durations = _.filter( data, { n: 'Duration'} );
			
			durations = _.sortBy( durations, function(aeo){ return parseInt( aeo.l ) } );
			
			this.durations = _.map( durations, function(aeo){ return { time: aeo.l, count: aeo.c } } ) || []; 
			
			this.date = impressions.t.$date;
			this.dateString = new Date( this.date ).toDateString();
			this.events = _.sortBy( _.where( data, function( aeo ){
				return aeo.g ? true : false;
			} ), 'g' ) || [];
		}

		return this
	}

	function DateRange( start, end ){
		var self = this;

		self.start = start || new Date();
		self.end = end || start || new Date();

		self.weekForDay = function( day ){
			var start = _getSartOfDay( day );
				start.setDate( start.getDate() - start.getDay() + 1 );
			var end = _getEndOfDay( start );
				end.setDate( end.getDate() + ( 7 - end.getDay() ) );
			self.start = start;
			self.end = end;
			return self;
		}

		self.index = -1;


		self.getNext = function(){
			var i = self.index + 1;
			var d = new Date( self.start );
				d.setDate(  self.start.getDate() + i);
			if( d.getTime() <= self.end.getTime() && d.getTime() >= self.start.getTime() ){
				self.index = i;
				return d;
			}
			return null;
		} 

		self.getPrevious = function(){
			var i = self.index - 1;
			var d = new Date( start ).setDate(  start.getDate() + i);
			if( d.getTime() <= self.end().getTime && d.getTime() >= start.getTime() ){
				self.index = i
				return d;
			}
			return null;
		} 

		self.rewind = function(){
			self.index = -1;
		}

		self.expand = function(){
			self.start = _getSartOfDay( start );
			self.end = _getEndOfDay( end );
			return self;
		}


		function _getSartOfDay( date ){
			var day = new Date( date.getTime() );
			day.setHours(0,0,0,0);
			return day;
		}

		function _getEndOfDay( date ){
			var day = new Date( date.getTime() );
			day.setHours(23,59,59,0);
			return day;
		}




		return this;
	}



	//View Controller
	return function( view, data ){

		var self = this;
		var id = window.location.pathname.split('/')[2];
	

		self.days = ko.observableArray();
		
		self.currentDate = ko.observable();

		var thisWeek = ( new DateRange() ).weekForDay( new Date() );

		self.startDate = ko.observable( thisWeek.start.getTime() );

		self.endDate = ko.observable( thisWeek.end.getTime() );


		self.startDate.subscribe( function( date ){
				_getRange();
		});

		self.endDate.subscribe( function( date ){
				_getRange();
		});

		self.getClickSize = ko.computed( function(){
			if(! self.days() )
				return 0;

			return (100 / (self.days().length  - 1 ) )  + '%';
		})

		//chart canvas init

		var canvas = document.getElementById("canvas1");
		var _rangeChart = new Chart( canvas.getContext("2d") );
	
		function _fillGaps( days ){
			var range = new DateRange( new Date( self.startDate() ), new Date( self.endDate() ) );
			range.expand();
			var date = range.getNext();
			var missing;
			while( date ){
				var exists = _.first( days, { 'date': date.getTime() } );
				if( exists.length == 0){
					missing = new Day();
					missing.date = date.getTime();
					missing.dateString = date.toDateString();
					missing.count=0;
					missing.uniqueCount = 0;
					missing.events = [];
					missing.durations = [];
					days.push( missing )
				}
				date = range.getNext();
			}

			return days;
			
		};

		self.data = ko.observableArray();
		self.data.subscribe(function( data ){
			

			var days = _.map( _.groupBy( data, function(aeo){return aeo.t['$date']} ),function( dateData ){
				return new Day( dateData );
			} );

			days = _fillGaps( days );

			days = _.sortBy( days, 'date');
			

			
			//chart data
			var uniqueImpressions = [], impressions = [],durations=[],timeRange=[];
			_.each( days, function( day ){
				uniqueImpressions.push( day.uniqueCount || 0);
				impressions.push( day.count );
				timeRange.push( day.dateString );
				var dur = _.find( data, {name:"Duration"} );
				durations.push( day.duration );

			} )

			self.days( days );
		
			var lineChartData = {
				labels : timeRange,
				datasets : [
					{
						fillColor : "rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,1)",
						pointColor : "rgba(220,220,220,1)",
						pointStrokeColor : "#fff",
						data : impressions,
					},
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : uniqueImpressions,
					}
				]
				
			}

		
			_rangeChart.Line(lineChartData,{animation:false, scaleLabel : "<%=parseInt(value)%>" });

			
		});


		self.getEventColor = function(event){
			if(! ko.unwrap( self.currentDate) )
					return
			var events = self.currentDate().events;
			var uniqueComponentEvents = _.unique( events, 'component' );
		
			var i = _.findIndex( uniqueComponentEvents, { 'component':event.component});
			var l = uniqueComponentEvents.length;
		

			var r = Math.floor( i/l  * 200 ),
				g= Math.floor(  (l-i) / l * 200 ),
				b = 0;
	
			return 'rgba(' + r + ',' + g + ',' + b + ',0.8)'
		}


		self.getEventPercentage= function(event){
		
				if(! ko.unwrap( self.currentDate) )
					return '0%'

				var events = ko.unwrap( self.currentDate().events );

				var max = _.max( events, 'c' ).c;
			
			
				return ( ( 100 / max) * event.c ).toFixed(2) + '%';
				
			}




		function _getRange( date ){
	
			var range = new DateRange( new Date( self.startDate() ),  new Date( self.endDate() ) );
			range.expand();
			
			var startDate = Math.floor( range.start.getTime() / 1000 );
			var endDate = Math.floor( range.end.getTime() / 1000 );

			var query = { id: id, time:{ '$gte':startDate,'$lte':endDate} };
		
			mongo('analytics').find( query  ).run(function(data){
				self.data(data);
				self.currentDate( null );
			});
		}

		function _resizeCanvas( e ){

			$('canvas').each(function() {

				var w = $(this).parent().width();
				var h = $(this).parent().height();
				
				if( ko.unwrap( self.data) )
					self.data.valueHasMutated();
				if( ko.unwrap( self.currentDate) )
					self.currentDate.valueHasMutated();
			});
		}
		
		// SCROLL FUNCTION 
		function scrollToElement(selector, time, verticalOffset) {
		    time = typeof(time) != 'undefined' ? time : 1000;
		    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		    element = $(selector);
		    offset = element.offset();
		    offsetTop = offset.top + verticalOffset;
		    $('html, body').animate({
		        scrollTop: offsetTop
		    }, time);
		}
		$('#topDwell').click(function () {
		    scrollToElement('#dayStats');
		});

		$('.backToTop').click(function () {
		    scrollToElement('.app-container');
		});
		

		
		ko.applyBindings( self, $(view)[0] );

		_getRange();

		$( window ).resize( _resizeCanvas );

		_resizeCanvas();

		
	}
});