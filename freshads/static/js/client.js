define( ['lodash','mongo', 'knockout','formValidator'], function(_,mongo,ko,formValidator ){
	return function( view ){
		var view = $( view );


		var id = { '$oid': window.location.pathname.split('/')[2] };

		var self = this

		self.items = ko.observableArray();

		self.selectedItem = ko.observable();

		self.query = ko.observable( '' )

		var filter= '$natural';

		var _queryTimeout;
		self.query.subscribe( function(query){
			clearTimeout( _queryTimeout );
			$( '.fa-smart-search .dropdown',self.view ).html('');
			if( self.query().length < 4 ) return;
			_queryTimeout = setTimeout( function(){
				$( '.fa-smart-search .dropdown',self.view ).load( '/search?q=' + self.query() )
			},800)

		})

		function Campaign(){ 
			this.name=""; 
			//this.brand="";
			this.parent= id;
			//this.bookingId="";
		}


		var _filter = {'modified':-1};
		self.setFilter = function(value,e){
			var button = $(e.currentTarget);
			var filter = button.data('filter');
		
			var direction = parseInt( button.attr('data-direction') );
			if( filter in _filter){//toggle ascending descending
				direction = -1 * direction;
				button.attr('data-direction', direction);
			}
		
			_filter = {};
			_filter[filter] = direction;
			button.addClass('active');
			button.siblings().removeClass( 'active' );
			updateList()
		
		}


		function updateList(){

			mongo( 'campaigns').find( { parent: id }, { components:0 } ).sort( _filter ).run( function( result){
				self.items( result );
			})
		}

		self.removeItem = function( item ){
			mongo( 'campaigns' ).remove( {'_id':item._id} ).run( function( result ){
				self.items.remove( item._id );
				updateList();
			})
			
		}

		self.createItem = function(){
			self.editItem( new Campaign() );
		}

		self.editItem = function( item ){
			if( _.has(item,'_id') ){
				mongo('campaigns').findOne( { _id: item._id } ).run( function( model ){
					self.selectedItem( model );
				})
			}
			else{
				self.selectedItem( _.clone(item,true) );
			}
			
		}

		self.saveItem = function( form ){
			var valid = formValidator.validateForm( form );
			if(!valid)
				return;

			var item = self.selectedItem();
			var cleanedData = ko.mapping.toJS( item );
			var id = ko.unwrap( item.id );
			mongo( 'campaigns' ).save( cleanedData ).run( function( result ){
				if( _.has( result, 'id')){
					var i = _.findIndex( self.items(), { id: result.id } );
					if(i > -1 )
						self.items.splice( i, 1, result );
					else
						updateList();
					self.selectedItem( null );
				}
			});
		}


		self.cancelItem = function( item ){
			self.selectedItem(null);
		}

		updateList();

		ko.applyBindings( self, view[0] )
	}

})