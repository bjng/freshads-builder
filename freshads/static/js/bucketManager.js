define(['lodash','knockout','mongo','jquery.slimScroll'], function( _, ko, mongo) {





    return function( id, view,  maxSize, maxUploads, filetypes){

        var self = this;

        self.id = id ? id : null;
        self.maxSize = maxSize ? maxSize : 2048;
        self.maxUploads = maxUploads ? maxUploads : 2;

        self.filetypes =  filetypes || ['pdf','jpg','png','gif','vob','mp4','ogg','mp3','mp4'];
           
        _view = $(view);
        _fileListView = $( '.file-list', view).first();


        $( '.scroll',_view).slimScroll({height:'300px',wheelStep: 10,size:'10px'});

        _input = $( 'input[type="file"]' , view );

        self.multiple = true;
        self.fileList = ko.observableArray( [] );
        self.groupedFiles = ko.observableArray( [] );
        self.queue = ko.observableArray( [] );
        self.running = ko.observableArray( [] );

        
        self.target = null;
        self.targetContentType='';

        self.totalToString = ko.observable( '0 KB' );

        self.total = ko.observable( 0 );

        self.items = ko.observableArray();

        self.fileList.subscribe( function( list ){
            var bytes = 0;
            _.each( list, function( file ){
                bytes+=file.length;
            })
            self.total( bytes );
            self.totalToString( _parseBytes( bytes ) );
            self.groupedFiles( _groupFiles( list ) );
            _updateList();
        })

        function _groupFiles(files){
            var grouped = [];

            var siblingsCount = {};

            var parts,family, siblings, totalLength;
           
            _.each( files, function( file ){
               
                parts = _getFileParts( file );
                family = parts[1] || '';
                
                if(! _.has( siblingsCount, family ) ){
                    siblings = _naturalSort( self.getSiblingsFor( file, files ) );

                    siblingsCount[family] = siblings.length;
                    totalLength = _.reduce( siblings, function(sum,item){
                        return sum + item.length;
                    },0);
                    if( siblingsCount[family] > 3 )
                        grouped.push( { filename: family + '...' + parts[4], length:totalLength, children:siblings, contentType: file.contentType,_id:{$oid:''}  } ); 
                    else
                        grouped.push( file );
                }
                else if( siblingsCount[family] <= 3){
                        grouped.push( file );
                }
            });
         
            return _naturalSort( grouped );
        }

        self.queue.subscribe( function( queue ) {
            _runNext();

        });

        function onDrop( e ){
            e = e.originalEvent || e;
            e.preventDefault();
            e.stopPropagation();
 
            var files = e.target.files || e.dataTransfer.files;
            
            if(!files || files.length == 0 )
                return false

            self.upload( files );
            self.show( true );

            return false
        }
        
        _input.change( onDrop );
        $( document ).on( 'drop',onDrop);
        $( document ).on( 'dragover',function(){return false});
        $( document ).on( 'dragenter',function(){return false});


        self.upload = function( files ){
            var queue = self.queue();
            _.each( files, function( file ){
                queue.push( { filename:file.name, length: file.size, progress: ko.observable( 0 ), data:file, created: new Date().getTime() } );
            });
            self.queue( queue );
        }

        function _load(){
            if( !self.id ) return
            mongo('fs.files').find( { 'metadata.bucket':self.id } ).run( function( data ){
              self.fileList( _.map( data , _parseFile ) );
            })
            return this;
        }

        self.load = function( id ){
            if( self.id == id )
                return this;
            self.id = id;
            _load();
            return this;
        }

        self.hasChildren = function( item ){
            return _.has( item, 'children' ) && item.children.length!=0;
        }


        self.getSiblingsFor = function( file, files ){
            if( !file || !file.filename )
                return null

            var all = files || self.fileList();

            var familyMatch = _getFileParts( file );
        
            if(!familyMatch || !familyMatch[1])
                return []

            var siblings = _.filter( all, function( item ){
                return item.filename.search( familyMatch[1] ) == 0;
            });

/*
            var maxZeroRatio = _.reduce( siblings, function( zeroRatio, item ){
                var parts = _getFileParts( file ) ;
                var numerationLength = parts[2].length + parts[3].length;
      
                if( parts[2].length > zeroRatio.zeros)
                    zeroRatio.zeros = parts[2].length;
                if( numerationLength < zeroRatio.length )
                    zeroRatio.length = numerationLength
                return zeroRatio
                
            },{zeros:0,length:1})
*/
        
            return siblings;

        }



        function _parseFile( file ){
            return file;
        }

        /* [ filename, sequenceName, leadingZeros, number, extension]
            e.g. [ filename-012.jpeg, filename-, 0, 12, jpeg ]
        */
        function _getFileParts( file ){
            var partsEx = /([^\/\\\?\%\*\:\|\"\'\<\>]*[^\d\.])*(0*)?(\d*)?\.([a-z0-9]{0,4})/i;
            return file.filename.match( partsEx );
        }

        function _naturalSort( arr ){
          
            arr.sort( function(a,b){
               
                var ma = _getFileParts( a ),
                    mb = _getFileParts( b ),
                    a_str = ma[1],
                    b_str = mb[1],
                    a_num = parseInt( (ma[2] || 0) + (ma[3] || 0 ),10),
                    b_num = parseInt( (mb[2] || 0) + (mb[3] || 0 ),10),
                    a_rem = ma[4],
                    b_rem = mb[4];

                return a_str > b_str ? 1 : a_str < b_str ? -1 : a_num > b_num ? 1 : a_num < b_num ? -1 : a_rem > b_rem;  
            });

            return arr;

        }



        function _parseBytes( bytes ){
            var unit = bytes < 1024*1024 ? 'KB' : 'MB';
            var size = bytes/ ( unit == 'MB' ? 1024*1024 : 1024 );
            return  size.toFixed(1) + ' ' + unit;
        }

        self.with = function( target, type  ){ //itype = image,video or '' for all
            self.multiple = target ?  _.isArray( ko.unwrap( target ) )  : false;
            _input.attr( 'multi', self.multiple);
            self.targetContentType = type || '';
            self.target = target;
            self.fileList.valueHasMutated();
            _updateList();

        }


        function _addToTarget( file ){
            if( ! self.target ) return;
            var target = ko.unwrap( self.target );
            if( _.isArray( target ) ){
                 self.target.push( file );
            }
            else{
                self.target( file );
            }

            _updateList();
            return file;
        }


        function _removeFromTarget( file ){
            if(!self.target) return 
            
            var target = ko.unwrap( self.target );
            
            if( _.isArray( target  ) ){
                file = _.find( target, { filename:file.filename } );
                self.target.remove( file );
            }
            else if(self.target()){
                self.target( null );
            }

            _updateList();

            return file
        }


        self.remove = function( file ){

            var items = _.has( file,'children') ? file.children : [file];

            _.each(items, function(item){
                mongo('fs.files').remove({ _id:item._id}).run( function( data ){
                    self.fileList.remove( item );
                });
                _removeFromTarget( item );
            })
        }

        self.toggleSequence = function( sequence ){
            var i = self.groupedFiles().indexOf( sequence );

            $( '.file-list',view).children().eq(i).children('.children-wrapper').toggle();


        }

        self.select = function( file,event ){
            if( _.isArray( ko.unwrap( self.target ) ) 
                && _.has( file,'children') ){
            
                self.target( file.children.length == 0 || _targetContains( file.children[0] ) ? [] : file.children );
                _updateList();
                
            }
            else{
                if(_.has( file,'children') )
                    file = file.children[0] || null;
                (_targetContains( file ) ? _removeFromTarget( file ) : _addToTarget( file ) );
            }
                
        }

        function _targetContains( file ){
            var target = ko.unwrap( self.target );
            if(!target) return false
            return ( _.isArray( target ) ? Boolean(_.find( target, { 'filename':file.filename } ) ) : target.filename == file.filename);
        }

        function _isValidTarget(file){
            if(!self.targetContentType) return true;
            return file.contentType.indexOf( self.targetContentType ) == 0;
        }


        function _updateList(){

            var ul = _fileListView.children();

            var files = self.groupedFiles();

            var target = ko.unwrap(self.target);

            var file;

            var checkStyle = function( file, node ){
                var n = $( node );
                ( _targetContains( file ) ? n.addClass( 'active ') : n.removeClass('active') );
                ( !_isValidTarget( file ) ? n.addClass( 'disabled ') : n.removeClass( 'disabled ') );
            }

            ul.each( function(){
                file = files[ $(this).index() ];
                checkStyle( file, $(this) );

                if( _.has( file, 'children' ) ){
                    $( '.list-group', $(this) ).children().each( function(){
                        checkStyle( file.children[ $(this).index() ], this)
                    });
                }
            });

            
        }


        self.position = function( position ){
            if(!position || position == undefined ) return;

            $(_view).css( { top:'',left:'',bottom:'',right:''});
            $(_view).css( position );
            return this;

        }


        function _hitTest( x,y ){
            var bounds = _getBounds();
            return x > bounds.left && x< bounds.right && y > bounds.top && y < bounds.bottom;
        }

        function _getBounds(){
            var bounds = { top:0,left:0,width:0,height:0,right:0,bottom:0};

            if(!_view)
                return bounds;

            var pos = _view.offset();
            bounds.width = _view.outerWidth();
            bounds.height = _view.outerHeight();
            bounds.left = pos.left;
            bounds.top = pos.top;
            bounds.right = bounds.left + bounds.width;
            bounds.bottom = bounds.top + bounds.height;
        
            return bounds
        }

        self.show = function( ){
            if(! _view.parents('dropup').hasClass('open') )
                _view.siblings('.dropdown-toggle').dropdown('toggle');
            _updateList();
        }

        self.bytesToUnit = function( bytes ){
            return _parseBytes( bytes )
        }

        self.hasFiles = ko.computed( function(){
            fileList = ko.unwrap( self.fileList )
            if(!fileList) return false
            return fileList.length == 0
        })

        


        function _runNext(){
           
            if( self.queue().length == 0 ||  self.running().length == self.maxUploads  )
                return
           
            var next = self.queue.shift();
            self.running.push( next );
           
            var formData = new FormData();
            formData.append('file', next.data);
            formData.append( 'bucket',  self.id );
            
            var xhr = new XMLHttpRequest();
            xhr.upload.onprogress = _onProgress;
            xhr.onload = _onComplete;
            xhr.onerror = _onFailed;
            xhr.onabort =  _onCanceled;
            xhr.upload._target = next;

            xhr.open('POST', '/upload', true);
            xhr.send( formData );
            
        }

        function _onProgress( e ){

            var origin = e.target._target;
            origin.progress( e.loaded/e.total );


        }

        function _onComplete( e ){
            var file = JSON.parse( e.target.response )[0];
            var origin = e.target.upload._target;

            var fileList = self.fileList();
            _.remove( fileList, { filename: file.filename } );

            fileList.push( file )

            self.fileList(  fileList  );

            self.running.remove( function( item ) { return item.created == origin.created } );
           
            _runNext();
        }

        function _onFailed( e ){

        }

        function _onCanceled( e ){

        }

        
        _load( self.id );



        _view.parents('.dropup').on('hidden.bs.dropdown',function(){
            self.target = null;
            self.targetContentType = '';
            _updateList();
        });

        ko.bindingHandlers.bucket = {
            init:function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext){
                $(element).css( { cursor: 'pointer' } );
                $(element).on('click',function(e){
                    var va = valueAccessor();   
                    var ab = allBindingsAccessor();

                    self.with( va, ab['contentType'] || '');
                    if( ! _view.parent().hasClass('open') )
                        _view.siblings('.dropdown-toggle').dropdown('toggle');

                    e.stopPropagation();
                    
                });
            },
            update:function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext){
                var va = valueAccessor();
                var val = ko.unwrap( va );
                if( ! _.isArray( val ) ){
                    if(  val && _.contains(val.contentType, 'image' ) )
                        $( element ).css( { backgroundImage: "url('/files/" + self.id + '/' + ko.unwrap( val.filename ) + "')" });
                    else 
                        $( element ).css( { backgroundImage: '' } );
                }
            }

        }

        
    
    }

});