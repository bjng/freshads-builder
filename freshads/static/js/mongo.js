define( ['tasks'], function( tasks ){
	

	function send( collection, method, data, callback ){
		data.name = collection;
		data.method = method;
		$.post( '/api/'+ collection+'/' + method, {data: JSON.stringify( data ) })
			.done( function( data ){ 
				if(callback)
					callback( data ) 
			})
			.fail( function( xhr,textStatus, errorThrown ){ 
				if(callback) 
					callback( JSON.parse( xhr.responseText ) ); 
			});
	}

	tasks.mongo = { 
		FindOne : (function(){
			var FindOne = function( collection, query, projection){
				this.collection = collection || null;
				this.query = query || {};
				this.projection = projection || null;
			};
			FindOne.prototype = Object.create( tasks.Task );
			FindOne.prototype.constructor = FindOne;
			FindOne.prototype.run = function( callback ){
				var t = this;
				if( !t.query ){
					throw new Error( "No Query defined for Find" )
				}
				if(! t.collection || ! t.query)
					if( callback ) callback.call( t, null )	;
				send( t.collection, 'findOne', { query:t.query, projection:t.projection }, function( data ){ 
					if(callback)
						callback.call(t,data);
				} );
			}
			return FindOne;
			})(),


		Find: (function(){
			var Find = function( collection,query,projection ){
				this.collection = collection || null;
				this.query = query || {};
				this.projection = projection || null;
				this._skip = 0;
				this._limit = 0;
				this._sort = null;
			};
			Find.prototype = Object.create( tasks.Task );
			Find.prototype.constructor = Find;
			Find.prototype.skip = function( value ){  };
			Find.prototype.limit = function( value ){ 
				if( value == undefined) return this._limit 
				else this._limit = value;
				return this
			};
			Find.prototype.sort = function( value ){
				if( value == undefined) return this._sort 
				else this._sort = value;
                return this;
            };
			Find.prototype.skip = function( value ){ 
				if( value == undefined) return this._skip 
				else this._skip = value	
				return this
			};
			Find.prototype.run = function( callback ){
				var t = this;
				if( !t.query ){
					throw new Error( "No Query defined for Find" )
				}
				if(! this.collection || ! this.query)
					if( callback ) callback.call( this, null )	;
				send( this.collection, 'find', 
					{ query:this.query, projection:this.projection, limit:this._limit, sort:this._sort,skip:this._skip }, 
					function( data ){ 
						if(callback)
							callback.call(t,data);
				} );
			}
			return Find
		})(),

		Save: (function(){
			var Save = function( collection,document ){
				this.collection = collection;
				this.document = document || null;

			}
			Save.prototype = Object.create( tasks.Task )
			Save.prototype.constructor = Save;
			Save.prototype.run = function( callback ){
				var t = this;
				if( !t.document ){
					throw new Error( "No Document defined to Save" )
				}
				send( this.collection, 'save', 
					{ 'document':t.document  }, 
					function( data ){ 
						if(callback)
							callback.call(t,data);
				} );
			}
			return Save
		})(),
		
		Remove: (function(){
			var Remove = function( collection,query ){
				this.collection = collection;
				this.query = query || null;

			}
			Remove.prototype = Object.create( tasks.Task )
			Remove.prototype.constructor = Remove;
			Remove.prototype.run = function( callback ){
				var t = this;
				if( !t.query ){
					throw new Error( "No Document Query defined for Remove" )
				}
				send( this.collection, 'remove', 
					{ 'query':t.query  }, 
					function( data ){ 
						if(callback)
							callback.call(t,data);
				} );
			}
			return Remove
		})()
	}

	return function( collection ){
		if(!collection){
			throw new Error( 'Collection not defined');
		}
		return {
			findOne: function( query, projection ){
				return new tasks.mongo.FindOne( collection,query, projection )
			},
			find: function( query,projection){
				return new tasks.mongo.Find( collection,query,projection)
			},
			save: function( document ){
				return new tasks.mongo.Save( collection, document )
			},
			remove: function( query ){
				return new tasks.mongo.Remove( collection, query )
			}
		}
	}

	

	

})