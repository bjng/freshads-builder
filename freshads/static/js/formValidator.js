define( [ 'bootstrap'], function( bootstrap){

	self.validateForm = function( form ){
		var formValid = 1;

		$('input', form).each( function(){

			var pattern = $( this ).attr('pattern');
			var valid   = false;
			var name   = $(this).attr('name');

			//if ( name.val() == null ) alert('required');

			if( pattern && pattern.indexOf('#') == 0){

				var target = $( pattern,form );
				valid = target.val() == $( this ).val();
			}
			else {
				valid = $( this ).val().match( $( this ).attr('pattern') );
			}

			if( !valid || valid.length == 0 ){
				
				$(this).popover('show');
				$(this).one('click', function(){
					$(this).popover('hide');
				});
				formValid = 0;
			}

		});
		if(formValid){
			$(form).parents('.modal').modal('hide');
			//console.log('parents',$(this).parents('.modal'));
		}

		return formValid;
	}

	return self;
	
});