define([], function( app ){
	var tasks = {};

	tasks.run = function( tasks,callback ){
			var self = this;
			var i=0,total = tasks.length, complete= 0, task;
			var results = new Array( tasks.length );
			for( i;i<total;i++ ){
				task = tasks[ i ];
				if(task){
					task.run( function( data ){
						complete++;
						results[ tasks.indexOf( this ) ] = data;
						if(complete == total && callback)
							callback( results )
					})
				}else{
					complete++;
					results[i]=null;
					if(complete == total && callback)
						callback( results )
				}
			}
	};

	

	tasks.undo = tasks.run;

	tasks.Task = (function(){
		var Task = function(){};
		Task.prototype.run = function(){
			if(callback)
				return callback.call( this, null );
		}
		Task.prototype.undo = function( callback ){
			if(callback)
				return callback.call( this, null );
		}
		return Task;	
	})()

	tasks.Load = (function(){
		function Load( url, data, type ){
			this.url = url;
			this.data = data;
			this.type = type;
		}
		Load.prototype = Object.create( tasks.Task.prototype );
		Load.prototype.constructor = Load; 
		Load.prototype.run = function( callback ){
			var t = this;
			$.ajax( this.url, {
				data:this.data,
				type: this.type || 'GET',
				cache: true,
			})
			.done( function(data){ 
				callback.call( t,data )
			})
			.fail(function(e){ throw new Error( e.status + ": " + e.statusText ) })
		}

		return Load;
	})()

	tasks.Require = (function(){
		var Require = function( module ){
			this.modules = [ module ];
		};

		Require.prototype = Object.create( tasks.Task.prototype );
		Require.prototype.constructor = Require;

		Require.prototype.run = function(callback){
			var t = this;
			require( this.modules,function( m ){
				if(callback)
					callback.call( t, m );
			});
		}
		return Require;

	})();


	return tasks;
})