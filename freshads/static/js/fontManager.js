define( [ 'lodash','knockout'],function( _,ko){

	var FILE_TYPES = [ 'ttf','woff','eot','svg'];
	var FORMAT_MAP = [ 'truetype','woff','eot','svg'];
	var TAGS = [ 'P','H1','H2','H3','H4','a'];
	var DEFAULT_FAMILIES = ['Serif', 'Sans-Serif', 'Arial','Courier', 'Courier New', 'Georgia','Helvetica','Palatino','Times New Roman','Trebutchet MS', 'Verdana'];
	var DEFAULT_WEIGHTS = ['',100,200,300,400,500,600,700,800,900]
	var DEFAULT_STYLE = { 
			fontWeight:'',
			color:'',
			fontSize:'',
			fontFamily:'',
			lineHeight: '',
			letterSpacing: ''
		}

	function FontFace( name, files ){
		this.name = name;
		this.files = files || [];
	}

	function TagStyle( name, css){
		if(!name) throw new Error( "Tag Name not defined for TagStyle");
		this.name = name;
		this.style = css ? _.extend(  DEFAULT_STYLE, css ) : _.clone( DEFAULT_STYLE );
	}



	var DEFAULT_FONTS = _.map( DEFAULT_FAMILIES, function( family ){
			return new FontFace( family )
	} )
	

	return function( view, adModel, bucket ){	

		var self = this;

		self.weights = DEFAULT_WEIGHTS;
		self.tagStyle  = ko.observable();
		self.styleSheet;
		self.placeHolder = "Lorem Ipsum";

		self.currentStyle;

		adModel.subscribe( function(ad){
			ad.styleSheet =  ad.styleSheet || [];
			self.styleSheet = ad.styleSheet;


			updateStyles();
		});

		$(view).on('change','input,select',updateStyles);

		self.fonts = [];
		self.tags = TAGS;
		
		self.tagStyle  = ko.observable();

		self.selectedTag = ko.observable();

		self.selectedTag.subscribe( function( tag ){

			if(!tag){
				self.tagStyle(null);
				return
			}

			var sheet = self.styleSheet;

			if(!sheet) return;
			
			var tagStyle = _.find( sheet, { name: tag } );
			
			
			if( !tagStyle ){
				tagStyle = new TagStyle( tag );
				
				sheet.push( tagStyle );
			}
			_.extend( DEFAULT_STYLE,tagStyle.style);
		
			self.tagStyle( tagStyle );
		})



		self.setFontFamily = function( font ){
			self.tagStyle().style.fontFamily( font.name );
		}

		bucket.fileList.subscribe( function( files ){
			var fonts = [ new FontFace('',[]) ];

			_.each( files, function( file ){
				var nameType = file.filename.split('.');
	
				if( FILE_TYPES.indexOf( nameType[1] ) ==-1 )
					return
				var fontFace = _.find( fonts, {name: nameType[0]} );
				if( fontFace )
					fontFace.files.push( file.filename );
				else{
					fontFace = new FontFace( nameType[0], [ file.filename ] );
					fonts.push( fontFace )
				}
			} )

		
			self.fonts = _.union( fonts, DEFAULT_FONTS );

			updateStyles();
		})



		function updateStyles(){

			$('style',view).remove();
			$(view).append( _createStyles( self.fonts,self.styleSheet ) );
		}

		
		function camelToDash(str) {
	      	return str.replace(/\W+/g, '-')
	                .replace(/([a-z\d])([A-Z])/g, '$1-$2').toLowerCase();
		}


		function _createStyles( fonts,styleSheet ){

			var styleNode = $('<style data-freshads="freshads" type="text/css"/>')
			_.each( fonts, function( font ){
				if( _.has( font, 'files') && font.files.length > 0){
					styleNode.append( _createFontFace( font ) );
				}
				
			});
			_.each( styleSheet, function( val ){
				styleNode.append( _createDefinition( val.name, ko.mapping.toJS( val.style ) ) );
			})



			return styleNode;
		}

		function _createDefinition( name,style ){
			var str = '#preview ' + name + '{';
			_.each( style, function( val, key){
				str+= camelToDash( key ) + ':' + val + ';';
			})
			str += '}';
			return str
			

		}

		function _createFontFace( font ){
			var files = font.files;
			if( files.length == 0 )
				return

			var html = '@font-face {font-family:' + font.name + ';';
			if(files.length > 0 ){
				html += 'src:';
			}
			_.each( files, function( file,i ){
				var type = FORMAT_MAP[ FILE_TYPES.indexOf( file.split('.')[1] ) ];
				html += 'url("/files/' + bucket.id + '/' + file + '") format("' + type + '")';
				if(i+1<files.length)
					html+=','
			});
			if(files.length > 0 ){
				html+=';';
			}

			html += '}'
			return html

		}

	}

} )