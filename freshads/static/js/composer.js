define(['lodash',  'knockout','bucketManager','fontManager','canvasControl','mongo','tasks','bootstrap'],
	function(_,ko,Bucket,FontManager,CanvasControl,mongo,tasks){
		
		//view controller
		return function( view ){

			window.app.composer = this;
			
			var d = new Date();
			var yesterday = new Date();
			d.setDate( d.getDate() - 1 );

			var self = this;
			self.id = { '$oid': window.location.pathname.split('/')[2] };
			self.view = $( view );
			//self.view.on('click','input',function(e){ e.stopPropagation() });

/* DOCUMENT EVENTS */


			/* prevents page exit if changes to the ad haven't been saved */
			$( window ).on('beforeunload',function() {
				if( ko.unwrap( self.modelChanged ) )
			  		return "You changes haven't been saved yet.";
			});


			// Hide the smart search on scroll
			var myScrollObject = $('#adboard',view);
			var up = false;
			myScrollObject.scroll(function () {
			    var newScroll = myScrollObject.scrollTop();
			    if (newScroll > 0 && !up) {
			        $('.center').stop().animate( { top : '-100px' },200 );
			        up = !up;
			    } else if(newScroll == 0 && up) {
			        $('.center').stop().animate( { top : '0px' },200 );
			        up = !up;
			    }

			});


			
			function _reloadWarning(){
				app.warning( "Please <strong>refresh</strong> the page. You haven't made any changes for a while and another user might have made changes.");
			}

			var timeOutAfter = 30 * 60 * 1000; //30 min
			var interactionTimeout = setTimeout( _reloadWarning, timeOutAfter );//30min
			$( document ).click( function(){
				clearTimeout( interactionTimeout );
				interactionTimeout = setTimeout( _reloadWarning, timeOutAfter); //30 min
			});



	
/* SUBVIEWS */
			_previewView = $( '#preview', view);
			_componentsDropdown = $( '#componentsDropdown',view);

/* PUBLIC CONTROLLER PROPERTIES */

			self.proof = ko.observable();
			self.componentsPrototypes = ko.observableArray([]);


			self.modelChanged = ko.observable( false );
			self.modelLocked = ko.observable( false );

			self.adModel = ko.observable();
			self.adModel.subscribe( function( model ){

				self.modelChanged( false );
				self.modelLocked( false );

				if( ko.unwrap( model.lock ) ){
					self.modelLocked( true );
					var m = $('<div/>');
					var unlockBtn = $( '<button>Unlock</button>');
					unlockBtn.addClass('btn btn-xs btn-default btn-warning pull-right');
					m.html( 'This Ad is <strong>Locked!</strong> It looks like someone else is editing it. ' );
					m.append( unlockBtn );
					
					var warning =  window.app.warning( m );

					unlockBtn.one('click', function(){
						self.adModel().lock(false);
						self.modelLocked( false );
						warning.alert('close')
					});

				};
				model.style.width.subscribe( function( val ){
					_modelHasMutated()
				})
				model.style.height.subscribe( function( val ){
					_modelHasMutated()
				})
				model.style.backgroundColor.subscribe( function( val ){
					_modelHasMutated()
				})
				model.data.grid.subscribe( function( val ){
					_modelHasMutated()
				})
				model.components.subscribe( function( val){
					_modelHasMutated()
				})
				
				updateExpandable();

				_refreshPreview( function(){
					updateDisplay();
				})

			} );

			self.adSiblings = ko.observableArray();


			self.save = function(){
				
				var components = ko.unwrap( self.adModel().components );

				var adData = ko.mapping.toJS( self.adModel() );
			
				if(adData.lock)
					return;
				//adData.components = components;
			    mongo('ads').save( adData ).run( 
			    	function( data ){
			           	self.adModel( _parseAdData( data ) );
		        	}
			    )
			}

			self.generateProof = function(){
				mongo( 'proofs' ).save( { target: self.id.$oid } ).run( function( data ){
					self.proof( data );
				})
			}


			self.modifiedToString = ko.computed( function(){
				if( !self.adModel() ) return ''
				var date = new Date( ko.unwrap(self.adModel().modified['$date'] ));
				return date.toLocaleString();
			})
			self.insertionToString = ko.computed( function(){
				if( !self.adModel() ) return ''
				var date = new Date( ko.unwrap(self.adModel().insertionDate ) );
				return date.toDateString();
			})


			self.createComponent = function( prototype ){

				var components = ko.unwrap( self.adModel().components );

				var adWidth = ko.unwrap( self.adModel().style.width );
				var adHeight = ko.unwrap( self.adModel().style.height );

			    var c = _canvasControl;

			    var data = _.clone( prototype.model, true );


			    var typeCounts = _.countBy( components, function( val ){ 
			        return val.type
			    } );

			    var name = prototype.verboseName + ' ' + ( _.has( typeCounts, prototype._id.$oid ) ? typeCounts[ prototype._id.$oid ] : '' );              


		        var x= 100* (c.selection.left()/adWidth) || 0,
		        y = 100* (c.selection.top()/adHeight) || 0;
		        w = 100*(Math.max(10,c.selection.width)/adWidth)  || 100 - x,
		        h= 100*(Math.max(10,c.selection.height)/adHeight)  || 100 - y;

			    var component= { 
			        type:prototype.type, 
			        id: new Date().getTime(),
			        name:name, 
			        data: data,
			        style: {top:y+'%',left:x+'%',width:w + "%",height:h+"%"},
			        components: [],
			    };

			   
			   	self.editComponent( component );
				updateDisplay();

				
			}

/* Top Right Menu */

		self.showGrid = ko.observable(true);
		self.toggleGrid = function(){
		    self.showGrid( ! self.showGrid() );
		    _canvasControl.showGrid( self.showGrid() );
		    _canvasControl.snapGrid = self.showGrid();

		}

		self.showGuides = ko.observable( true );
		self.toggleGuides = function(){
		    self.showGuides( ! self.showGuides() );
		    _canvasControl.showGuides( self.showGuides() );
		    _canvasControl.snapGuides = self.showGuides();
		}

		self.toggleGridSnap = function(){
		    _canvasControl.snapGrid = !self.canvasControl.snapGrid;
		}

		self.runMode = ko.observable(true);
		self.toggleRun = function(){
		    self.runMode( !self.runMode() );
		}

		self.bundle = function( controller ){
			window.location = '/bundle/' + self.id.$oid;
			//$.get('bundle/' + self.ad()._id.$oid )
		}


/* Edit Component */


		var _originalComponent;
		var _componentEditor = $('#componentEditor').modal();
		_componentEditor.modal('hide');
		self.component = ko.observable( );
		self.component.subscribe( function( comp ){
			if(comp){
				_canvasControl.active( false ); 
				_componentEditor.modal('show');
			}
			else{ 
				_componentEditor.modal('hide');
				ko.cleanNode( $(  '#componentEditor .modal-body' ).children().first()[0] );
	    		$(  '#componentEditor .modal-body' ).html( '' );
	    		_originalComponent = null;
	    		_canvasControl.active( true ); 
	    		_originalComponent = null;
	    		_canvasControl.active( true ); 
	    	}
		});

		self.editComponent = function( component ){
		    if(!component) return;

		    _canvasControl.active( false );

		    _originalComponent = component;
		    component = _.clone( component, true );

			var cSrc = '/components/' + component.type + '/' + component.type + '-edit';
			
			_loadViewController( cSrc, '../' + cSrc, function( v,c){
				$(  '.modal-body', _componentEditor ).html( v );
				new c( v, component );
				self.component( component )
			})
		}



		self.updateComponent = function( ){

			if(! ko.unwrap( self.component ) ) 
				return;
			

			var comp = self.component();
			var components =  self.adModel().components;

			var cleanedData = ko.mapping.toJS( self.component() );


			
		    var i = _.findIndex( ko.unwrap( components ), { id:_originalComponent.id } );
		    if(i==-1) 
		    	components.push( cleanedData );
		    else
		    	components.splice( i, 1, cleanedData );

		    self.component( null );
	  
	   		self.save();
		}



		_componentEditor.on('hidden.bs.modal', function(){ self.component( null ) } );


		self.removeComponent = function( ){
			var components = self.adModel().components;
			var found = _.find( ko.unwrap( components ), { id:_originalComponent.id } );
			if(found)
				components.remove( found );
			self.component( null );
			self.save()
		}


		tasks.run([ 
			mongo( 'ads').findOne({ '_id' : self.id } ),
			mongo( 'components.prototype').find(),
			mongo( 'proofs').findOne( { target:self.id['$oid']} )
		], function( results ){

			mongo( 'ads')
				.find( 
					{ parent: results[0].parent }, 
					{ id:1,name:1 } )
				.run( function( siblings ){
					_.remove(siblings, {id:results[0].id});
					siblings.unshift({id:null,name:'None'});

					self.adModel( _parseAdData( results[0] ) );
					self.componentsPrototypes( results[1] );
					self.proof( results[2] );
					self.adSiblings( siblings );
					

				});

		});

		function _parseAdData( data ){
			if(! _.has( data,'components') )
				data.components = [];
			if(! _.has( data,'expandable') )
				data.expandable='';

			var mapping = { 'copy': ['components','data.guides','expandable','id','_id','parent','styleSheet'] };

			var mapped = ko.mapping.fromJS( data,mapping );
			mapped.components = ko.observableArray( data.components );
			mapped.expandable = ko.observable( data.expandable );
			mapped.guides = ko.observableArray( data.guides );
	
			return mapped;
		}

		function _modelHasMutated(){
			self.modelChanged(true);
			updateDisplay();
		}

		function updateDisplay(){
			if(! self.adModel()) return

			var model = self.adModel();

			components = ko.unwrap( model.components );
			 _.each( components,function( comp ){ //sorting
		    	var target = $('#' + comp.id).css( comp.style );
		    	if(target.length)
		    		$(target[0]).trigger( 'changed' );
		    	$('#preview').append( target );
		    });

			$('#preview').width( ko.unwrap( model.style.width ) );
			$('#preview').height( ko.unwrap( model.style.height ) );
			$('#preview').css( 'backgroundColor', ko.unwrap( model.style.backgroundColor ) );

		}

		function _refreshPreview( callback ){
			if(! self.adModel()) return;

	    	var ad = ko.unwrap( self.adModel );
	     
			$('#preview',view).load( '/preview/' + self.id.$oid + '?t=' + new Date().getTime() );
			$('#preview',view).css(  {backgroundColor:ad.backgroundColor,width:ad.width,height: ad.height} );
		 	if(callback)
				callback();
		}

/* Module - Canvas Renderer */

		_canvasControl = new CanvasControl( self, $('canvas',view )[0] );
		_canvasControl.onSelectionComplete = function(e){
			_componentsDropdown.css( { left:e.position.x,top:e.position.y } );
			$(document).one('click',function(e){ //selection complete fires on mouseup
				$(_componentsDropdown).addClass( 'show' );
				_canvasControl.active(false)
				$(this).one( 'click', function(){
					_canvasControl.selection = null;
					_canvasControl.active(true)
					_componentsDropdown.removeClass( 'show' );
				})	
			})

		}

		_canvasControl.onHandleComplete = function( component ){
			
			updateDisplay();
		    //self.ad.valueHasMutated();
		    //self.save();
		};

		$(document).dblclick( function( e ){
			if(!_canvasControl.isActive() ) return
		    if(_canvasControl.activeComponent){
		        self.editComponent( _canvasControl.activeComponent );
		    }

		} );

/* Module - BucketManager */

		self.bucket = new Bucket( self.id['$oid'], $( '#bucket', view )  );


/* Module - Font Manager */

		self.fontManager = new FontManager( '#fontManager', self.adModel, self.bucket );

		
/* Module - Expandable */

		
		
		self.selectedExpandable = ko.observable()
		self.selectedExpandable.subscribe( function(val){
			self.adModel().expandable = val;
		});



		self.adSiblings.subscribe( function( adSiblings){
			updateExpandable();
		});

		function updateExpandable(){
			var adModel = ko.unwrap( self.adModel ) 
			if( !adModel) return

			var siblings = ko.unwrap( self.adSiblings );
			var expandable = ko.unwrap( adModel.expandable )
			if(expandable && siblings){
				var existing = _.find( siblings,{ id: expandable.$oid } );
				self.selectedExpandable( existing ? existing._id : siblings[0] )	
			}
			
		}


		ko.applyBindings( self, self.view[0])

	}

	//Module functions
	
	//helper function to load components view and controllers
	function _loadViewController( viewName, controllerName, callback ){
		tasks.run(
			[	
				viewName ? new tasks.Load( '/views/' + viewName ) : null,
				controllerName ? new tasks.Require( controllerName ) : null
			],
			function( results ){
				v = $(results[0])[0],c = results[1];
				if(callback)
					callback(v,c)
			}
		);
	};

	//prevent default backspace navigation
	$(document).on('keydown', function (event) {
	    if (event.keyCode === 8){
	    
	    	var target = event.target;

	   	
	    	var editable = String( $(target).attr( 'contenteditable' ) ).toUpperCase() === 'TRUE'
	    		|| target.tagName.toUpperCase() === 'INPUT'
	    		|| target.tagName.toUpperCase() === 'TEXTAREA';
	    	if( editable )
	        	return 1;
	   		event.preventDefault();
		}
	});

	//Hide Smart Search on scroll down


});
	

	


