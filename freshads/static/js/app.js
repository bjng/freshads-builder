requirejs.config(
  { baseUrl: '/static/js/',
    paths:{
        lodash: 'lib/lodash.min',
        knockout:'lib/knockout-3.0.0',
        'knockout.mapping':'lib/knockout.mapping',
        bootstrap:'lib/bootstrap.min',
        'jquery.hotkeys':'lib/jquery.hotkeys',
        prettify:'lib/prettify',
        'jquery.slimScroll':'lib/jquery.slimscroll.min',
        datepicker:'lib/bootstrap-datepicker',
        wysiwyg: 'lib/bootstrap-wysiwyg',
        redactor: 'lib/redactor.min',
        'colorpicker': 'lib/bootstrap-colorpicker.min',
        'chart': 'lib/Chart.min',
        },
      shim: {
      	'jquery.slimscrollcroll': ['jquery'],
		'chart':{
		  exports:"Chart"
		},
		'datepicker':{ deps:['bootstrap'] },
		'colorpicker':{ deps:['bootstrap']  }
      },
      deps:['bootstrap'],
  } 
);

window.app = (function(){
		
	var self = this;
	self.staticsRoot = "static/§"
	self.filesRoot = "files/"

	self.warning = function( content ){
		return _createMessage( content, 'warning' );
	}

	self.danger = function( content ){
		return _createMessage( content, 'danger' );
	}

	self.info = function( content ){
		return _createMessage( content, 'info' );
	}

	self.success = function( content ){
		return _createMessage( content, 'success' );
	}

	function _createMessage( content, type){
		var m = $( '<div class="alert" data-dismiss="alert" aria-hidden="true"/>' );
		m.addClass( );
		m.addClass('alert-' + type );
		m.addClass('alert-dismissable');
		m.append( $('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>') );
		m.append( $('<div/>').html(content) );
		$('#messages').append(m);
		return m;
	}

	return self;

})(window);



require( ['lodash','jquery','knockout','knockout.mapping','tasks','mongo','datepicker','colorpicker'], function(_,$,ko,komapping,tasks,mongo){
	
/* logging */



/* polyfills */

	if (typeof Object.create !== 'function') {
	    Object.create = function (o) {
	        function F() { }
	        F.prototype = o;
	        return new F();
	    };
	}

/*ko general setup*/

	ko.mapping = komapping;

	ko.bindingHandlers.stopBinding = {
	    init: function() {
	        return { controlsDescendantBindings: true };
	    }
	};

/* ko bindings */


	//Helper for non observables
	function koSetNonObservable( element,bindingHandler,viewModel, value){
		
		var originalBind = $(element).data( 'bind' );
		var regex = new RegExp( bindingHandler + ':(\\w+)');
		var key =  originalBind.match( regex )[1] || null;

		if( key && key in viewModel && !ko.isObservable( viewModel[ key ]) ){		
			viewModel[key] = value;
		}
		return key ? true : false;
	}


	ko.bindingHandlers.datepicker = {
	    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
			
			var value = ko.utils.unwrapObservable(valueAccessor());
			var options = allBindings().datepickerOptions || {};
			options['autoclose']  = options['autoclose'] || true;
			options['format']  = options['format'] || "D dd M yyyy";
			options['todayHighlight'] = options['todayHighligh'] || true;
			options['todayBtn'] = options['todayBtn'] || 'linked';
			var dp = $(element).datepicker( options );

			$(element).on( "change", function (){
			    var va = valueAccessor();
			    var value = $(element).datepicker('getDate').getTime();
			    
			   	if (ko.isObservable(va)){
			    	va( value );
			   	}
			    else{
			    	koSetNonObservable( element, 'datepicker', viewModel,value);
			    }
			});

	        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
	            $(element).datepicker("destroy");
	        });

	    },

	    update: function(element, valueAccessor){
	    	var value = ko.utils.unwrapObservable( valueAccessor() );
	    	if( value){
	    		$(element).datepicker('update', new Date(value) );
	    	}
	    }
	};


	ko.bindingHandlers.colorpicker = {
	    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
			
			var value = ko.utils.unwrapObservable(valueAccessor());
	
			var dp = $(element).colorpicker( );

			$(element).on( "change", function (){
			    var va = valueAccessor();
			    var value = this.value;
			   	if (ko.isObservable(va)){
			    	va( value );
			   	}
			    else{
			    	koSetNonObservable( element, 'colorpicker', viewModel,value);
			    }
			});

	        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
	            $(element).colorpicker("destroy");
	        });

	    },

	    update: function(element, valueAccessor){
	    	var value = ko.utils.unwrapObservable( valueAccessor() );
	    	if( value){
	    		$(element).colorpicker('setValue', value );
	    	}
	    }
	};

	//Prevent default to avoind the modal closing on change of color
	$(document).on('click','.colorpicker',function(e){ e.stopPropagation() });




/*smart search*/


	$(document).ready(function(){

		var _query='';
		var _queryTimeout;

		$('.smart-search input').on('click','',function(e){			
			return !$('.smart-search .dropdown').hasClass('open')
		});
		$('.smart-search input').on('keyup',function(e){
			_query = $(this).val();
			clearTimeout( _queryTimeout );
			if( _query.length < 4 ) return;
			$( '.smart-search .dropdown-menu' ).remove();
			_queryTimeout = setTimeout( function(){
				$.get('/search?q=' + _query, function( data ){
					$( '.smart-search .dropdown' ).append( data );
				});
			},800)
		});


		//CONFIRM DELETE

		var deleteModal = (function(){

			var _target;

			$(document).on('click','#confirm .btn-success',function(e){
					if(_target)
						_target.trigger('click');
						$('#confirm').modal('hide');
						_target = null;
			});

			$('#confirm').on('hidden.bs.modal', function(){
				_target = null;
			});

			$(document).on('mousedown','button[data-role="delete"]',function(e){ 
				//open modal
				$('#confirm').modal('show');
				_target = $(this);

				e.stopImmediatePropagation();
				
				return 0;
			});

		})();

		$(document).on('click','.stopPropagation',function(e){ e.stopPropagation() });

	});




})




