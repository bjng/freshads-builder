from pyramid.config import Configurator

from urllib.parse import urlparse

from pymongo import MongoClient
import bson.json_util
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.renderers import JSON

from freshads.resources import Root
from freshads.resources import groupfinder

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings,root_factory=Root)

    config.include('pyramid_mako')
    config.add_static_view( 'static', 'static', cache_max_age=0) #3600
   
    config.add_renderer( 'json', JSON( serializer= bson.json_util.dumps )  )

    authn_policy = AuthTktAuthenticationPolicy('seekrit',callback=groupfinder, hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()

    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    
    db_host = urlparse(settings['mongo_host'])
    analytics_db_host = urlparse(settings['analytics_host'])
    
    mongo = MongoClient(
        host=db_host.hostname,
        port=db_host.port,
    )

    if 'mongo_user' in settings and settings['mongo_user'] != '':
        mongo.freshads.authenticate(settings['mongo_user'],settings['mongo_pass'])


    config.registry.mongo = mongo
    config.registry.db = mongo.freshads

    mongo_analytics = MongoClient(
        host=analytics_db_host.hostname,
        port=analytics_db_host.port,
    )

    if 'analytics_user' in settings and settings['analytics_user'] != '':
        mongo_analytics.analytics.authenticate(settings['analytics_user'],settings['analytics_pass'])

    config.registry.analytics_mongo = mongo_analytics
    config.registry.analytics_db = mongo_analytics.analytics


    def add_analytics_db(request):
       return config.registry.analytics_db

    def add_db(request):
       return config.registry.db
    
    config.add_request_method(add_db, 'db', reify=True)
    config.add_request_method(add_analytics_db, 'analytics_db', reify=True)
      
    config.scan('freshads')
    
    return config.make_wsgi_app()
