from pyramid.view import view_config,forbidden_view_config
from pyramid.response import Response, FileResponse
from pyramid.renderers import render_to_response, render
from pyramid.httpexceptions import HTTPFound,HTTPNotFound, HTTPBadRequest
from pyramid.security import remember,forget,authenticated_userid
from pyramid.path import AssetResolver


from bson.json_util import dumps, loads
from bson.objectid import ObjectId

import gridfs
import sys

from freshads.resources import Root


@forbidden_view_config(renderer='freshads:templates/login.mako')
def login(request):
    came_from = request.params.get('came_from', request.url)
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login'].lower()
        password = request.params['password']
        user = request.db.users.find_one( { 'email':login} )

        if user and user.get('password') == password:
            headers = remember(request, login)
            goto = came_from;
            return HTTPFound(location = goto,
                             headers = headers)

        message = 'Username or password incorrect'

    return dict(
        message = message,
        url = request.application_url,
        came_from = came_from,
        login = login,
        password = password,
        )

@view_config(context='freshads:Root', name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location = request.resource_url(request.context),
                     headers = headers)

@view_config(context='freshads:Root', name='analytics', permission='view', renderer='freshads:templates/analytics.mako')
def analytics(request):
    data = request.db.ads.find_one( ObjectId( request.subpath[0] )) if len(request.subpath) else None
    if data:
        parent = request.db.campaigns.find_one( data.get('parent') );
        user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
        return {'user': user, 'data':data, 'parent':parent}
    return HTTPNotFound();


@view_config(context='freshads:Root', permission='view')
def home_view(request):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    group = user.get('group');
    goto = '/admin/' if group  == 'superuser' or group == 'producermanager' else '/clients/' 
            
    return HTTPFound(location = goto)


@view_config(context='freshads:Root', name="clients", permission='view')
def clients_view(request):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    if request.subpath:
        data = request.db.clients.find_one( ObjectId( request.subpath[0] )) if len(request.subpath) else None
        if data:
            return render_to_response('freshads:templates/client.mako', {'user': user, 'data':data},request )
        return HTTPNotFound();

    return render_to_response('freshads:templates/clients.mako', {'user': user},request )


@view_config(context='freshads:Root', name="users", permission='view', renderer='freshads:templates/users.mako')
def users_view(request):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    return {'user': user}



@view_config(context='freshads:Root', name="admin", permission='view', renderer='freshads:templates/admin.mako')
def admin_view(request):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    return {'user': user}


@view_config(context='freshads:Root', name="templates", permission='view', renderer='freshads:templates/templates.mako')
def templates_view(request):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    return {'user': user}



@view_config(context='freshads:Root', name="campaigns", permission='view', renderer='freshads:templates/campaign.mako')
def campaigns_view(request):
    data = request.db.campaigns.find_one( ObjectId( request.subpath[0] )) if len(request.subpath) else None
    if data:
        parent = request.db.clients.find_one( data.get('parent') );
        user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
        return {'user': user, 'data':data, 'parent':parent}
    return HTTPNotFound();

@view_config(context='freshads:Root', name="ads", permission='view', renderer='freshads:templates/composer.mako')
def ads_view(request):
    data = request.db.ads.find_one( ObjectId( request.subpath[0] )) if len(request.subpath) else None
    if data:
        parent = request.db.campaigns.find_one( data.get('parent') );
        user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
        return {'user': user, 'data':data, 'parent':parent}
    return HTTPNotFound();


@view_config(context='freshads:Root', name="search",renderer='freshads:templates/search.mako')
def smart_search( context, request ):
    user = request.db.users.find_one( { 'email': authenticated_userid(request) } )
    group = user.get('group')
    finds = {}
    import re
    query = re.compile('^' + request.params.get('q',''), re.IGNORECASE)
    projection = { 'name':1,'email':1,'id':1 }
    limit = 5
    if group is not 'producer':
        finds['clients'] = request.db.clients.find( {'$or' : [ {'id' : query},{'name' : query} ] } ,projection).limit(limit)
        if not finds['clients'].count(): del finds['clients']

    finds['campaigns'] = request.db.campaigns.find( {'$or' : [ {'id' : query},{'name' : query} ] },projection).limit(limit)
    if not finds['campaigns'].count() : del finds['campaigns']
    return finds
  


@view_config( context='freshads:resources.UsersCollection',name="save",renderer='json')
@view_config( context='freshads:resources.TemplatesCollection',name="save",renderer='json')
@view_config( context='freshads:resources.ClientsCollection',name="save",renderer='json')
@view_config( context='freshads:resources.CampaignsCollection',name="save",renderer='json')
@view_config( context='freshads:resources.AdsCollection',name="save",renderer='json')
@view_config( context='freshads:resources.ProofsCollection',name="save",permission="save",renderer='json')
def api_save( context,request ):
    document = context.data.get('document')
    result = document;

    if document:
        try:
           result = context.save( document )
        except Exception as e:
            request.response.status = 400;
            return { 'error' : str( e ) };
    return document;


@view_config( context='freshads:resources.UsersCollection',name="findOne",permission="find",renderer='json')
@view_config( context='freshads:resources.TemplatesCollection',name="findOne",permission="find",renderer='json')
@view_config( context='freshads:resources.ClientsCollection',name="findOne",permission="find",renderer='json')
@view_config( context='freshads:resources.CampaignsCollection',name="findOne",permission="find",renderer='json')
@view_config( context='freshads:resources.AdsCollection',name="findOne",permission="find",renderer='json')
@view_config( context='freshads:resources.ProofsCollection',name="findOne",permission="find",renderer='json')
def api_find_one( context, request ):
    query = context.data.get('query',{})
    doc = context.find_one( query )
    return doc


@view_config( context='freshads:resources.UsersCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.TemplatesCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.ClientsCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.CampaignsCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.AdsCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.ComponentsPrototypeCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.FilesCollection',name="find",permission="find",renderer='json')
@view_config( context='freshads:resources.AnalyticsCollection',name="find",permission="find",renderer='json')
def api_find( context, request ):
    query = context.data.get('query',{})
    projection = context.data.get('projection')
    sort = context.data.get( 'sort' )
    limit = context.data.get( 'limit' )
    skip = context.data.get( 'skip' )
    result = context.find( query,projection )
    if skip:
        results.skip( skip );
    if limit:
        result.limit( limit )
    if sort:
        sort = [ (k,v) for k,v in sort.items() ]
        result.sort( sort )
    return result


@view_config( context='freshads:resources.UsersCollection',name="remove",permission="remove",renderer='json')
@view_config( context='freshads:resources.TemplatesCollection',name="remove",permission="remove",renderer='json')
@view_config( context='freshads:resources.CampaignsCollection',name="remove",permission="remove",renderer='json')
@view_config( context='freshads:resources.ClientsCollection',name="remove",permission="remove",renderer='json')
@view_config( context='freshads:resources.AdsCollection',name="remove",permission="remove",renderer='json')
@view_config( context='freshads:resources.FilesCollection',name="remove",permission="remove",renderer='json')
def api_remove( context, request ):
    query = context.data.get('query',{})
    result = context.remove( query )
    return result


@view_config(context=Root, name="views")
def views(request):
    template = "/".join( request.subpath )
    return render_to_response('freshads:templates/' + template + '.mako' ,{},request=request)


@view_config(context=Root,name="edit")
def component_edit( request ):
    type = request.subpath[0]
    return render_to_response('freshads:templates/components/' + type + '-edit.mako',{}, request=request )



@view_config(context=Root,name="proof")
def proof( request ):
    id = request.subpath[0]

    proof = request.db['proofs'].find_one( ObjectId( id ) );
    ad = request.db['ads'].find_one( ObjectId( proof.get( 'target') )  )

    assets = request.db['fs.files'].find( { 'metadata.bucket': str( ad.get( '_id')  ) } );
    import re
    fontEx = re.compile(r'(ttf|woff|eof|svg)$')
    fonts = [ asset for asset in assets if fontEx.search( asset.get('filename') ) ]

    env={}
    env['ad'] = ad
    env['assets_root'] = '../files/' + str( ad.get('_id') ) + '/'
    env['fonts'] = fonts
    env['static_root'] = "/static/"
    env['component_controller_root'] = "/static/components/" #relative from proof url
    env['analytics_url'] = request.registry.settings.get('analytics_url')
    return render_to_response( "freshads:templates/proof.mako",{'env':env}, request )

@view_config(context=Root,name="preview")
def preview( request ):
    id = request.subpath[0];
    
    ad = request.db['ads'].find_one( ObjectId( id ) )
    
    if not ad: 
        return HTTPNotFound()

    env = {};
    env['ad'] = ad
    env['assets_root'] = '/files/' + str( ad.get('_id') ) + '/'
    env['static_root'] = "/static/" 
    env['component_controller_root'] = "/static/components/"

    return render_to_response( "freshads:templates/preview.mako",{'env':env}, request );


@view_config(context=Root,name="bundle")
def bundle( request ):
    id = request.subpath[0] 
    ad = request.db['ads'].find_one( ObjectId( id ) )

    tempRoot = AssetResolver('freshads').resolve('').abspath() + '/tmp/';

    import os
    if not os.path.exists( tempRoot ):
            os.makedirs( tempRoot );

    import tempfile
    zip = tempfile.NamedTemporaryFile( dir= tempRoot, delete=True ) 

    from freshads.bundle import Bundle
    Bundle( ad, request.db,request.registry.settings ).run( zip )
    
    from pyramid.response import FileResponse
    zipname = "freshad_" + str( ad.get('_id') ) + ".zip"  
    response = FileResponse( zip.name, request )
    response.headers['Content-Disposition'] = ( 'attachment; filename="' + zipname +'"')
    response.headers['Content-Type'] = "application/zip"

    zip.close()

    return response

        


@view_config(context=Root,name="files")
def files( request ):
    bucket = request.subpath[0];
    filename = request.subpath[1];

    f = request.db.fs.files.find_one( { 'filename':filename, 'metadata.bucket': bucket } )
    if f:
        fs = gridfs.GridFS( request.db )
        with fs.get( f.get( '_id' ) ) as gridout:
            response = Response(content_type=gridout.content_type,body_file=gridout,conditional_response=True)
            response.content_length = f.get('length')
            return response

    return HTTPNotFound();



@view_config(context=Root,name="upload")
def upload(request):
    
    post = request.POST;

    from freshads.forms import parse
    form = parse( request.POST )

    data = list();
    for imageList in ( v for k,v in form.items() if isinstance( v,list) ):
            for fieldstorage in imageList:
                meta = form.get('meta');
                data.append( save_file( request.db, fieldstorage.file,  fieldstorage.filename,  fieldstorage.type, form.get('bucket',None) ) )

    return Response( dumps( data),content_type='application/json' )


def save_file( db, file, name, type, bucket):

    fs = gridfs.GridFS( db )

    existing = db.fs.files.find_one( { 'filename':name, 'metadata.bucket':bucket } );
    if existing:
        fs.delete( existing.get('_id') )
 
    with fs.new_file( content_type = type,  filename = name, metadata = { 'bucket':bucket } ) as upload_file:
        while True:
            chunk = file.read(upload_file.chunk_size)
            if not chunk: break
            upload_file.write(chunk)

    return { '_id': upload_file._id , 'filename': name, 'length': upload_file.length, 'contentType':type, 'bucket' : bucket }


def parseQuery( query ):
    if query is None:
        return query
    import re
    for key,val in query.items():
        if isinstance(val,str) and re.match(r'^\/',val): 
            query[key] = re.compile( re.search(r'\/(.*)\/',val).group(1) )
    return query