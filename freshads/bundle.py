import sys
import os
import shutil
import zipfile

from bson.objectid import ObjectId
from pyramid.path import AssetResolver

import gridfs

class Bundle:


	zip = None

	def __init__(self,ad,db,settings):
		self.settings = settings
		self.db = db
		self.ad = ad
		self.appRoot = AssetResolver('freshads').resolve('').abspath();

	def run(self, tempfile ):
		
		root = self.appRoot;
		
		ad = self.ad;
		

		bundle_root = root + '/tmp/' + str( ad.get('_id') ) + '/'
	
		if os.path.exists( bundle_root ): 
			shutil.rmtree( bundle_root )

		os.makedirs( bundle_root )

		#included libs
		shutil.copy2( root + '/static/bundle/icomoon.woff',  bundle_root + 'icomoon.woff' )
		shutil.copy2( root + '/static/bundle/icomoon.ttf',  bundle_root + 'icomoon.ttf' )
		shutil.copy2( root + '/static/bundle/icomoon.eot',  bundle_root + 'icomoon.eot' )
		shutil.copy2( root + '/static/bundle/icomoon.svg',  bundle_root + 'icomoon.svg' )
		shutil.copy2( root + '/static/bundle/main.css',  bundle_root + 'main.css' )
		shutil.copy2( root + '/static/bundle/fa.js',  bundle_root + 'fa.js' )
		shutil.copy2( root + '/static/bundle/freshad.js',  bundle_root + 'freshad.js' )
		shutil.copy2( root + '/static/bundle/require.js',  bundle_root + 'require.js' )
		shutil.copy2( root + '/static/bundle/jquery-2.0.3.min.js',  bundle_root + 'jquery-2.0.3.min.js' )
		shutil.copy2( root + '/static/bundle/landscape.png',  bundle_root + 'landscape.png' )

		#bundle main ad files
		self._bundle_statics_to( ad, bundle_root, bundle_root + 'assets/')
		self._writeHTML( ad,bundle_root, 'index.html' )

		#bundle expandable files
		if 'expandable' in ad:
			expandable = self.db['ads'].find_one( ad.get('expandable') )
			if expandable:
				self._bundle_statics_to( expandable,bundle_root, bundle_root + 'assets/' )
				self._writeHTML( expandable,bundle_root, expandable.get('id') + '.html' )

		zf = zipfile.ZipFile( tempfile, "w",  zipfile.ZIP_DEFLATED)
		for root, dirs, files in os.walk( bundle_root ):
		    arcdir = root.replace( bundle_root, '' );
		    for filename in files:
		        zf.write( os.path.join( root, filename ), os.path.join( arcdir, filename) )

		shutil.rmtree( bundle_root )

		return tempfile


	def _bundle_statics_to( self, ad, root, assets_root ):
		components = ad.get('components',[])
		self._compile_components_to( components,root + 'components/'  )
		self._compile_assets_to( self._get_assets( ad ), assets_root + str( ad.get('_id') ) +'/' )
		

	def _compile_assets_to( self, files, path ):
		if not os.path.exists( path ):
			os.makedirs( path );

		fs = gridfs.GridFS( self.db )
		files.rewind();
		for fsfile in files:
			with fs.get( fsfile.get( '_id' ) ) as gridout:
				with open( path + gridout.filename, 'wb') as asset:
					asset.write( gridout.read() )
					asset.close()
			gridout.close()

	def _compile_components_to( self, components, root ):
		for c in components:
		    c_type = c.get( 'type' )
		    c_srcdir = self.appRoot + '/static/components/' + c_type +'/'
		    c_dstdir = root + c_type + '/'
		    if os.path.exists( c_dstdir  ): #component sources already copied
		        continue
		    
		    componentStatics = list()

		    try:
		        with open( c_srcdir + 'main.css' ) as cssFile:
		            componentStatics.append( cssFile )
		        cssFile.close()
		    except IOError:
		        pass
		    try:
		        with open( c_srcdir + c_type + '-view.js' ) as controllerFile:
		            componentStatics.append(controllerFile )
		        controllerFile.close()
		    except IOError:
		        pass

		    if len( componentStatics) > 0:
		        os.makedirs(c_dstdir)
		        for f in componentStatics:
		            shutil.copy2( f.name,  c_dstdir + os.path.basename(f.name) );

	
	def _collectFonts( self, ad ):
		fonts = [];
		
		import re
		fontEx = re.compile(r'(ttf|woff|eof|svg)$')
		fonts.extend( [ asset for asset in self._get_assets( ad ) if fontEx.search( asset.get('filename') ) ] )	
		
		if 'expandable' in ad:
			expandable = self.db['ads'].find_one( ad.get('expandable') )
			if expandable:
				fonts.extend( [ asset for asset in self._get_assets( expandable ) if fontEx.search( asset.get('filename') ) ] )
	
		return fonts

	def _get_assets( self, ad ):
		return self.db['fs.files'].find( { 'metadata.bucket': str( ad.get( '_id')  ) } );
		

	def _writeHTML( self, ad, root, filename ):
		env = {}

		env['ad'] = ad
		env['assets_root'] = 'assets/' + str(ad.get('_id')) + '/'
		env['fonts'] = self._collectFonts( ad );
		env['static_root'] = ''
		env['component'] = None
		env['component_controller_root'] = "components/" #relative from require JS 
		env['analytics_url'] = self.settings.get('analytics_url');

		env['expandable'] = self.db['ads'].find_one( ad.get('expandable') )

		from pyramid.renderers import render
		htmlText = render( "freshads:templates/bundle-index.mako",{'env':env} )

		with open( root + filename,'w') as html:
			html.write( htmlText )
		html.close()



		
