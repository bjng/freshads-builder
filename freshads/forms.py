def parse( post ):
	parsed = {}
	for k,v in post.items():
		if "FieldStorage" in type( v ).__name__ and k not in parsed:
			images = collectImages( k, post );
			parsed[ k ] = images
		elif k not in parsed:
			parsed[ k ] = v
	return parsed			


def collectImages( name, post ):
	return list( v for k,v in post.iteritems() if k.startswith( name ) )