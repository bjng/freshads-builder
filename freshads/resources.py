from pyramid.security import Allow,Everyone,Authenticated
from bson.objectid import ObjectId
from datetime import datetime
from pymongo.errors import OperationFailure
import math

def groupfinder(userid, request):
	user = request.db.users.find_one( {'email':userid} );

	if user:
		return  ['group:' + user.get( 'group')] if user.get('group') else [] 




class Root(dict):
    __name__ = None
    __parent__ = None
    __acl__ = [(Allow,Authenticated,'view')]
    
    def __init__(self, request):
    	self.request = request;
    	pass

    def __getitem__(self, key):
    	if key == 'admin':
    		self.__acl__ = [(Allow,'group:superuser', ('view') ),
    			(Allow,'group:producermanager', ('view') )]
    		raise KeyError
    	elif key == 'templates':
    		self.__acl__ = [(Allow,'group:superuser', ('view') ),
    			(Allow,'group:producermanager', ('view') )]
    		raise KeyError
    	elif key == 'users':
    		self.__acl__ = [(Allow,'group:superuser', ('view') )]
    		raise KeyError
    	elif key == 'ads':
    		self.__acl__ = [(Allow,'group:superuser', ('view') ),
    			 (Allow,'group:producer', ('view') ),
    			 (Allow,'group:producermanager', ('view') )]
    		raise KeyError
    	elif key == 'api':
    		from bson.json_util import loads
    		jsonString = self.request.POST.get('data') 
    		data = loads( jsonString ) if jsonString else None 
    		return Api(self.request, data)
    	raise KeyError


class Api( dict ):
	__name__ = ''
	__parent__ = Root

	def __init__(self,request,data):
		self.request = request
		self.data = data

	def __getitem__(self,key ):
		if key == 'fs.files':
			return FilesCollection(self.request, self.data)
		if key == 'analytics':
			return AnalyticsCollection(self.request, self.data)
		elif key == 'components.prototype':
			return ComponentsPrototypeCollection(self.request, self.data)
		elif key == 'proofs':
			return ProofsCollection(self.request, self.data)
		elif key == 'ads':
			return AdsCollection(self.request, self.data)
		elif key == 'campaigns':
			return CampaignsCollection(self.request,self.data )
		elif key == 'users':
			return UsersCollection(self.request,self.data )
		elif key == 'clients':
			return ClientsCollection(self.request, self.data)
		elif key == 'templates':
			return TemplatesCollection(self.request, self.data)
		
		raise KeyError

class MongoContext():
	__parent__ = Api
	__acl__ = []

	def __init__(self, request, data = None):
		self.request = request;
		self.db = request.db
		self.data = data
		self.collection = self.db[self.__name__]

	def save( self, document ):
		from pyramid.security import authenticated_userid
		user = self.db.users.find_one( { 'email': authenticated_userid(self.request) }, {'name':1,'email':1} )
		document['modified'] = datetime.utcnow()
		document['modifiedBy'] = user
		if '_id' not in document:
			_id = ObjectId()
			document['id'] = str(_id)
			document['_id'] = _id;
		
		return self.collection.save( document )

	def remove( self, query ):
		if not query:
			return
		return self.collection.remove( query )

	def find( self, query, projection = None ):
		return self.collection.find( query, projection );

	def find_one( self, query, projection = None ):
		return self.collection.find_one( query, projection )


class AdsCollection( MongoContext ):
	__name__ = 'ads'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:accountmanager', ('find') ),
		(Allow,'group:producermanager', ('save','find','remove') ),
		(Allow,'group:producer', ('save','find') ),
		(Allow,'group:superuser', ('save','find','remove') ) ]

	def save(self,document):
		id = super().save( document );
		if not document.get('proof'):
			proof =  ProofsCollection( self.request ).save( { 'target': str(id) } );
			document['proof'] = proof
			self.collection.save( document )
		return id

	def find_one( self, query, projection = None ):
		from pyramid.security import authenticated_userid
		user = authenticated_userid(self.request)		
		doc =  super().find_one( query, projection )
		
		doc['lock'] = False

		if user != doc.get('modifiedBy',{}).get('email'):
			doc['lock'] = True
	
		#if last change 15 minutes ago, unlock automatically
		if doc['lock']  and ( datetime.utcnow() - doc.get('modified') ).seconds / 60 > 15:
			doc['lock'] = False

		#print( user,doc.get('modifiedBy',{}).get('email'))
		#print(( datetime.utcnow() - doc.get('modified') ).seconds / 60)
		#print( 'lock', doc['lock'] )

		return doc

	def remove( self, query ):
		if not query:
			return
			
		ads = self.collection.find(  query  )
		for ad in ads:
			ProofsCollection(self.request).remove( { 'target': str(ad.get('_id')) } )
			import gridfs
			fs = gridfs.GridFS( self.db );
			for f in FilesCollection(self.request).find( {'metadata.bucket': str( ad.get('_id') ) } ):
				fs.delete( f.get('_id') )

		return super().remove( query )

class UsersCollection( MongoContext ):
	__name__ = 'users'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:superuser', ('save','find','remove') ) ]

	def save(self,document):
		if 'email' in document:
			document[ 'email' ] = document['email'].lower();
		if not '_id' in document and self.collection.find_one({ 'email': document['email']}):
			raise OperationFailure( 'User could not be created. The email address is already in use!' )
		return super().save( document );	


class TemplatesCollection( MongoContext ):
	__name__ = 'templates'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:superuser', ('save','find','remove') ),
		(Allow,'group:producermanager', ('save','find','remove') ) ]

class ClientsCollection( MongoContext ):
	__name__ = 'clients'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:accountmanager', ( 'find') ),
		(Allow,'group:producermanager', ('save','find','remove') ),
		(Allow,'group:producer', ('save','find') ),
		(Allow,'group:superuser', ('save','find','remove') ) ]


	def remove( self, query ):
		if not query:
			return

		clients = self.collection.find(  query  );
		for c in clients:
			CampaignsCollection(self.request).remove( { 'parent':c.get('_id') } );
	
		return super().remove( query )


class CampaignsCollection( MongoContext ):
	__name__ = 'campaigns'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:accountmanager', ( 'find') ),
		(Allow,'group:producermanager', ('save','find','remove') ),
		(Allow,'group:producer', ('save','find') ),
		(Allow,'group:superuser', ('save','find','remove') ) ]

	def remove( self, query ):
		if not query:
			return

		campaigns = self.collection.find(  query  )
		for c in campaigns:
			AdsCollection(self.request).remove( { 'parent':c.get('_id') } );
			
		return super().remove( query )
	

class ComponentsPrototypeCollection( MongoContext ):
	__name__ = 'components.prototype'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:superuser', ('find') ),
		(Allow,'group:producer', ('find') ),
		(Allow,'group:producermanager', ('find') )
		]

class ProofsCollection( MongoContext ):
	__name__ = 'proofs'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:accountmanager', ( 'find') ),
		(Allow,'group:producermanager', ('save','find') ),
		(Allow,'group:producer', ('save','find') ),
		(Allow,'group:superuser', ('save','find') ) ]

	def save( self, document ):
		if 'target' in document:
			existing = self.collection.find_one( { 'target':document.get( 'target') } )
			if existing: 
				self.remove( { '_id': existing.get('_id') } )
		return super().save( document );

class FilesCollection( MongoContext ):
	__name__ = 'fs.files'
	__parent__ = Api
	__acl__ = [
		(Allow,'group:accountmanager', ( 'find' ) ),
		(Allow,'group:producermanager', ('save','find','remove') ),
		(Allow,'group:producer', ('save','find','remove') ),
		(Allow,'group:superuser', ('save','find','remove') ) ]

class AnalyticsCollection( MongoContext ):
	__name__ = 'daily'
	__parent__ = Api
	__acl__ = [(Allow,Authenticated,'find')]


	def __init__(self, request, data):
		self.db = request.analytics_db
		self.data = data
		self.collection = self.db[self.__name__]


	def find(self,query,projection = None ):


		start = datetime.fromtimestamp( query[ 'time' ]['$gte'] )
		end = datetime.fromtimestamp( query[ 'time']['$lte'] )

		query = { 
					'uid': query['id'],
					't': {'$gte' : start , '$lte' : end  },
				};



		return self.collection.find( query );
		
		'''
		agg = self.collection.aggregate([
			{ '$match' : query },
			{ '$group' :{ 
				'_id':{ 'ad': '$ad', 'ip':'$ip', 'date':'$date', 'name':'$name', 'unique':'$unique','component':'$component' },
				'count': {'$sum':1},
				'valueAverage': {'$avg':'$value'}
				}
			},
			{ '$project':
				{
					'_id':0,
					'ad':'$_id.ad',
					'date':'$_id.date',
					'name':'$_id.name',
					'component':'$_id.component',
					'ip':'$_id.ip',
					'unique':'$_id.unique',
					'count': 1,
					'valueAverage': 1,
				}
			}
		])

		return agg.get('result');
		'''